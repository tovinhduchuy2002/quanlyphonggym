﻿
namespace GUI
{
    partial class fDanhsachdungcutheduc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fDanhsachdungcutheduc));
            this.dtbDanhsachthietbi = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTSTB = new System.Windows.Forms.TextBox();
            this.txtTBBT = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbloaithietbi = new System.Windows.Forms.ComboBox();
            this.btnXuatThongtin = new System.Windows.Forms.Button();
            this.btnDSTBBT = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnThemTB = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnMenu = new System.Windows.Forms.Button();
            this.btnTrangChu = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtbDanhsachthietbi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // dtbDanhsachthietbi
            // 
            this.dtbDanhsachthietbi.AllowUserToAddRows = false;
            this.dtbDanhsachthietbi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtbDanhsachthietbi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtbDanhsachthietbi.Location = new System.Drawing.Point(493, 220);
            this.dtbDanhsachthietbi.Name = "dtbDanhsachthietbi";
            this.dtbDanhsachthietbi.RowHeadersWidth = 51;
            this.dtbDanhsachthietbi.RowTemplate.Height = 24;
            this.dtbDanhsachthietbi.Size = new System.Drawing.Size(648, 485);
            this.dtbDanhsachthietbi.TabIndex = 36;
            this.dtbDanhsachthietbi.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtbDanhsachthietbi_CellDoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(25, 353);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 20);
            this.label2.TabIndex = 74;
            this.label2.Text = "Tổng số TB:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(25, 417);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 20);
            this.label1.TabIndex = 75;
            this.label1.Text = "TB bảo trì:";
            // 
            // txtTSTB
            // 
            this.txtTSTB.Location = new System.Drawing.Point(157, 351);
            this.txtTSTB.Name = "txtTSTB";
            this.txtTSTB.Size = new System.Drawing.Size(155, 22);
            this.txtTSTB.TabIndex = 76;
            // 
            // txtTBBT
            // 
            this.txtTBBT.Location = new System.Drawing.Point(157, 415);
            this.txtTBBT.Name = "txtTBBT";
            this.txtTBBT.Size = new System.Drawing.Size(155, 22);
            this.txtTBBT.TabIndex = 77;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(337, 353);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 20);
            this.label3.TabIndex = 78;
            this.label3.Text = "cái";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(337, 417);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 20);
            this.label4.TabIndex = 79;
            this.label4.Text = "cái";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(765, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 20);
            this.label5.TabIndex = 127;
            this.label5.Text = "Loại thiết bị";
            // 
            // cmbloaithietbi
            // 
            this.cmbloaithietbi.FormattingEnabled = true;
            this.cmbloaithietbi.Items.AddRange(new object[] {
            "MÁY CARDIO",
            "MÁY TẬP NGỰC",
            "MÁY TẬP VAI",
            "MÁY TẬP TAY",
            "MAY TẬP BỤNG",
            "MÁY TẬP CHÂN",
            "MÁY TẬP LƯNG",
            "MÁY TẬP XÔ",
            "MÁY TẬP TỔNG HỢP",
            "DÀN RACK",
            "TẠ ĐƠN",
            "TẠ ĐÒN"});
            this.cmbloaithietbi.Location = new System.Drawing.Point(893, 180);
            this.cmbloaithietbi.Name = "cmbloaithietbi";
            this.cmbloaithietbi.Size = new System.Drawing.Size(167, 24);
            this.cmbloaithietbi.TabIndex = 126;
            this.cmbloaithietbi.SelectedValueChanged += new System.EventHandler(this.cmbloaithietbi_SelectedValueChanged);
            // 
            // btnXuatThongtin
            // 
            this.btnXuatThongtin.AutoSize = true;
            this.btnXuatThongtin.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnXuatThongtin.BackgroundImage = global::GUI.Properties.Resources.Xuat;
            this.btnXuatThongtin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnXuatThongtin.FlatAppearance.BorderSize = 0;
            this.btnXuatThongtin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXuatThongtin.Location = new System.Drawing.Point(231, 663);
            this.btnXuatThongtin.Name = "btnXuatThongtin";
            this.btnXuatThongtin.Size = new System.Drawing.Size(140, 60);
            this.btnXuatThongtin.TabIndex = 105;
            this.btnXuatThongtin.UseVisualStyleBackColor = false;
            this.btnXuatThongtin.Click += new System.EventHandler(this.btnXuatThongtin_Click);
            // 
            // btnDSTBBT
            // 
            this.btnDSTBBT.AutoSize = true;
            this.btnDSTBBT.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDSTBBT.BackgroundImage = global::GUI.Properties.Resources.DSTBBT;
            this.btnDSTBBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDSTBBT.FlatAppearance.BorderSize = 0;
            this.btnDSTBBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDSTBBT.Location = new System.Drawing.Point(29, 481);
            this.btnDSTBBT.Name = "btnDSTBBT";
            this.btnDSTBBT.Size = new System.Drawing.Size(342, 60);
            this.btnDSTBBT.TabIndex = 104;
            this.btnDSTBBT.UseVisualStyleBackColor = false;
            this.btnDSTBBT.Click += new System.EventHandler(this.btnDSTBBT_Click);
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = global::GUI.Properties.Resources.f5;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(1099, 175);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(42, 29);
            this.button1.TabIndex = 103;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnThemTB
            // 
            this.btnThemTB.AutoSize = true;
            this.btnThemTB.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnThemTB.BackgroundImage = global::GUI.Properties.Resources.ThemTB;
            this.btnThemTB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnThemTB.FlatAppearance.BorderSize = 0;
            this.btnThemTB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThemTB.Location = new System.Drawing.Point(29, 663);
            this.btnThemTB.Name = "btnThemTB";
            this.btnThemTB.Size = new System.Drawing.Size(140, 60);
            this.btnThemTB.TabIndex = 102;
            this.btnThemTB.UseVisualStyleBackColor = false;
            this.btnThemTB.Click += new System.EventHandler(this.btnThemTB_Click);
            // 
            // btnBack
            // 
            this.btnBack.AutoSize = true;
            this.btnBack.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBack.BackgroundImage = global::GUI.Properties.Resources.back;
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Location = new System.Drawing.Point(1107, 711);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(63, 30);
            this.btnBack.TabIndex = 33;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::GUI.Properties.Resources.lbDanhsach;
            this.pictureBox5.Location = new System.Drawing.Point(-2, 280);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(464, 44);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 25;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.ErrorImage = null;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(389, 158);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(266, 35);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 24;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::GUI.Properties.Resources.QLDC;
            this.pictureBox2.Location = new System.Drawing.Point(57, 210);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(392, 37);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 22;
            this.pictureBox2.TabStop = false;
            // 
            // btnMenu
            // 
            this.btnMenu.AutoSize = true;
            this.btnMenu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnMenu.BackgroundImage = global::GUI.Properties.Resources._3_gach;
            this.btnMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMenu.FlatAppearance.BorderSize = 0;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Location = new System.Drawing.Point(12, 210);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(39, 37);
            this.btnMenu.TabIndex = 21;
            this.btnMenu.UseVisualStyleBackColor = false;
            // 
            // btnTrangChu
            // 
            this.btnTrangChu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTrangChu.BackgroundImage = global::GUI.Properties.Resources.logoTrangChu;
            this.btnTrangChu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTrangChu.FlatAppearance.BorderSize = 0;
            this.btnTrangChu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrangChu.Location = new System.Drawing.Point(12, 143);
            this.btnTrangChu.Name = "btnTrangChu";
            this.btnTrangChu.Size = new System.Drawing.Size(133, 61);
            this.btnTrangChu.TabIndex = 20;
            this.btnTrangChu.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GUI.Properties.Resources.tileQTV;
            this.pictureBox1.Location = new System.Drawing.Point(-2, -3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1186, 140);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.ErrorImage = null;
            this.pictureBox3.Image = global::GUI.Properties.Resources.quanliDCTD;
            this.pictureBox3.Location = new System.Drawing.Point(132, 158);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(266, 35);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 23;
            this.pictureBox3.TabStop = false;
            // 
            // fDanhsachdungcutheduc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1182, 753);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbloaithietbi);
            this.Controls.Add(this.btnXuatThongtin);
            this.Controls.Add(this.btnDSTBBT);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnThemTB);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTBBT);
            this.Controls.Add(this.txtTSTB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtbDanhsachthietbi);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnMenu);
            this.Controls.Add(this.btnTrangChu);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Name = "fDanhsachdungcutheduc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fDanhsachdungcutheduc";
            this.Load += new System.EventHandler(this.fDanhsachdungcutheduc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtbDanhsachthietbi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Button btnTrangChu;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.DataGridView dtbDanhsachthietbi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTSTB;
        private System.Windows.Forms.TextBox txtTBBT;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnThemTB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnDSTBBT;
        private System.Windows.Forms.Button btnXuatThongtin;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbloaithietbi;
    }
}