﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
namespace DAL
{
    public class HoaDonAccess:DatabaseAccess
    {
        public int demHoadon()
        {
            return DatabaseAccess.demHoaDon();
        }
        public bool themHoaDon(HOADON hd)
        {
            return DatabaseAccess.ThemHoaDon(hd);
        }
    }
}
