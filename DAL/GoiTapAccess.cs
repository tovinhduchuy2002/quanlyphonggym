﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
namespace DAL
{
    public class GoiTapAccess:DatabaseAccess
    {

        public int ThangGoiTap(string magoitap)
        {
            return DatabaseAccess.LoaiGoiTap(magoitap);
        }
        public List<string> xemgoiTap(string gioitinh)
        {
            return DatabaseAccess.xemGoiTap(gioitinh);
        }
        public bool KiemTraGoiTap(string magoitap)
        {
            return DatabaseAccess.kiemtraGoiTap(magoitap);
        }
        public int tongtienBienLai(string mabl)
        {
            return tongtienBienLai(mabl);
        }
    }
}
