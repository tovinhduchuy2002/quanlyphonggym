﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class SanPham
    {
        private string masp;
        public string MaSP
        {
            get => masp;
            set => masp = value;
        }
        private string loaisp;
        public string LoaiSP
        {
            get => loaisp;
            set => loaisp = value;
        }
        private string tensp;
        public string TenSP
        {
            get => tensp;
            set => tensp = value;
        }
        private int giathanh;
        public int GiaThanh
        {
            get => giathanh;
            set => giathanh = value;
        }
        private int sl;
        public  int SL
        {
            get => sl;
            set => sl = value;
        }
        private Byte[] anh;
        public Byte[] Anh
        {
            get => anh;
            set => anh = value;
        }
        public  SanPham()
        { }
        ~SanPham() { }
    }
}
