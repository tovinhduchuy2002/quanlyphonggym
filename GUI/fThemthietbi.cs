﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Drawing.Imaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;
namespace GUI
{
    public partial class fThemthietbi : Form
    {
        public fThemthietbi()
        {
            InitializeComponent();
        }
        
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        THIETBI tb = new THIETBI();
        ThietBiBLL tbll = new ThietBiBLL();
        private bool ptbClicked = false;
        private void btnLuu_Click(object sender, EventArgs e)
        {
            
            if (cmbloaithietbi.SelectedIndex == -1)
            {
                MessageBox.Show("Chọn loại thiết bị");
                return;
            }

            if (cmbTinhTrang.SelectedIndex == -1)
            {
                MessageBox.Show("Chọn tình trạng thiết bị");
                return;
            }

            if (string.IsNullOrEmpty(tbtenTB.Text))
            {
                MessageBox.Show("Nhập tên thiết bị");
                return;
            }
            else
            {
                tb.Tenthietbi = tbtenTB.Text;
            }
            tb.Anh = imageToByteArray(ptbTB);
            bool getinsert = tbll.themdulieu(tb);
            switch(getinsert)
            {
                case true:
                    MessageBox.Show("Thêm thành công");
                    return;
                case false:
                    MessageBox.Show("Thêm thất bại");
                    return;
            }
           

        }
        private byte[] imageToByteArray(PictureBox ptb)
        {
            using (MemoryStream ms = new MemoryStream())
            {

                ptb.Image.Save(ms, ptb.Image.RawFormat); // Thay đổi định dạng ảnh nếu cần thiết
                return ms.ToArray();
            }
        }

        private void ptbTB_Click(object sender, EventArgs e)
        {
            OpenFileDialog moFile = new OpenFileDialog();
            moFile.Title = "Chọn ảnh khách hàng";
            moFile.Filter = "Image Files(*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf)|*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf";
            if (moFile.ShowDialog() == DialogResult.OK)
            {
                ptbClicked = true;

                ptbTB.ImageLocation = moFile.FileName;


            }
        }

        private void cmbloaithietbi_SelectedValueChanged(object sender, EventArgs e)
        {
            if(cmbloaithietbi.SelectedItem.ToString()== "MÁY CARDIO")
            {
                tb.Loaithietbi = "MAYCARDIO";
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP NGỰC")
            {
                tb.Loaithietbi = "MAYTAPNGUC";
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP VAI")
            {
                tb.Loaithietbi = "MAYTAPVAI";
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP TAY")
            {
                tb.Loaithietbi = "MAYTAPTAY";
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP BỤNG")
            {
                tb.Loaithietbi = "MAYTAPBUNG";
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP CHÂN")
            {
                tb.Loaithietbi = "MAYTAPCHAN";
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP LƯNG")
            {
                tb.Loaithietbi = "MAYTAPLUNG";
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP XÔ")
            {
                tb.Loaithietbi = "MAYTAPXO";
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP TỔNG HỢP")
            {
                tb.Loaithietbi = "MAYTAPTONGHOP";
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "DÀN RACK")
            {
                tb.Loaithietbi = "DANRACK";
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "TẠ ĐƠN")
            {
                tb.Loaithietbi = "TADON";
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "TẠ ĐÒN")
            {
                tb.Loaithietbi = "TADONN";
            }

        }

        private void cmbTinhTrang_SelectedValueChanged(object sender, EventArgs e)
        {
            if(cmbTinhTrang.SelectedItem.ToString() == "TOT")
            {
                tb.Tinhtrang = "TOT";
            }
            else if (cmbTinhTrang.SelectedItem.ToString() == "BAOTRI")
            {
                tb.Tinhtrang = "BAOTRI";
            }
        }
    }
}
