﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;
namespace GUI
{
    public partial class fDanhSachSP : Form
    {
        public fDanhSachSP()
        {
            InitializeComponent();
        }
        SanPhamBLL spbll = new SanPhamBLL();
        private string loaisp;
        List<SanPham> lsp = new List<SanPham>();
        
        private void dtgDSSP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if(cmbLoaiSP.SelectedItem == "Thực phẩm")
            {
                loaisp = "THUCPHAM";
                LoadListSPtheoLoai(loaisp);
            }
            if(cmbLoaiSP.SelectedItem == "Dụng cụ hỗ trợ")
            {
                loaisp = "DUNGCU";
                LoadListSPtheoLoai(loaisp);

            }
        }
  
        private void fDanhSachSP_Load(object sender, EventArgs e)
        {
            LoadLISTSP();
        }
        private void LoadLISTSP()
        {
            int count = 0;
            lsp = spbll.xemListSP();
            dtgDSSP.Columns.Add("Column1", "STT");
            dtgDSSP.Columns.Add("Column2", "MASP");
            dtgDSSP.Columns.Add("Column3", "LOAISP");
            dtgDSSP.Columns.Add("Column4", "TENSP");
            dtgDSSP.Columns.Add("Column5", "SOLUONG");
            dtgDSSP.Columns.Add("Column6", "DONVI");
            foreach(SanPham sp in lsp)
            {
                count++;
                dtgDSSP.Rows.Add(count.ToString(),sp.MaSP,sp.LoaiSP,sp.TenSP,sp.SL,"Cái");
            }

        }
        private void LoadListSPtheoLoai(string loaiSP)
        {
            int count = 0;
            dtgDSSP.Rows.Clear();
            lsp = spbll.xemListSPtheoLoai(loaiSP);
           
            foreach (SanPham sp in lsp)
            {
                count++;
                dtgDSSP.Rows.Add(count.ToString(), sp.MaSP, sp.LoaiSP, sp.TenSP, sp.SL, "Cái");
            }
        }
        private void RefreshData()
        {
            int count = 0;
            dtgDSSP.Rows.Clear();
            List<SanPham> lsp = new List<SanPham>();

            lsp = spbll.xemListSP();

            foreach (SanPham sp in lsp)
            {
                count++;
                dtgDSSP.Rows.Add(count.ToString(), sp.MaSP, sp.LoaiSP, sp.TenSP, sp.SL, "Cái");
            }

        }

        private void dtgDSSP_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1 && e.RowIndex >= 0)
            {
                object value = dtgDSSP.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                string maSP = (string)value;
                fThonTinSP fttsp = new fThonTinSP();
                fThonTinSP.masp = maSP;
                this.Hide();
                fttsp.ShowDialog();
                this.Show();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RefreshData();
        }
    }
}

