﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class BienlaiBLL
    {

        BienlaiAccess blaccss = new BienlaiAccess();
        public static string maBL { get; set; }
        public void capnhatBienlai(KHACHHANG khachhang)
        {
            BIENLAI bl = new BIENLAI();
            bl.MaThe = khachhang.Mathe;
            bl.MaGoiTap = khachhang.maGoiTap;
            bl.GoiTapPT = khachhang.GoitapPT;
            bl.NgayThanhToan = khachhang.NgayBatDau;
            bl.MaBienLai = bl.MaThe + bl.MaGoiTap + bl.NgayThanhToan.ToShortDateString();
            maBL = bl.MaBienLai;
            blaccss.ThemBienLai(bl);

        }
        public BIENLAI xemBienlai()
        {
            return blaccss.xemBienLai(maBL);

        }
    }
}
