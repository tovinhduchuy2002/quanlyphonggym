﻿
namespace GUI
{
    partial class fBanSP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fBanSP));
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnMenu = new System.Windows.Forms.Button();
            this.btnTrangChu = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.cmbLoaiSP = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ptbSP1 = new System.Windows.Forms.PictureBox();
            this.dtgDSSP = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.ptbSP2 = new System.Windows.Forms.PictureBox();
            this.ptbSP3 = new System.Windows.Forms.PictureBox();
            this.lbTenSP1 = new System.Windows.Forms.Label();
            this.lbTenSP2 = new System.Windows.Forms.Label();
            this.lbtenSP3 = new System.Windows.Forms.Label();
            this.lbGiaThanhSP1 = new System.Windows.Forms.Label();
            this.lbGiaThanhSP2 = new System.Windows.Forms.Label();
            this.lbGiaThanhSP3 = new System.Windows.Forms.Label();
            this.lbSLSP1 = new System.Windows.Forms.Label();
            this.lbSLSP2 = new System.Windows.Forms.Label();
            this.lbSLSP3 = new System.Windows.Forms.Label();
            this.btnCongSP1 = new System.Windows.Forms.Button();
            this.btnTruSP1 = new System.Windows.Forms.Button();
            this.btnCongSP2 = new System.Windows.Forms.Button();
            this.btnTruSP2 = new System.Windows.Forms.Button();
            this.btnTruSP3 = new System.Windows.Forms.Button();
            this.btnCongSP3 = new System.Windows.Forms.Button();
            this.btnTrai = new System.Windows.Forms.Button();
            this.btnPhai = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbMaKH = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLuu = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbSP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDSSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbSP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbSP3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.ErrorImage = null;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(402, 109);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(266, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 58;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(106, 165);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(469, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 56;
            this.pictureBox2.TabStop = false;
            // 
            // btnMenu
            // 
            this.btnMenu.AutoSize = true;
            this.btnMenu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnMenu.BackgroundImage = global::GUI.Properties.Resources._3_gach;
            this.btnMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMenu.FlatAppearance.BorderSize = 0;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Location = new System.Drawing.Point(22, 155);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(60, 61);
            this.btnMenu.TabIndex = 55;
            this.btnMenu.UseVisualStyleBackColor = false;
            // 
            // btnTrangChu
            // 
            this.btnTrangChu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTrangChu.BackgroundImage = global::GUI.Properties.Resources.logoTrangChu;
            this.btnTrangChu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTrangChu.FlatAppearance.BorderSize = 0;
            this.btnTrangChu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrangChu.Location = new System.Drawing.Point(22, 98);
            this.btnTrangChu.Name = "btnTrangChu";
            this.btnTrangChu.Size = new System.Drawing.Size(149, 61);
            this.btnTrangChu.TabIndex = 54;
            this.btnTrangChu.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GUI.Properties.Resources.tileQTV;
            this.pictureBox1.Location = new System.Drawing.Point(-2, -3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1186, 95);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 53;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.ErrorImage = null;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(157, 109);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(266, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 57;
            this.pictureBox3.TabStop = false;
            // 
            // cmbLoaiSP
            // 
            this.cmbLoaiSP.FormattingEnabled = true;
            this.cmbLoaiSP.Items.AddRange(new object[] {
            "Thực phẩm",
            "Dụng cụ hỗ trợ"});
            this.cmbLoaiSP.Location = new System.Drawing.Point(106, 235);
            this.cmbLoaiSP.Name = "cmbLoaiSP";
            this.cmbLoaiSP.Size = new System.Drawing.Size(152, 24);
            this.cmbLoaiSP.TabIndex = 150;
            this.cmbLoaiSP.SelectedValueChanged += new System.EventHandler(this.cmbLoaiSP_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(18, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 20);
            this.label4.TabIndex = 149;
            this.label4.Text = "Loại";
            // 
            // ptbSP1
            // 
            this.ptbSP1.Image = ((System.Drawing.Image)(resources.GetObject("ptbSP1.Image")));
            this.ptbSP1.Location = new System.Drawing.Point(22, 331);
            this.ptbSP1.Name = "ptbSP1";
            this.ptbSP1.Size = new System.Drawing.Size(124, 137);
            this.ptbSP1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbSP1.TabIndex = 153;
            this.ptbSP1.TabStop = false;
            // 
            // dtgDSSP
            // 
            this.dtgDSSP.AllowUserToAddRows = false;
            this.dtgDSSP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgDSSP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDSSP.Location = new System.Drawing.Point(671, 289);
            this.dtgDSSP.Name = "dtgDSSP";
            this.dtgDSSP.RowHeadersWidth = 51;
            this.dtgDSSP.RowTemplate.Height = 24;
            this.dtgDSSP.Size = new System.Drawing.Size(489, 368);
            this.dtgDSSP.TabIndex = 154;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.label1.Location = new System.Drawing.Point(860, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 32);
            this.label1.TabIndex = 155;
            this.label1.Text = "Giỏ Hàng";
            // 
            // ptbSP2
            // 
            this.ptbSP2.Image = ((System.Drawing.Image)(resources.GetObject("ptbSP2.Image")));
            this.ptbSP2.Location = new System.Drawing.Point(229, 331);
            this.ptbSP2.Name = "ptbSP2";
            this.ptbSP2.Size = new System.Drawing.Size(124, 137);
            this.ptbSP2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbSP2.TabIndex = 156;
            this.ptbSP2.TabStop = false;
            // 
            // ptbSP3
            // 
            this.ptbSP3.Image = ((System.Drawing.Image)(resources.GetObject("ptbSP3.Image")));
            this.ptbSP3.Location = new System.Drawing.Point(438, 331);
            this.ptbSP3.Name = "ptbSP3";
            this.ptbSP3.Size = new System.Drawing.Size(124, 137);
            this.ptbSP3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbSP3.TabIndex = 157;
            this.ptbSP3.TabStop = false;
            // 
            // lbTenSP1
            // 
            this.lbTenSP1.AutoSize = true;
            this.lbTenSP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenSP1.ForeColor = System.Drawing.Color.Teal;
            this.lbTenSP1.Location = new System.Drawing.Point(61, 485);
            this.lbTenSP1.Name = "lbTenSP1";
            this.lbTenSP1.Size = new System.Drawing.Size(45, 20);
            this.lbTenSP1.TabIndex = 158;
            this.lbTenSP1.Text = "Loại";
            // 
            // lbTenSP2
            // 
            this.lbTenSP2.AutoSize = true;
            this.lbTenSP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenSP2.ForeColor = System.Drawing.Color.Teal;
            this.lbTenSP2.Location = new System.Drawing.Point(267, 485);
            this.lbTenSP2.Name = "lbTenSP2";
            this.lbTenSP2.Size = new System.Drawing.Size(45, 20);
            this.lbTenSP2.TabIndex = 159;
            this.lbTenSP2.Text = "Loại";
            // 
            // lbtenSP3
            // 
            this.lbtenSP3.AutoSize = true;
            this.lbtenSP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtenSP3.ForeColor = System.Drawing.Color.Teal;
            this.lbtenSP3.Location = new System.Drawing.Point(477, 485);
            this.lbtenSP3.Name = "lbtenSP3";
            this.lbtenSP3.Size = new System.Drawing.Size(45, 20);
            this.lbtenSP3.TabIndex = 160;
            this.lbtenSP3.Text = "Loại";
            // 
            // lbGiaThanhSP1
            // 
            this.lbGiaThanhSP1.AutoSize = true;
            this.lbGiaThanhSP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiaThanhSP1.ForeColor = System.Drawing.Color.Teal;
            this.lbGiaThanhSP1.Location = new System.Drawing.Point(61, 520);
            this.lbGiaThanhSP1.Name = "lbGiaThanhSP1";
            this.lbGiaThanhSP1.Size = new System.Drawing.Size(45, 20);
            this.lbGiaThanhSP1.TabIndex = 161;
            this.lbGiaThanhSP1.Text = "Loại";
            // 
            // lbGiaThanhSP2
            // 
            this.lbGiaThanhSP2.AutoSize = true;
            this.lbGiaThanhSP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiaThanhSP2.ForeColor = System.Drawing.Color.Teal;
            this.lbGiaThanhSP2.Location = new System.Drawing.Point(267, 520);
            this.lbGiaThanhSP2.Name = "lbGiaThanhSP2";
            this.lbGiaThanhSP2.Size = new System.Drawing.Size(45, 20);
            this.lbGiaThanhSP2.TabIndex = 162;
            this.lbGiaThanhSP2.Text = "Loại";
            // 
            // lbGiaThanhSP3
            // 
            this.lbGiaThanhSP3.AutoSize = true;
            this.lbGiaThanhSP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiaThanhSP3.ForeColor = System.Drawing.Color.Teal;
            this.lbGiaThanhSP3.Location = new System.Drawing.Point(477, 520);
            this.lbGiaThanhSP3.Name = "lbGiaThanhSP3";
            this.lbGiaThanhSP3.Size = new System.Drawing.Size(45, 20);
            this.lbGiaThanhSP3.TabIndex = 163;
            this.lbGiaThanhSP3.Text = "Loại";
            // 
            // lbSLSP1
            // 
            this.lbSLSP1.AutoSize = true;
            this.lbSLSP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSLSP1.ForeColor = System.Drawing.Color.Teal;
            this.lbSLSP1.Location = new System.Drawing.Point(61, 554);
            this.lbSLSP1.Name = "lbSLSP1";
            this.lbSLSP1.Size = new System.Drawing.Size(45, 20);
            this.lbSLSP1.TabIndex = 164;
            this.lbSLSP1.Text = "Loại";
            // 
            // lbSLSP2
            // 
            this.lbSLSP2.AutoSize = true;
            this.lbSLSP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSLSP2.ForeColor = System.Drawing.Color.Teal;
            this.lbSLSP2.Location = new System.Drawing.Point(267, 554);
            this.lbSLSP2.Name = "lbSLSP2";
            this.lbSLSP2.Size = new System.Drawing.Size(45, 20);
            this.lbSLSP2.TabIndex = 165;
            this.lbSLSP2.Text = "Loại";
            // 
            // lbSLSP3
            // 
            this.lbSLSP3.AutoSize = true;
            this.lbSLSP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSLSP3.ForeColor = System.Drawing.Color.Teal;
            this.lbSLSP3.Location = new System.Drawing.Point(477, 554);
            this.lbSLSP3.Name = "lbSLSP3";
            this.lbSLSP3.Size = new System.Drawing.Size(45, 20);
            this.lbSLSP3.TabIndex = 166;
            this.lbSLSP3.Text = "Loại";
            // 
            // btnCongSP1
            // 
            this.btnCongSP1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCongSP1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCongSP1.BackgroundImage")));
            this.btnCongSP1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCongSP1.FlatAppearance.BorderSize = 0;
            this.btnCongSP1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCongSP1.Location = new System.Drawing.Point(112, 550);
            this.btnCongSP1.Name = "btnCongSP1";
            this.btnCongSP1.Size = new System.Drawing.Size(59, 42);
            this.btnCongSP1.TabIndex = 167;
            this.btnCongSP1.UseVisualStyleBackColor = false;
            this.btnCongSP1.Click += new System.EventHandler(this.btnCongSP1_Click);
            // 
            // btnTruSP1
            // 
            this.btnTruSP1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTruSP1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTruSP1.BackgroundImage")));
            this.btnTruSP1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTruSP1.FlatAppearance.BorderSize = 0;
            this.btnTruSP1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTruSP1.Location = new System.Drawing.Point(-2, 550);
            this.btnTruSP1.Name = "btnTruSP1";
            this.btnTruSP1.Size = new System.Drawing.Size(57, 42);
            this.btnTruSP1.TabIndex = 168;
            this.btnTruSP1.UseVisualStyleBackColor = false;
            this.btnTruSP1.Click += new System.EventHandler(this.btnTruSP1_Click);
            // 
            // btnCongSP2
            // 
            this.btnCongSP2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCongSP2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCongSP2.BackgroundImage")));
            this.btnCongSP2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCongSP2.FlatAppearance.BorderSize = 0;
            this.btnCongSP2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCongSP2.Location = new System.Drawing.Point(318, 550);
            this.btnCongSP2.Name = "btnCongSP2";
            this.btnCongSP2.Size = new System.Drawing.Size(59, 42);
            this.btnCongSP2.TabIndex = 169;
            this.btnCongSP2.UseVisualStyleBackColor = false;
            this.btnCongSP2.Click += new System.EventHandler(this.btnCongSP2_Click);
            // 
            // btnTruSP2
            // 
            this.btnTruSP2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTruSP2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTruSP2.BackgroundImage")));
            this.btnTruSP2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTruSP2.FlatAppearance.BorderSize = 0;
            this.btnTruSP2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTruSP2.Location = new System.Drawing.Point(204, 550);
            this.btnTruSP2.Name = "btnTruSP2";
            this.btnTruSP2.Size = new System.Drawing.Size(57, 42);
            this.btnTruSP2.TabIndex = 170;
            this.btnTruSP2.UseVisualStyleBackColor = false;
            this.btnTruSP2.Click += new System.EventHandler(this.btnTruSP2_Click);
            // 
            // btnTruSP3
            // 
            this.btnTruSP3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTruSP3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTruSP3.BackgroundImage")));
            this.btnTruSP3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTruSP3.FlatAppearance.BorderSize = 0;
            this.btnTruSP3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTruSP3.Location = new System.Drawing.Point(414, 550);
            this.btnTruSP3.Name = "btnTruSP3";
            this.btnTruSP3.Size = new System.Drawing.Size(57, 42);
            this.btnTruSP3.TabIndex = 171;
            this.btnTruSP3.UseVisualStyleBackColor = false;
            this.btnTruSP3.Click += new System.EventHandler(this.btnTruSP3_Click);
            // 
            // btnCongSP3
            // 
            this.btnCongSP3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCongSP3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCongSP3.BackgroundImage")));
            this.btnCongSP3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCongSP3.FlatAppearance.BorderSize = 0;
            this.btnCongSP3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCongSP3.Location = new System.Drawing.Point(528, 550);
            this.btnCongSP3.Name = "btnCongSP3";
            this.btnCongSP3.Size = new System.Drawing.Size(59, 42);
            this.btnCongSP3.TabIndex = 172;
            this.btnCongSP3.UseVisualStyleBackColor = false;
            this.btnCongSP3.Click += new System.EventHandler(this.btnCongSP3_Click);
            // 
            // btnTrai
            // 
            this.btnTrai.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTrai.BackgroundImage = global::GUI.Properties.Resources.back;
            this.btnTrai.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTrai.FlatAppearance.BorderSize = 0;
            this.btnTrai.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrai.Location = new System.Drawing.Point(112, 671);
            this.btnTrai.Name = "btnTrai";
            this.btnTrai.Size = new System.Drawing.Size(59, 42);
            this.btnTrai.TabIndex = 173;
            this.btnTrai.UseVisualStyleBackColor = false;
            this.btnTrai.Click += new System.EventHandler(this.btnTrai_Click);
            // 
            // btnPhai
            // 
            this.btnPhai.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPhai.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPhai.BackgroundImage")));
            this.btnPhai.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPhai.FlatAppearance.BorderSize = 0;
            this.btnPhai.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPhai.Location = new System.Drawing.Point(414, 671);
            this.btnPhai.Name = "btnPhai";
            this.btnPhai.Size = new System.Drawing.Size(59, 42);
            this.btnPhai.TabIndex = 174;
            this.btnPhai.UseVisualStyleBackColor = false;
            this.btnPhai.Click += new System.EventHandler(this.btnPhai_Click);
            // 
            // btnBack
            // 
            this.btnBack.AutoSize = true;
            this.btnBack.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBack.BackgroundImage = global::GUI.Properties.Resources.back;
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Location = new System.Drawing.Point(1058, 683);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(102, 47);
            this.btnBack.TabIndex = 175;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = global::GUI.Properties.Resources.f5;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(298, 235);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(37, 29);
            this.button1.TabIndex = 176;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbMaKH
            // 
            this.cmbMaKH.FormattingEnabled = true;
            this.cmbMaKH.Location = new System.Drawing.Point(991, 240);
            this.cmbMaKH.Name = "cmbMaKH";
            this.cmbMaKH.Size = new System.Drawing.Size(169, 24);
            this.cmbMaKH.TabIndex = 177;
            this.cmbMaKH.SelectedValueChanged += new System.EventHandler(this.cmbMaKH_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Lime;
            this.label2.Location = new System.Drawing.Point(886, 240);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 25);
            this.label2.TabIndex = 178;
            this.label2.Text = "Mã KH";
            // 
            // btnLuu
            // 
            this.btnLuu.AutoSize = true;
            this.btnLuu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLuu.BackgroundImage = global::GUI.Properties.Resources.luu;
            this.btnLuu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLuu.FlatAppearance.BorderSize = 0;
            this.btnLuu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLuu.Location = new System.Drawing.Point(907, 683);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(121, 50);
            this.btnLuu.TabIndex = 179;
            this.btnLuu.UseVisualStyleBackColor = false;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // fBanSP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1182, 753);
            this.Controls.Add(this.btnLuu);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbMaKH);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnPhai);
            this.Controls.Add(this.btnTrai);
            this.Controls.Add(this.btnCongSP3);
            this.Controls.Add(this.btnTruSP3);
            this.Controls.Add(this.btnTruSP2);
            this.Controls.Add(this.btnCongSP2);
            this.Controls.Add(this.btnTruSP1);
            this.Controls.Add(this.btnCongSP1);
            this.Controls.Add(this.lbSLSP3);
            this.Controls.Add(this.lbSLSP2);
            this.Controls.Add(this.lbSLSP1);
            this.Controls.Add(this.lbGiaThanhSP3);
            this.Controls.Add(this.lbGiaThanhSP2);
            this.Controls.Add(this.lbGiaThanhSP1);
            this.Controls.Add(this.lbtenSP3);
            this.Controls.Add(this.lbTenSP2);
            this.Controls.Add(this.lbTenSP1);
            this.Controls.Add(this.ptbSP3);
            this.Controls.Add(this.ptbSP2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtgDSSP);
            this.Controls.Add(this.ptbSP1);
            this.Controls.Add(this.cmbLoaiSP);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnMenu);
            this.Controls.Add(this.btnTrangChu);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Name = "fBanSP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fBanSP";
            this.Load += new System.EventHandler(this.fBanSP_Load);
            this.LocationChanged += new System.EventHandler(this.fBanSP_LocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbSP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDSSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbSP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbSP3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Button btnTrangChu;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.ComboBox cmbLoaiSP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox ptbSP1;
        private System.Windows.Forms.DataGridView dtgDSSP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox ptbSP2;
        private System.Windows.Forms.PictureBox ptbSP3;
        private System.Windows.Forms.Label lbTenSP1;
        private System.Windows.Forms.Label lbTenSP2;
        private System.Windows.Forms.Label lbtenSP3;
        private System.Windows.Forms.Label lbGiaThanhSP1;
        private System.Windows.Forms.Label lbGiaThanhSP2;
        private System.Windows.Forms.Label lbGiaThanhSP3;
        private System.Windows.Forms.Label lbSLSP1;
        private System.Windows.Forms.Label lbSLSP2;
        private System.Windows.Forms.Label lbSLSP3;
        private System.Windows.Forms.Button btnCongSP1;
        private System.Windows.Forms.Button btnTruSP1;
        private System.Windows.Forms.Button btnCongSP2;
        private System.Windows.Forms.Button btnTruSP2;
        private System.Windows.Forms.Button btnTruSP3;
        private System.Windows.Forms.Button btnCongSP3;
        private System.Windows.Forms.Button btnTrai;
        private System.Windows.Forms.Button btnPhai;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cmbMaKH;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnLuu;
    }
}