﻿
namespace GUI
{
    partial class fThongTinKH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbMaThẻ = new System.Windows.Forms.TextBox();
            this.tbHoTen = new System.Windows.Forms.TextBox();
            this.tbNgaysinh = new System.Windows.Forms.TextBox();
            this.tbCCCD = new System.Windows.Forms.TextBox();
            this.tbDiachi = new System.Windows.Forms.TextBox();
            this.tbSDT = new System.Windows.Forms.TextBox();
            this.tbEMail = new System.Windows.Forms.TextBox();
            this.tbGHichu = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbGioiTinh = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tbNgayHethan = new System.Windows.Forms.TextBox();
            this.tbNgayDK = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tbTENPT = new System.Windows.Forms.TextBox();
            this.tbThoiHanPT = new System.Windows.Forms.TextBox();
            this.btnXoaanh = new System.Windows.Forms.Button();
            this.btnChinhSua = new System.Windows.Forms.Button();
            this.btnXuatThongtin = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.ptbAvatar = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnMenu = new System.Windows.Forms.Button();
            this.btnTrangChu = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.cmbLoai = new System.Windows.Forms.ComboBox();
            this.cbPT = new System.Windows.Forms.CheckBox();
            this.cmbMSHLV = new System.Windows.Forms.ComboBox();
            this.cmbGoiTapPT = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.ptbAvatar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(93, 350);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 20);
            this.label2.TabIndex = 25;
            this.label2.Text = "Mã thẻ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(92, 394);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 20);
            this.label3.TabIndex = 26;
            this.label3.Text = "Họ tên";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(94, 431);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 20);
            this.label4.TabIndex = 27;
            this.label4.Text = "Ngày sinh";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(92, 471);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 20);
            this.label5.TabIndex = 28;
            this.label5.Text = "CMND/CCCD";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Teal;
            this.label6.Location = new System.Drawing.Point(94, 507);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 20);
            this.label6.TabIndex = 29;
            this.label6.Text = "Địa chỉ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Teal;
            this.label7.Location = new System.Drawing.Point(94, 546);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 20);
            this.label7.TabIndex = 30;
            this.label7.Text = "SDT";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Teal;
            this.label8.Location = new System.Drawing.Point(92, 586);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 20);
            this.label8.TabIndex = 31;
            this.label8.Text = "Email";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Teal;
            this.label9.Location = new System.Drawing.Point(92, 624);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 20);
            this.label9.TabIndex = 32;
            this.label9.Text = "Ghi chú";
            // 
            // tbMaThẻ
            // 
            this.tbMaThẻ.Location = new System.Drawing.Point(224, 350);
            this.tbMaThẻ.Name = "tbMaThẻ";
            this.tbMaThẻ.Size = new System.Drawing.Size(78, 22);
            this.tbMaThẻ.TabIndex = 33;
            // 
            // tbHoTen
            // 
            this.tbHoTen.Location = new System.Drawing.Point(224, 394);
            this.tbHoTen.Name = "tbHoTen";
            this.tbHoTen.Size = new System.Drawing.Size(309, 22);
            this.tbHoTen.TabIndex = 34;
            // 
            // tbNgaysinh
            // 
            this.tbNgaysinh.Location = new System.Drawing.Point(224, 431);
            this.tbNgaysinh.Name = "tbNgaysinh";
            this.tbNgaysinh.Size = new System.Drawing.Size(165, 22);
            this.tbNgaysinh.TabIndex = 35;
            // 
            // tbCCCD
            // 
            this.tbCCCD.Location = new System.Drawing.Point(224, 471);
            this.tbCCCD.Name = "tbCCCD";
            this.tbCCCD.Size = new System.Drawing.Size(165, 22);
            this.tbCCCD.TabIndex = 36;
            // 
            // tbDiachi
            // 
            this.tbDiachi.Location = new System.Drawing.Point(224, 507);
            this.tbDiachi.Name = "tbDiachi";
            this.tbDiachi.Size = new System.Drawing.Size(309, 22);
            this.tbDiachi.TabIndex = 37;
            // 
            // tbSDT
            // 
            this.tbSDT.Location = new System.Drawing.Point(224, 546);
            this.tbSDT.Name = "tbSDT";
            this.tbSDT.Size = new System.Drawing.Size(309, 22);
            this.tbSDT.TabIndex = 38;
            // 
            // tbEMail
            // 
            this.tbEMail.Location = new System.Drawing.Point(224, 586);
            this.tbEMail.Name = "tbEMail";
            this.tbEMail.Size = new System.Drawing.Size(309, 22);
            this.tbEMail.TabIndex = 39;
            // 
            // tbGHichu
            // 
            this.tbGHichu.Location = new System.Drawing.Point(224, 624);
            this.tbGHichu.Name = "tbGHichu";
            this.tbGHichu.Size = new System.Drawing.Size(309, 22);
            this.tbGHichu.TabIndex = 40;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Teal;
            this.label10.Location = new System.Drawing.Point(395, 433);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 20);
            this.label10.TabIndex = 41;
            this.label10.Text = "Giới tính";
            // 
            // tbGioiTinh
            // 
            this.tbGioiTinh.Location = new System.Drawing.Point(481, 431);
            this.tbGioiTinh.Name = "tbGioiTinh";
            this.tbGioiTinh.Size = new System.Drawing.Size(52, 22);
            this.tbGioiTinh.TabIndex = 42;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Teal;
            this.label11.Location = new System.Drawing.Point(572, 352);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 20);
            this.label11.TabIndex = 43;
            this.label11.Text = "Loại";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Teal;
            this.label13.Location = new System.Drawing.Point(572, 394);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(117, 20);
            this.label13.TabIndex = 47;
            this.label13.Text = "Ngày đăng kí";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Teal;
            this.label14.Location = new System.Drawing.Point(890, 396);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 20);
            this.label14.TabIndex = 48;
            this.label14.Text = "Đến ngày";
            // 
            // tbNgayHethan
            // 
            this.tbNgayHethan.Location = new System.Drawing.Point(1001, 396);
            this.tbNgayHethan.Name = "tbNgayHethan";
            this.tbNgayHethan.Size = new System.Drawing.Size(169, 22);
            this.tbNgayHethan.TabIndex = 49;
            // 
            // tbNgayDK
            // 
            this.tbNgayDK.Location = new System.Drawing.Point(713, 392);
            this.tbNgayDK.Name = "tbNgayDK";
            this.tbNgayDK.Size = new System.Drawing.Size(162, 22);
            this.tbNgayDK.TabIndex = 50;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Teal;
            this.label15.Location = new System.Drawing.Point(572, 433);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(186, 20);
            this.label15.TabIndex = 51;
            this.label15.Text = "Huấn luyện viên (PT)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Teal;
            this.label16.Location = new System.Drawing.Point(572, 471);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 20);
            this.label16.TabIndex = 56;
            this.label16.Text = "Tên PT";
            this.label16.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Teal;
            this.label17.Location = new System.Drawing.Point(572, 507);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 20);
            this.label17.TabIndex = 57;
            this.label17.Text = "MSNV";
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Teal;
            this.label18.Location = new System.Drawing.Point(572, 546);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 20);
            this.label18.TabIndex = 58;
            this.label18.Text = "Gói tập";
            this.label18.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Teal;
            this.label19.Location = new System.Drawing.Point(572, 586);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(81, 20);
            this.label19.TabIndex = 59;
            this.label19.Text = "Thời hạn";
            this.label19.Visible = false;
            // 
            // tbTENPT
            // 
            this.tbTENPT.Location = new System.Drawing.Point(703, 471);
            this.tbTENPT.Name = "tbTENPT";
            this.tbTENPT.Size = new System.Drawing.Size(255, 22);
            this.tbTENPT.TabIndex = 60;
            this.tbTENPT.Visible = false;
            // 
            // tbThoiHanPT
            // 
            this.tbThoiHanPT.Location = new System.Drawing.Point(703, 586);
            this.tbThoiHanPT.Name = "tbThoiHanPT";
            this.tbThoiHanPT.Size = new System.Drawing.Size(255, 22);
            this.tbThoiHanPT.TabIndex = 63;
            this.tbThoiHanPT.Visible = false;
            // 
            // btnXoaanh
            // 
            this.btnXoaanh.AutoSize = true;
            this.btnXoaanh.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnXoaanh.BackgroundImage = global::GUI.Properties.Resources.xoa_anh;
            this.btnXoaanh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnXoaanh.FlatAppearance.BorderSize = 0;
            this.btnXoaanh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXoaanh.Location = new System.Drawing.Point(852, 284);
            this.btnXoaanh.Name = "btnXoaanh";
            this.btnXoaanh.Size = new System.Drawing.Size(114, 41);
            this.btnXoaanh.TabIndex = 65;
            this.btnXoaanh.UseVisualStyleBackColor = false;
            this.btnXoaanh.Click += new System.EventHandler(this.btnXoaanh_Click);
            // 
            // btnChinhSua
            // 
            this.btnChinhSua.AutoSize = true;
            this.btnChinhSua.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnChinhSua.BackgroundImage = global::GUI.Properties.Resources.Chinhsua;
            this.btnChinhSua.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnChinhSua.FlatAppearance.BorderSize = 0;
            this.btnChinhSua.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChinhSua.Location = new System.Drawing.Point(922, 681);
            this.btnChinhSua.Name = "btnChinhSua";
            this.btnChinhSua.Size = new System.Drawing.Size(140, 60);
            this.btnChinhSua.TabIndex = 55;
            this.btnChinhSua.UseVisualStyleBackColor = false;
            this.btnChinhSua.Click += new System.EventHandler(this.btnChinhSua_Click);
            // 
            // btnXuatThongtin
            // 
            this.btnXuatThongtin.AutoSize = true;
            this.btnXuatThongtin.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnXuatThongtin.BackgroundImage = global::GUI.Properties.Resources.Xuat;
            this.btnXuatThongtin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnXuatThongtin.FlatAppearance.BorderSize = 0;
            this.btnXuatThongtin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXuatThongtin.Location = new System.Drawing.Point(747, 681);
            this.btnXuatThongtin.Name = "btnXuatThongtin";
            this.btnXuatThongtin.Size = new System.Drawing.Size(140, 60);
            this.btnXuatThongtin.TabIndex = 54;
            this.btnXuatThongtin.UseVisualStyleBackColor = false;
            this.btnXuatThongtin.Click += new System.EventHandler(this.btnXuatThongtin_Click);
            // 
            // btnBack
            // 
            this.btnBack.AutoSize = true;
            this.btnBack.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBack.BackgroundImage = global::GUI.Properties.Resources.back;
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Location = new System.Drawing.Point(1068, 694);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(102, 47);
            this.btnBack.TabIndex = 53;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // ptbAvatar
            // 
            this.ptbAvatar.Image = global::GUI.Properties.Resources.avatar_unknow;
            this.ptbAvatar.Location = new System.Drawing.Point(972, 160);
            this.ptbAvatar.Name = "ptbAvatar";
            this.ptbAvatar.Size = new System.Drawing.Size(187, 177);
            this.ptbAvatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbAvatar.TabIndex = 23;
            this.ptbAvatar.TabStop = false;
            this.ptbAvatar.Click += new System.EventHandler(this.ptbAvatar_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::GUI.Properties.Resources.ThongtinKH;
            this.pictureBox3.Location = new System.Drawing.Point(348, 242);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(469, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 20;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::GUI.Properties.Resources.QuanliKH;
            this.pictureBox2.Location = new System.Drawing.Point(96, 186);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(469, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            // 
            // btnMenu
            // 
            this.btnMenu.AutoSize = true;
            this.btnMenu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnMenu.BackgroundImage = global::GUI.Properties.Resources._3_gach;
            this.btnMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMenu.FlatAppearance.BorderSize = 0;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Location = new System.Drawing.Point(21, 175);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(60, 61);
            this.btnMenu.TabIndex = 18;
            this.btnMenu.UseVisualStyleBackColor = false;
            // 
            // btnTrangChu
            // 
            this.btnTrangChu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTrangChu.BackgroundImage = global::GUI.Properties.Resources.logoTrangChu;
            this.btnTrangChu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTrangChu.FlatAppearance.BorderSize = 0;
            this.btnTrangChu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrangChu.Location = new System.Drawing.Point(21, 108);
            this.btnTrangChu.Name = "btnTrangChu";
            this.btnTrangChu.Size = new System.Drawing.Size(149, 61);
            this.btnTrangChu.TabIndex = 17;
            this.btnTrangChu.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GUI.Properties.Resources.tileQTV;
            this.pictureBox1.Location = new System.Drawing.Point(-2, -3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1186, 105);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::GUI.Properties.Resources.quanliKHmauxanhduong;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(160, 119);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(229, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 21;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::GUI.Properties.Resources.ThongtinKH1;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(348, 119);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(213, 50);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 22;
            this.pictureBox5.TabStop = false;
            // 
            // cmbLoai
            // 
            this.cmbLoai.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbLoai.FormattingEnabled = true;
            this.cmbLoai.Location = new System.Drawing.Point(713, 350);
            this.cmbLoai.Name = "cmbLoai";
            this.cmbLoai.Size = new System.Drawing.Size(162, 24);
            this.cmbLoai.TabIndex = 113;
            // 
            // cbPT
            // 
            this.cbPT.AutoSize = true;
            this.cbPT.Location = new System.Drawing.Point(809, 436);
            this.cbPT.Name = "cbPT";
            this.cbPT.Size = new System.Drawing.Size(18, 17);
            this.cbPT.TabIndex = 114;
            this.cbPT.UseVisualStyleBackColor = true;
            this.cbPT.CheckedChanged += new System.EventHandler(this.cbPT_CheckedChanged);
            // 
            // cmbMSHLV
            // 
            this.cmbMSHLV.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbMSHLV.FormattingEnabled = true;
            this.cmbMSHLV.Location = new System.Drawing.Point(703, 505);
            this.cmbMSHLV.Name = "cmbMSHLV";
            this.cmbMSHLV.Size = new System.Drawing.Size(255, 24);
            this.cmbMSHLV.TabIndex = 115;
            this.cmbMSHLV.Visible = false;
            // 
            // cmbGoiTapPT
            // 
            this.cmbGoiTapPT.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbGoiTapPT.FormattingEnabled = true;
            this.cmbGoiTapPT.Location = new System.Drawing.Point(703, 542);
            this.cmbGoiTapPT.Name = "cmbGoiTapPT";
            this.cmbGoiTapPT.Size = new System.Drawing.Size(255, 24);
            this.cmbGoiTapPT.TabIndex = 116;
            this.cmbGoiTapPT.Visible = false;
            // 
            // fThongTinKH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1182, 753);
            this.Controls.Add(this.cmbGoiTapPT);
            this.Controls.Add(this.cmbMSHLV);
            this.Controls.Add(this.cbPT);
            this.Controls.Add(this.cmbLoai);
            this.Controls.Add(this.btnXoaanh);
            this.Controls.Add(this.tbThoiHanPT);
            this.Controls.Add(this.tbTENPT);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.btnChinhSua);
            this.Controls.Add(this.btnXuatThongtin);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.tbNgayDK);
            this.Controls.Add(this.tbNgayHethan);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbGioiTinh);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbGHichu);
            this.Controls.Add(this.tbEMail);
            this.Controls.Add(this.tbSDT);
            this.Controls.Add(this.tbDiachi);
            this.Controls.Add(this.tbCCCD);
            this.Controls.Add(this.tbNgaysinh);
            this.Controls.Add(this.tbHoTen);
            this.Controls.Add(this.tbMaThẻ);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ptbAvatar);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnMenu);
            this.Controls.Add(this.btnTrangChu);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox5);
            this.Name = "fThongTinKH";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fThongTinKH";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.fThongTinKH_FormClosed);
            this.Load += new System.EventHandler(this.fThongTinKH_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.ptbAvatar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Button btnTrangChu;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox ptbAvatar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbMaThẻ;
        private System.Windows.Forms.TextBox tbHoTen;
        private System.Windows.Forms.TextBox tbNgaysinh;
        private System.Windows.Forms.TextBox tbCCCD;
        private System.Windows.Forms.TextBox tbDiachi;
        private System.Windows.Forms.TextBox tbSDT;
        private System.Windows.Forms.TextBox tbEMail;
        private System.Windows.Forms.TextBox tbGHichu;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbGioiTinh;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbNgayHethan;
        private System.Windows.Forms.TextBox tbNgayDK;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnXuatThongtin;
        private System.Windows.Forms.Button btnChinhSua;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbTENPT;
        private System.Windows.Forms.TextBox tbThoiHanPT;
        private System.Windows.Forms.Button btnXoaanh;
        private System.Windows.Forms.ComboBox cmbLoai;
        private System.Windows.Forms.CheckBox cbPT;
        private System.Windows.Forms.ComboBox cmbMSHLV;
        private System.Windows.Forms.ComboBox cmbGoiTapPT;
    }
}