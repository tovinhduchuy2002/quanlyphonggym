﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class fLoaiThietBi : Form
    {
        public fLoaiThietBi()
        {
            InitializeComponent();
        }
        ThietBiBLL tbbll = new ThietBiBLL();
        private List<THIETBI> lTB = new List<THIETBI>();
        public static string loaiTB { get; set; }
        private void fLoaiThietBi_Load(object sender, EventArgs e)
        {
            LoadList();
        }
        private void LoadList()
        {

            lTB = tbbll.xemDSTBtheoloai(loaiTB);
            int stt = 0;
            int countbt = 0;
            dtbDanhsachthietbi.Columns.Add("Column1", "STT");
            dtbDanhsachthietbi.Columns.Add("Column2", "MATHIETBI");
            dtbDanhsachthietbi.Columns.Add("Column3", "TENTHIETBI");
            dtbDanhsachthietbi.Columns.Add("Column4", "SOLUONG");
            dtbDanhsachthietbi.Columns.Add("Column5", "DONVI");
            dtbDanhsachthietbi.Columns.Add("Column6", "TINHTRANG");
            lbSLTB.Text = lTB.Count.ToString();
            lbLoaiTB.Text = loaiTB;

            foreach (THIETBI tb in lTB)
            {
                stt++;
                dtbDanhsachthietbi.Rows.Add(stt,tb.Mathietbi, tb.Tenthietbi, tb.Soluong, tb.Donvi, tb.Tinhtrang);
                if(tb.Tinhtrang == "BAOTRI")
                {
                    countbt++;
                }    
            }
            lbSLTBBT.Text = countbt.ToString();




        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void dtbDanhsachthietbi_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1 && e.RowIndex >= 0)
            {
                object value = dtbDanhsachthietbi.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                string maTB = (string)value;
                fThongtinTB ftttb = new fThongtinTB();
                fThongtinTB.maTB = maTB;
                this.Hide();
                ftttb.ShowDialog();
                this.Show();
            }
        }
    }
}
