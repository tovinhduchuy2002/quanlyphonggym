﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class GOITAP
    {
        private string magoitap;
        public string maGoiTap
        {
            get => magoitap;
            set => magoitap = value;
        }
        private int thang;
        public int Thang
        {
            get => thang;
            set => thang = value;
        }
        private int chiphi;
        public int ChiPhi
        {
            get => chiphi;
            set => chiphi = value;
        }
        private string ghichu;
        public string GhiChu
        {
            get => ghichu; 
            set => ghichu = value;
            
        }
        public GOITAP()
        {

        }
        ~GOITAP() { }
    }
}
