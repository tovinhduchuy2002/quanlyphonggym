﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using System.Globalization;
using BLL;
using ZXing.Common;
using ZXing;

namespace GUI
{
    public partial class fThongTinKH : Form
    {
        public static string  mathe { get; set; }
     
        KHBLL khbll = new KHBLL();
        KHACHHANG kh = new KHACHHANG();
        PictureBox ptb_copy = new PictureBox();
        GoiTapBLL gtbll = new GoiTapBLL();
        public fThongTinKH()
        {
            InitializeComponent();
            
        }
        private bool btnxoaClicked = false;
        private bool ptbClicked = false;
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void LoadThongTin()
        {
            DateTime datenow = DateTime.Now;
            kh = khbll.xemThongTinKH(mathe);
            tbHoTen.Text = kh.Hoten;
            tbMaThẻ.Text = kh.Mathe.ToString();
            tbMaThẻ.Enabled = false;
            tbNgaysinh.Text = kh.Ngaysinh;
            tbGioiTinh.Text = kh.Gioitinh;
            tbGioiTinh.Enabled = false;
            tbCCCD.Text = kh.CMND;
            tbDiachi.Text = kh.diaChi;
            tbSDT.Text = kh.SDT;
            tbEMail.Text = kh.Email;
            List<string> listGoitap = gtbll.xemMaGoitap(kh.Gioitinh);
            foreach (string item in listGoitap)
            {
                cmbLoai.Items.Add(item);
            }
            cmbLoai.SelectedItem = kh.maGoiTap;
            List<string> listGoitapPT = gtbll.xemMaGoitap("PT");
            foreach (string item in listGoitapPT)
            {
                cmbGoiTapPT.Items.Add(item);

            }

            List<string> listHLV = khbll.xemMAHLV("HUANLUYENVIEN");
            foreach (string item in listHLV)
            {
                cmbMSHLV.Items.Add(item);
            }
            cmbGoiTapPT.SelectedItem = kh.GoitapPT;
            cmbMSHLV.SelectedItem = kh.maNhanVien;
            tbGHichu.Text = kh.GhiChu;
            tbNgayDK.Text = kh.NgayBatDau.Date.ToString("dd/MM/yyyy");
            tbNgayHethan.Text = kh.NgayHetHan.Date.ToString("dd/MM/yyyy");
            tbThoiHanPT.Text = kh.ThoiHan.Date.ToString("dd/MM/yyyy");
            tbTENPT.Text = kh.TenPT.ToString();
            tbTENPT.Enabled = false;
            cmbGoiTapPT.SelectedItem = kh.GoitapPT.ToString();
            cmbMSHLV.SelectedItem = kh.maNhanVien.ToString();



            if(kh.ThoiHan == new DateTime(2000,1,1))
            {
                tbThoiHanPT.Text = "0";
            }
            using (MemoryStream ms = new MemoryStream(kh.Anh))
            {
                ptbAvatar.Image = Image.FromStream(ms);
            }
               
            
            
            
            
        }

       
        private void XoathongtinKH()
        {
            bool check = khbll.xoathongtinKH(mathe);
            if ( check == true)
            {
                MessageBox.Show("Xóa thất bại");
            }
            else
            {
                MessageBox.Show("Xóa thành công");
            }
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn có chắc chắn muốn tiếp tục?", "Xác nhận", MessageBoxButtons.OKCancel);
            {
                if (result == DialogResult.OK)
                {
                    XoathongtinKH();
                    this.Hide();
                }
                
                
                    
                
            }
        }

        private void fThongTinKH_FormClosed(object sender, FormClosedEventArgs e)
        {
           
        }

        private void fThongTinKH_Load_1(object sender, EventArgs e)
        {
            DateTime datenow = DateTime.Now;
            
            LoadThongTin();
            if (kh.NgayHetHan < datenow)
            {
                MessageBox.Show("Khách hàng đã hết hạn sử dụng");
            }
        }

        private void btnChinhSua_Click(object sender, EventArgs e)
        {
            
            if (!string.IsNullOrEmpty(tbHoTen.Text))
            { kh.Hoten = tbHoTen.Text; }
            if (!string.IsNullOrEmpty(tbNgaysinh.Text))
            { kh.Ngaysinh = tbNgaysinh.Text; }
            if (!string.IsNullOrEmpty(tbCCCD.Text))
            { kh.CMND =tbCCCD.Text; }
            if (!string.IsNullOrEmpty(tbDiachi.Text))
            { kh.diaChi = tbDiachi.Text; }
            if (!string.IsNullOrEmpty(tbSDT.Text))
            { kh.SDT = tbSDT.Text; }
            if (!string.IsNullOrEmpty(tbEMail.Text))
            { kh.Email = tbEMail.Text; }
            if (!string.IsNullOrEmpty(tbGHichu.Text))
            { kh.GhiChu = tbGHichu.Text; }
            if (cmbLoai.SelectedIndex != -1)
            {
                kh.maGoiTap = cmbLoai.SelectedItem.ToString();

            }
            else
            {
                MessageBox.Show("Vui lòng chọn gói tập");
                return;

            }
            if (cbPT.Checked == true)
            {
                if (cmbGoiTapPT.SelectedIndex != -1)
                {
                    kh.GoitapPT = cmbGoiTapPT.SelectedItem.ToString();

                }
                else
                {
                    MessageBox.Show("Vui lòng chọn gói tập PT ");
                    return;
                }
                if (cmbMSHLV.SelectedIndex != -1)
                {
                    kh.maNhanVien = cmbMSHLV.SelectedItem.ToString();
                }
                else
                {
                    MessageBox.Show("Vui lòng chọn gói mã số Huấn Luyện Viên ");
                    return;
                }
            }
            
            if (tbNgayDK.Text != kh.NgayBatDau.Date.ToString("dd/MM/yyyy"))
            {
                
                if (!string.IsNullOrEmpty(tbNgayDK.Text))
                {
                    DateTime thamchieu;
                    if (DateTime.TryParseExact(tbNgayDK.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thamchieu))
                    {

                        if (thamchieu < kh.NgayHetHan)
                        {
                            MessageBox.Show("Nhập lại ngày đăng kí gói tập");
                            return;
                        }
                        else
                        {
                            kh.NgayBatDau = thamchieu;
                            kh.NgayHetHan = kh.NgayBatDau.AddMonths(gtbll.LoaiGoiTap(kh.maGoiTap));


                        }
                    }
                    else
                    {
                        MessageBox.Show("Nhập lại ngày đăng kí");
                        return;

                    }

                }
            }
            
            if (!string.IsNullOrEmpty(tbTENPT.Text))
            { kh.TenPT = tbTENPT.Text; }
            if(!string.IsNullOrEmpty(tbThoiHanPT.Text))
            {
                
                    DateTime thamchieu;
                    if (DateTime.TryParseExact(tbNgayDK.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thamchieu))
                    {

                        kh.ThoiHan = thamchieu;
                    }
                    else
                    {
                        MessageBox.Show("Nhập lại ngày hết hạn PT ");
                        return;

                    }
                
            }    
           
            


            if (ptbClicked == true)
            {
                kh.Anh = imageToByteArray(ptbAvatar);

            }
            else if (btnxoaClicked == true)
            {
                kh.Anh = imageToByteArray(ptbAvatar);
            }
            string updateKH = khbll.checkcapnhatKH(kh);
            switch(updateKH)
            {
                case "success":
                    MessageBox.Show("Thay đổi thành công");
                    return;
                case "fail":
                    MessageBox.Show("Thay đổi thất bại");
                    return;
            }
            
         
            
        }

        private void btnXoaanh_Click(object sender, EventArgs e)
        {
            btnxoaClicked = true;
            
            ptbAvatar.ImageLocation = "C:/ path / to / avatar_unknow.jpg";

        }
        

        private void ptbAvatar_Click(object sender, EventArgs e)
        {   
            
            OpenFileDialog moFile = new OpenFileDialog();
            moFile.Title = "Chọn ảnh khách hàng";
            moFile.Filter = "Image Files(*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf)|*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf";
            if (moFile.ShowDialog() == DialogResult.OK)
            {
                ptbClicked = true;
                
                ptbAvatar.ImageLocation = moFile.FileName;
               

            }
        }
        private byte[] imageToByteArray(PictureBox ptb)
        {
            using (MemoryStream ms = new MemoryStream())
            {
    
                ptb.Image.Save(ms, ptb.Image.RawFormat); // Thay đổi định dạng ảnh nếu cần thiết
                return ms.ToArray();
            }
        }
        private void SaveQRCodeToFile()
        {
            string maT = mathe;
            // tạo mã QR
            BarcodeWriter writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            EncodingOptions options = new EncodingOptions
            {
                Width = 300,
                Height = 300
            };
            writer.Options = options;
            Bitmap qrCodeImage = writer.Write(maT);
            // Hiển thị SaveFileDialog để chọn vị trí và tên file
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "PNG Image|*.png";
            saveFileDialog.Title = "Save QR Code";
            saveFileDialog.FileName = "QRCode_"+maT+".png";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Lưu file
                string filePath = saveFileDialog.FileName;
                qrCodeImage.Save(filePath, System.Drawing.Imaging.ImageFormat.Png);

                MessageBox.Show("QR Code saved successfully!");
            }
        }
        private void btnXuatThongtin_Click(object sender, EventArgs e)
        {
            SaveQRCodeToFile();
        }

        private void cbPT_CheckedChanged(object sender, EventArgs e)
        {
            cbPT = (CheckBox)sender;
            if (cbPT.Checked)
            {
                label16.Visible = true;
                label17.Visible = true;
                label18.Visible = true;
                label19.Visible = true;
                tbTENPT.Visible = true;
                tbThoiHanPT.Visible = true;
                cmbGoiTapPT.Visible = true;
                cmbMSHLV.Visible = true;
                
            }
            else
            {
                label16.Visible = false;
                label17.Visible = false;
                label18.Visible = false;
                label19.Visible = false;
                tbThoiHanPT.Visible = false;
                cmbGoiTapPT.Visible = false;
                tbTENPT.Visible = false;
                cmbMSHLV.Visible = false;

            }
        }
    }
}
