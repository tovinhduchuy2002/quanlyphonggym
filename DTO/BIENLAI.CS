﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class BIENLAI
    {
        private string mabienlai;
        public string MaBienLai
        {
            get => mabienlai;
            set => mabienlai = value;
        }
        private string mathe;
        public string MaThe
        {
            get => mathe;
            set => mathe = value;
        }
        private string magoitap;
        public string MaGoiTap
        {
            get => magoitap;
            set => magoitap = value;
            
        }
        private string goitapPT;
        public string GoiTapPT
        {
            get => goitapPT;
            set => goitapPT = value;
        }
        private int tongtien;
        public int TongTien
        {
            get => tongtien;
            set => tongtien = value;
        }
        private DateTime ngaythanhtoan;
        public DateTime NgayThanhToan
        {
            get => ngaythanhtoan;
            set => ngaythanhtoan = value;

        }
        public BIENLAI()
        {
            
        }
        ~BIENLAI() { }


    }
}
