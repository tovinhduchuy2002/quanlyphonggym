﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

using DTO;
namespace DAL
{
    public class KhachHangAccess:DatabaseAccess
    {   
        public List<string> xemListMathe()
        {
            return DatabaseAccess.xemListMaThe();
        }

        public int demKH()
        {
            return DatabaseAccess.demsoKH();
        }
        public string UpdateKH(KHACHHANG kh)
        {
            string updateKH = DatabaseAccess.UpdatethongtinKH(kh);
            return updateKH;
        }
        public List<KHACHHANG> xemKH()
        {
            List<KHACHHANG> lKH = new List<KHACHHANG>();
            lKH = DatabaseAccess.XemKH();
            return lKH;
        }
        public KHACHHANG xemthongTinKH(string mathe)
        {
            KHACHHANG kh = DatabaseAccess.xemThongTinKH(mathe);

            return kh;
        }
        public bool ThemDuLieu(KHACHHANG kh)
        {
            return themTaiKhoan(kh);
        }
        public bool xoaThontinKH(string maTHE)
        {
            bool xoathongtin = DatabaseAccess.XoaThongtinKH(maTHE);
            return xoathongtin;
        }
        public List<KHACHHANG> thongkeKH(DateTime d1, DateTime d2)
        {
            List<KHACHHANG> lKH = new List<KHACHHANG>();
            lKH = DatabaseAccess.thongkeKhachHang(d1,d2);
            return lKH;
        }
        public List<string> xemmaHLV(string CHUCVU)
        {
            return DatabaseAccess.xemMAHLV(CHUCVU);
        }
            

    }
}
