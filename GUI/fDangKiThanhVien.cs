﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using iText;
using iText.Kernel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using System.Globalization;
using BLL;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Kernel.Geom;
using iText.License;

namespace GUI
{
    public partial class fDangKiThanhVien : Form
    {
        public fDangKiThanhVien()
        {
            InitializeComponent();
        }
        TaiKhoanBLL tkbll = new TaiKhoanBLL();
        KHBLL kHBLL = new KHBLL();
        BienlaiBLL blbll = new BienlaiBLL();
        GoiTapBLL gtbll = new GoiTapBLL();
        KHACHHANG kh = new KHACHHANG();
        private int luukh = 0;
        private int xuatbienlai = 0;
        private void btnBack_Click(object sender, EventArgs e)
        {
            if(luukh == 0)
            {
                this.Hide();
            }
            else
            {
                if (xuatbienlai != 0)
                {
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Chưa xuất biên lai cho khách hàng");
                    return;
                }
            }
        }
        ImageFormat imgF;

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnLuu_Click(object sender, EventArgs e)
        {

          
            if (!string.IsNullOrEmpty(tbHoTen.Text))
            { kh.Hoten = tbHoTen.Text; }           
            if (!string.IsNullOrEmpty(tbNgaysinh.Text))
            { kh.Ngaysinh = tbNgaysinh.Text; }
            if (cmbGioiTinh.SelectedIndex == -1)
            {
                MessageBox.Show("Vui lòng chọn giới tính");
                return;
            }
           if(cmbLoai.SelectedIndex != -1)
            {
                kh.maGoiTap = cmbLoai.SelectedItem.ToString();
           
            }
           else
            {
                MessageBox.Show("Vui lòng chọn gói tập");
                return;

            }
            if (cbPT.Checked == true)
            {
                if(cmbGoiTapPT.SelectedIndex != -1)
                {
                    kh.GoitapPT = cmbGoiTapPT.SelectedItem.ToString();
                    
                }
                else
                {
                    MessageBox.Show("Vui lòng chọn gói tập PT ");
                    return;
                }
                if(cmbMSHLV.SelectedIndex != -1)
                {
                    kh.maNhanVien = cmbMSHLV.SelectedItem.ToString();
                }
                else
                {
                    MessageBox.Show("Vui lòng chọn gói mã số Huấn Luyện Viên ");
                    return;
                }
            }    
            if (!string.IsNullOrEmpty(tbCCCD.Text))
            { kh.CMND = tbCCCD.Text; }
            if (!string.IsNullOrEmpty(tbDiachi.Text))
            { kh.diaChi = tbDiachi.Text; }
            if (!string.IsNullOrEmpty(tbSDT.Text))
            { kh.SDT = tbSDT.Text; }
            if (!string.IsNullOrEmpty(tbEMail.Text))
            { kh.Email = tbEMail.Text; }
            if (!string.IsNullOrEmpty(tbGHichu.Text))
            { kh.GhiChu = tbGHichu.Text; }
            
            
            if (!string.IsNullOrEmpty(tbNgayDK.Text))
            {
                DateTime thamchieu;
       
                if (DateTime.TryParseExact(tbNgayDK.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture,DateTimeStyles.None, out thamchieu))
                {
                    kh.NgayBatDau = thamchieu;
                    kh.NgayHetHan = kh.NgayBatDau.AddMonths(gtbll.LoaiGoiTap(kh.maGoiTap));
                }
                else
                {
                    MessageBox.Show("Nhập lại ngày đăng kí ");
                    return;
                    
                }  
                
            
            }
            
           
           
            if (!string.IsNullOrEmpty(tbThoiHan.Text))
            {
                DateTime thamchieu;
                if (DateTime.TryParse(tbThoiHan.Text, out thamchieu))
                {
                    kh.ThoiHan = thamchieu;
                   
                }
                else
                {
                    MessageBox.Show("Nhập lại Thời hạn ");
                    return;

                }
                
            }
            kh.Anh = imageToByteArray(ptbAvatar);
            string getupdate = kHBLL.CheckthemKH(kh);



            switch (getupdate)
            {
                
                case "success":
                    MessageBox.Show("Thêm thành công ");
                    luukh++;
                    return;
                case "fail":
                    MessageBox.Show("Trùng mã thẻ rồi");
                    return;
                case "Nhapnbd":
                    MessageBox.Show("Vui lòng nhập ngày bắt đầu");
                    return;
                case "Sailoai":
                    MessageBox.Show("Sai gói tập rồi");
                    return;

            }

        }

        private void cbPT_CheckedChanged(object sender, EventArgs e)
        {
            cbPT = (CheckBox)sender;
            if(cbPT.Checked)
            {
                
                label17.Visible = true;
                label18.Visible = true;
                label19.Visible = true;
                tbThoiHan.Visible = true;
                cmbGoiTapPT.Visible = true;
                cmbMSHLV.Visible = true;
                List<string> listGoitap = gtbll.xemMaGoitap("PT");
                foreach (string item in listGoitap)
                {
                    cmbGoiTapPT.Items.Add(item);
                }
                List<string> listHLV = kHBLL.xemMAHLV("HUANLUYENVIEN");
                foreach (string item in listHLV)
                {
                    cmbMSHLV.Items.Add(item);
                }

            }
            else
            {            
                label17.Visible = false;
                label18.Visible = false;
                label19.Visible = false;
                tbThoiHan.Visible = false;
                cmbGoiTapPT.Visible = false;

            }
            
        }
        private byte[] imageToByteArray(PictureBox ptb)
        {
            using (MemoryStream ms = new MemoryStream())
            {

                ptb.Image.Save(ms, ptb.Image.RawFormat); // Thay đổi định dạng ảnh nếu cần thiết
                return  ms.ToArray();
            }
        }

        private void ptbAvatar_Click(object sender, EventArgs e)
        {
            OpenFileDialog moFile = new OpenFileDialog();
            moFile.Title = "Chọn ảnh khách hàng";
            moFile.Filter = "Image Files(*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf)|*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf";
            if(moFile.ShowDialog()==DialogResult.OK )
            {
                
                ptbAvatar.ImageLocation = moFile.FileName;
                

            }

        }

        private void cmbGioiTinh_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbGioiTinh.SelectedItem.ToString() == "NAM")
            {
                kh.Gioitinh = "NAM";
                cmbLoai.Items.Clear();
                List<string> listGoitap = gtbll.xemMaGoitap(kh.Gioitinh);
                foreach (string item in listGoitap)
                {
                    cmbLoai.Items.Add(item);
                }
            }
            else
            {
                kh.Gioitinh = "NU";
                cmbLoai.Items.Clear();
                List<string> listGoitap = gtbll.xemMaGoitap(kh.Gioitinh);
                foreach (string item in listGoitap)
                {
                    cmbLoai.Items.Add(item);
                }
            }
        }

        private void cmbMSHLV_SelectedValueChanged(object sender, EventArgs e)
        {
           
        }

        private void cmbGoiTapPT_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void btnXuatBienLai_Click(object sender, EventArgs e)
        {
            if (luukh == 0)
            {
                MessageBox.Show("Vui lòng lưu thông tin khách hàng");
                return;
            }
            else
            {
                BIENLAI pienlai = new BIENLAI();
                pienlai = blbll.xemBienlai();
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "Chọn đường dẫn lưu biên lai";
                saveFileDialog.Filter = "Tệp tin PDF|*.pdf";
                BIENLAI bienlai = new BIENLAI();


                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {

                    string path = saveFileDialog.FileName;
                    PdfDocument pdfDOc = new PdfDocument(new PdfWriter(path));
                    Document doc = new Document(pdfDOc);
                    Paragraph heading = new Paragraph("BIEN LAI")
                    .SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER)
                    .SetFontSize(50);
                    
                    doc.Add(heading);
                    Paragraph mabl = new Paragraph("MA BIEN LAI: "+pienlai.MaBienLai);
                    doc.Add(mabl);
                    Paragraph magoitap = new Paragraph("MA GOI TAP: "+pienlai.MaGoiTap);
                    doc.Add(magoitap);
                    if(pienlai.GoiTapPT != "rong")
                    {
                        Paragraph goitappt = new Paragraph("MA GOI TAP PT: " + pienlai.GoiTapPT);
                        doc.Add(goitappt);
                    }
                    
                    Paragraph tongtien = new Paragraph("TONG SO TIEN THANH TOAN: "+pienlai.TongTien.ToString()+" vnd");
                    doc.Add(tongtien);
                    

                    
                   
                   
                    Paragraph kiten = new Paragraph("NGAY THANH TOAN " + pienlai.NgayThanhToan.Date.ToString("dd/MM/yyyy")
                                                     ).SetTextAlignment(iText.Layout.Properties.TextAlignment.LEFT);

                                                            
               
                            
                   
                    doc.Add(kiten);
                    Paragraph phonggym = new Paragraph("----------- PHONG GYM SUC SONG MOI ----------- ").SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                    doc.Add(phonggym);
                    doc.Close();
                    pdfDOc.Close();
                    xuatbienlai++;





                }
                else
                {
                    MessageBox.Show("Đường dẫn không hợp lệ");
                    return;
                }
            }
        }
    }
}
