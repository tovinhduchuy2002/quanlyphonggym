﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class KHBLL
    {
        KhachHangAccess khaccs = new KhachHangAccess();
        GoiTapAccess gtaccs = new GoiTapAccess();
        BienlaiBLL bienlaibll = new BienlaiBLL();
        public List<string> xemListMaThe()
        {
            return khaccs.xemListMathe();
        }
        public bool themdulieu(KHACHHANG kh)
        {
            kh.Mathe = MaKHAuto();
            bool checking = khaccs.ThemDuLieu(kh);
            if(checking == true)
            {
                bienlaibll.capnhatBienlai(kh);
                return checking;
            }
            else
            {
                return checking;
            }    

            

        }
        private string MaKHAuto()
        {
            string makh = "";
            int dem = khaccs.demKH() + 1;
            if(dem < 10)
            {
                makh = "KH00000" + dem.ToString();
            }
            else if (dem >= 10 && dem < 100)
            {
                makh = "KH0000" + dem.ToString();
            }
            else if (dem >= 100 && dem < 1000)
            {
                makh = "KH000" + dem.ToString();
            }
            else if (dem >= 1000 && dem < 10000)
            {
                makh = "KH00" + dem.ToString();
            }
            else if (dem >= 10000 && dem < 100000)
            {
                makh = "KH0" + dem.ToString();
            }
            else
            {
                makh = "KH" + dem.ToString();
            }    
            return makh;
        }
        public string checkcapnhatKH(KHACHHANG kh)
        {
            string updatekh = khaccs.UpdateKH(kh);
            return updatekh;
        }
        public string CheckthemKH(KHACHHANG kh)
        {

            if (kh.NgayBatDau.Date == new DateTime(2000, 1, 1))
            {
                return "Nhapnbd";
                
                
            }
            else
            {
                if (gtaccs.KiemTraGoiTap(kh.maGoiTap) == true)
                {
                    return "Sailoai";
                }
                else 
                {
                    bool checking = themdulieu(kh);
                    if (checking == true)
                    {
                        return "success";
                    }
                    else
                    {
                        return "fail";
                    }
                }
            }
            


        }
        public List<KHACHHANG> xemKH()
        {
            List<KHACHHANG> lkh = new List<KHACHHANG>();
            lkh = khaccs.xemKH();
            return lkh;
        }
        public KHACHHANG xemThongTinKH(string mathe)
        {
            KHACHHANG kh = new KHACHHANG();
            kh = khaccs.xemthongTinKH(mathe);
            return kh;
        }
        public bool xoathongtinKH(string maTHE)
        {
            bool xoathongtin = khaccs.xoaThontinKH(maTHE);
            return xoathongtin;
        }
        public List<KHACHHANG> thongkeKH(DateTime d1, DateTime d2)
        {
            List<KHACHHANG> lkh = new List<KHACHHANG>();
            lkh = khaccs.thongkeKH(d1,d2);
            return lkh;
        }
       public List<string> xemMAHLV(string chucvu)
        {
            return khaccs.xemmaHLV(chucvu);
        }
    }
}
