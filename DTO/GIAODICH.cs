﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DTO
{
    public class GIAODICH
    {
        private string magiaodich;
        public string MaGD
        {
            get => magiaodich;
            set => magiaodich = value;
        }
        private string loaichiphi;
        public string LoaiChiPhi
        {
            get => loaichiphi;
            set => loaichiphi = value;
        }
        private long chiphi;
        public long ChiPhi
        {
            get => chiphi;
            set => chiphi = value;
        }
        private string masanpham;
        public string MaSP
        {
            get => masanpham;
            set => masanpham = value;
        }
        private string mathietbi;
        public string MaTB
        {
            get => mathietbi;
            set => mathietbi = value;
        }
        private string mota;
        public string MoTa
        {
            get => mota;
            set => mota = value;
        }
        private DateTime ngaygiaodich;
        public DateTime NgayGD
        {
            get => ngaygiaodich;
            set => ngaygiaodich = value;
        }
        public GIAODICH()
        {
            ngaygiaodich = DateTime.Now;
            MaSP = "RONG";
            MaTB = "RONG";
        }
        ~GIAODICH() { }
    }
}
