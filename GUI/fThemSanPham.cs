﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using System.Globalization;
using BLL;

namespace GUI
{
    public partial class fThemSanPham : Form
    {
        public fThemSanPham()
        {
            InitializeComponent();
        }
        SanPham sp = new SanPham();
        SanPhamBLL spbll = new SanPhamBLL();
        private byte[] imageToByteArray(PictureBox ptb)
        {
            using (MemoryStream ms = new MemoryStream())
            {

                ptb.Image.Save(ms, ptb.Image.RawFormat); // Thay đổi định dạng ảnh nếu cần thiết
                return ms.ToArray();
            }
        }

        private void ptbSanPham_Click(object sender, EventArgs e)
        {
            OpenFileDialog moFile = new OpenFileDialog();
            moFile.Title = "Chọn ảnh khách hàng";
            moFile.Filter = "Image Files(*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf)|*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf";
            if (moFile.ShowDialog() == DialogResult.OK)
            {
                
                ptbSanPham.ImageLocation = moFile.FileName;
            }
        }

       

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(cmbLoaiSP.SelectedIndex == -1)
            {
                MessageBox.Show("Chọn loại sản phẩm");
                return;
            }
            if(string.IsNullOrEmpty(tbTenSP.Text))
            {
                MessageBox.Show("Điền tên Sản Phẩm");
                return;
            }
            else
            {
                sp.TenSP = tbTenSP.Text;
            }
            if(string.IsNullOrEmpty(tbGiaBan.Text))
            {
                MessageBox.Show("Điền giá sản phẩm");
                return;
            }
            else
            {
                
                    int thamso;

                    if (int.TryParse(tbGiaBan.Text, out thamso) == true)
                    {
                        if(thamso >= 0)
                        {
                            sp.GiaThanh = thamso;
                        }
                        else
                        {
                            MessageBox.Show("Giá bán không thể âm");
                            return;
                        }

                    }
                    else
                    {
                        MessageBox.Show("Giá tiền phải là số nguyên dương ");
                        return;
                    }
                        

                
               
                
            }

            if (string.IsNullOrEmpty(tbSL.Text))
            {
                MessageBox.Show("Điền số lượng sản phẩm");
                return;
            }
            else
            {
                
                    int thamso;
          
                    if(int.TryParse((tbSL.Text),out thamso)== true)
                    {
                        
                        if(thamso >=0)
                        {
                            sp.SL = thamso;
                        }
                        else
                        {
                        MessageBox.Show("Số lượng không thể âm");
                        return;
                    }
                    }
                    else
                    {
                        MessageBox.Show("Số lượng phải là số nguyên dương ");
                        return;
                    }

               
              

            }
            sp.Anh = imageToByteArray(ptbSanPham);
            bool getsp = spbll.themSP(sp);
            if(getsp == true)
            {
                MessageBox.Show("Thêm sản phẩm thành công");
                return;
            }
            else
            {
                MessageBox.Show("Thêm sản phẩm thất bại");
                return;

            }
        }

        private void cmbLoaiSP_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbLoaiSP.SelectedItem == "Thực phẩm")
            {
                sp.LoaiSP = "THUCPHAM";
            }
            if (cmbLoaiSP.SelectedItem == "Dụng cụ hỗ trợ")
            {
                sp.LoaiSP = "DUNGCU";
            }
        }
    }
}
