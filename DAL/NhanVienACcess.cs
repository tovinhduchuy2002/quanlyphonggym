﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

using DTO;

namespace DAL
{
    public class NhanVienACcess:DatabaseAccess
    {
        public string UpdateNV(NHANVIEN nv)
        {
            string updateNV = DatabaseAccess.UpdatethongtinNV(nv);
            return updateNV;
        }
        public int demsoNV(string chucvu)
        {
            return DatabaseAccess.demsoNV(chucvu);
        }
        public List<NHANVIEN> xemLeTan()
        {
            List<NHANVIEN> lnv = new List<NHANVIEN>();
            lnv = DatabaseAccess.xemNhanVien();
            return lnv;
        }
        public List<NHANVIEN> xemHuanLuyenVien()
        {
            List<NHANVIEN> lhlv = new List<NHANVIEN>();
            lhlv = DatabaseAccess.xemHuanLuyenVien();
            return lhlv;
        }
        public NHANVIEN xemthongTinNV(string manv)
        {
            NHANVIEN nv = DatabaseAccess.xemThongTinNV(manv);

            return nv;
        }
        public NHANVIEN xemthongTinHLV(string manv)
        {
            NHANVIEN hlv = DatabaseAccess.xemThongTinNV(manv);

            return hlv;
        }
        public bool ThemNV(NHANVIEN nv)
        {
            return DatabaseAccess.themNhanVien(nv);
        }
        public bool xoaNV(string manv)
        {
            return DatabaseAccess.XoaThongtinNV(manv);
        }
    }
}
