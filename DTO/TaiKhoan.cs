﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class TaiKhoan
    {
        private string maTK;
        public string maTaiKhoan
        {
            get => maTK;
            set => maTK = value;
        }
        private string matkhau;
        public string MatKhau
        {
            get => matkhau;
            set => matkhau = value;
        }
        private int loaiTK;
        public int LoaiTK
        {
            get => loaiTK;
            set => loaiTK = value;
        }
        public TaiKhoan()
        {
            loaiTK = 2;
        }
        ~TaiKhoan() { }




    }
}
