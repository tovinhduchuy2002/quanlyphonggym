﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;
namespace GUI
{
    public partial class fDanhSachHLV : Form
    {
        TaiKhoanBLL tkbll = new TaiKhoanBLL();
        NhanVienBLL nvbll = new NhanVienBLL();
        public fDanhSachHLV()
        {
            InitializeComponent();
        }
        private void LoadList()
        {
            List<NHANVIEN> lHLV = new List<NHANVIEN>();
            lHLV = nvbll.xemHuanLuyenVien();

            dtgHuanLuyenVien.Columns.Add("Column1", "MANHANVIEN");
            dtgHuanLuyenVien.Columns.Add("Column2", "HOTEN");
            dtgHuanLuyenVien.Columns.Add("Column3", "SDT");
            dtgHuanLuyenVien.Columns.Add("Column4", "CMND");
            dtgHuanLuyenVien.Columns.Add("Column5", "CHUCVU");
            foreach (NHANVIEN nhanvien in lHLV)
            {
                dtgHuanLuyenVien.Rows.Add(nhanvien.maNhanVien, nhanvien.Hoten, nhanvien.SDT, nhanvien.CMND, nhanvien.Chucvu);
            }
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void fDanhSachHLV_Load(object sender, EventArgs e)
        {
            LoadList();
        }

       

        private void dtgHuanLuyenVien_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex >= 0)
            {
                object value = dtgHuanLuyenVien.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                string maNV = value.ToString();
                fThongtinHLV fttnv = new fThongtinHLV();
                fThongtinHLV.maNV = maNV;
                this.Hide();
                fttnv.ShowDialog();
                this.Show();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RefreshData();
        }
        private void RefreshData()
        {
            dtgHuanLuyenVien.Rows.Clear();
            List<NHANVIEN> lHLV = new List<NHANVIEN>();
            lHLV = nvbll.xemHuanLuyenVien();
            foreach (NHANVIEN nhanvien in lHLV)
            {
                dtgHuanLuyenVien.Rows.Add(nhanvien.maNhanVien, nhanvien.Hoten, nhanvien.SDT, nhanvien.CMND, nhanvien.Chucvu);
            }
        }
    }
}
