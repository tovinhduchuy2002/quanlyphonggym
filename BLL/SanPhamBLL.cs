﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class SanPhamBLL
    {
        SanPhamAccess spaccs = new SanPhamAccess();


        public bool themSP(SanPham sp)
        {
            int dem = spaccs.demSP(sp.LoaiSP) + 1;
            if(dem < 10)
            {
                sp.MaSP = sp.LoaiSP + "00" + dem.ToString();
            }
            else if(dem >= 10 && dem<100)
            {
                sp.MaSP = sp.LoaiSP + "0" + dem.ToString();
            }
            else
            {
                sp.MaSP = sp.LoaiSP + dem.ToString();
            }
            return spaccs.themSP(sp);
           
        }
        public List<SanPham> xemListSP()
        {
            return spaccs.xemListSP();
        }
        public List<SanPham> xemListSPtheoLoai(string loaisp)
        {
            return spaccs.xemListSPtheoLoai(loaisp);
        }
        public string updateSP(SanPham sp)
        {
            return spaccs.updateSP(sp);
        }
        public SanPham xemSP(string masp)
        {
            return spaccs.xemSP(masp);
        }





    }
}
