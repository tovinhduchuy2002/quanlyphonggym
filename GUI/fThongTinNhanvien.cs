﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using System.Globalization;
using BLL;
using ZXing;
namespace GUI
{
    public partial class fThongTinNhanvien : Form
    {


        NhanVienBLL nvbll = new NhanVienBLL();
        NHANVIEN lt = new NHANVIEN();
        public bool checkupdateanh = false;
        public static string maNV { get; set; }
        public fThongTinNhanvien()
        {
            InitializeComponent();
        }
        private void LoadThongTin()
        {
            
            lt = nvbll.xemThongTinNV(maNV);
            tbHoTen.Text = lt.Hoten;
            tbMaNV.Text = lt.maNhanVien.ToString();
            tbMaNV.Enabled = false;
            tbNgaysinh.Text = lt.Ngaysinh;
            tbGioiTinh.Text = lt.Gioitinh;
            tbCCCD.Text = lt.CMND.ToString();
            tbDiachi.Text = lt.diaChi;
            tbSDT.Text = lt.SDT.ToString();
            tbEMail.Text = lt.Email;
            tbGHichu.Text = lt.GhiChu;
            tbChucVu.Text = lt.Chucvu;
            tbChucVu.Enabled = false;
            using (MemoryStream ms = new MemoryStream(lt.Anh))
            {
                ptbAvatar.Image = Image.FromStream(ms);
            }

        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void fThongTinNhanvien_Load(object sender, EventArgs e)
        {
            LoadThongTin();
        }

        private void btnChinhSua_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbHoTen.Text))
            {
                MessageBox.Show("Điền họ tên");
                return;
            }
            else
            { lt.Hoten = tbHoTen.Text; }
            if (string.IsNullOrEmpty(tbNgaysinh.Text))
            {
                MessageBox.Show("Điền ngày sinh");
                return;
            }
            else
            { lt.Ngaysinh = tbNgaysinh.Text; }
            if (string.IsNullOrEmpty(tbGioiTinh.Text))
            {
                MessageBox.Show("Điền giới tính");
                return;
            }   
            else
            { lt.Gioitinh = tbGioiTinh.Text; }
            if (string.IsNullOrEmpty(tbCCCD.Text))
            {
                MessageBox.Show("Điền CCCD");
                return;
            }
            else
            { lt.CMND = (string)tbCCCD.Text; }
            if (string.IsNullOrEmpty(tbDiachi.Text))
            {
                MessageBox.Show("Điền địa chỉ");
                return;
            }
            else
            { lt.diaChi = tbDiachi.Text; }
            if (string.IsNullOrEmpty(tbSDT.Text))
            {
                MessageBox.Show("Điền SDT");
                return;
            }
            else
            { lt.SDT = (string)tbSDT.Text; }
            if (string.IsNullOrEmpty(tbEMail.Text))
            {
                MessageBox.Show("Điền Số EMAIL");
                return;
            }    
            else
            { lt.Email = tbEMail.Text; }
            if (string.IsNullOrEmpty(tbGHichu.Text))
            {
                MessageBox.Show("Điền ghi chú");
                return;
            }    
            else
            { lt.GhiChu = tbGHichu.Text; }
            if(checkupdateanh == true)
            {
                lt.Anh = imageToByteArray(ptbAvatar);
            }    
            
            string getupdate = nvbll.checkcapnhat(lt);
            switch (getupdate)
            {

                case "success":
                    MessageBox.Show("Thay đổi thành công");
                    return;
                case "fail":
                    MessageBox.Show("Thay đổi thất bại");
                    return;
            }





        }

        private void ptbAvatar_Click(object sender, EventArgs e)
        {
            OpenFileDialog moFile = new OpenFileDialog();
            moFile.Title = "Chọn ảnh khách hàng";
            moFile.Filter = "Image Files(*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf)|*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf";
            if (moFile.ShowDialog() == DialogResult.OK)
            {
                checkupdateanh = true;
                ptbAvatar.ImageLocation = moFile.FileName;
            }
        }
        private byte[] imageToByteArray(PictureBox ptb)
        {
            using (MemoryStream ms = new MemoryStream())
            {

                ptb.Image.Save(ms, ptb.Image.RawFormat); // Thay đổi định dạng ảnh nếu cần thiết
                return ms.ToArray();
            }
        }

        private void btnXuatThongtin_Click(object sender, EventArgs e)
        {

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            bool check = nvbll.XoaNV(maNV);
            if (check == true)
            {
                MessageBox.Show("Xóa thất bại");
            }
            else { MessageBox.Show("Xóa thành công"); }

        }
    }
}
