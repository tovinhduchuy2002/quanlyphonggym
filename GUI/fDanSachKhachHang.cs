﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using ZXing;
using ZXing.Common;
using System.Globalization;
using BLL;
using OfficeOpenXml;
namespace GUI
{
    public partial class fDanSachKhachHang : Form
    {
        public fDanSachKhachHang()
        {
            InitializeComponent();
        }
       
        TaiKhoanBLL tkbll = new TaiKhoanBLL();
        KHBLL khbll = new KHBLL();
        List<KHACHHANG> listExportKH = new List<KHACHHANG>();
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        
        private void LoadList()
        {
            List<KHACHHANG> lKH = new List<KHACHHANG>();
            lKH = khbll.xemKH();
            listExportKH = lKH.ToList();
            dtgKhachHang.Columns.Add("Column1", "MATHE");
            dtgKhachHang.Columns.Add("Column2", "HOTEN");
            dtgKhachHang.Columns.Add("Column3", "NGAYBATDAU");
            dtgKhachHang.Columns.Add("Column4", "NGAYHETHAN");
            dtgKhachHang.Columns.Add("Column5", "TENPT");
            foreach (KHACHHANG khachhang in lKH)
            {
                dtgKhachHang.Rows.Add(khachhang.Mathe, khachhang.Hoten, khachhang.NgayBatDau, khachhang.NgayHetHan, khachhang.TenPT);
            }
            
           
           
        }
        private void RefreshData()
        {
            dtgKhachHang.Rows.Clear();
            List<KHACHHANG> lKH = new List<KHACHHANG>();
            lKH = khbll.xemKH();
            foreach (KHACHHANG khachhang in lKH)
            {
                dtgKhachHang.Rows.Add(khachhang.Mathe, khachhang.Hoten, khachhang.NgayBatDau, khachhang.NgayHetHan, khachhang.TenPT);
            }
        }
        private void fDansachkhachhang_Load(object sender, EventArgs e)
        {
            LoadList();
        }

      

        private void button1_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void tbxTimKiem_TextChanged(object sender, EventArgs e)
        {


            string searchValue = tbxTimKiem.Text;

                foreach (DataGridViewRow row in dtgKhachHang.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        if (cell.Value != null && cell.Value.ToString().Contains(searchValue))
                        {
                            row.Selected = true;
                            dtgKhachHang.CurrentCell = row.Cells[0]; // Di chuyển đến ô đầu tiên của hàng
                            dtgKhachHang.FirstDisplayedScrollingRowIndex = row.Index; // Cuộn để hiển thị hàng đầu tiên
                            return;
                        }
                    }
                }
            
        }

        private void btnXuatThongtin_Click(object sender, EventArgs e)
        {
            string filepath = "";
            //Tạo saveDiaglouge
            SaveFileDialog save = new SaveFileDialog();
            //file filter
            save.Filter = "Excel Workbook|*.xlsx ";
            if(save.ShowDialog() == DialogResult.OK)
            {
                filepath = save.FileName;
            }
            if(string.IsNullOrEmpty(filepath))
            {
                MessageBox.Show("Đường dẫn không hợp lệ");
                return;
            }
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            try
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Properties.Author = "Sucsongmoi";
                    excel.Workbook.Properties.Title = "Danh sách khách hàng";
                    excel.Workbook.Worksheets.Add("Danh sách khách hàng");
                    ExcelWorksheet ws = excel.Workbook.Worksheets[0];
                    ws.Name = "Danh sách khách hàng";
                    ws.Cells.Style.Font.Size = 13;
                    ws.Cells.Style.Font.Name = "Tahoma";
                    string[] arrColumnheader =
                    {
                        "Mã thẻ",
                        "Họ tên",
                        "Ngày sinh",
                        "Giới tính",
                        "CMND",
                        "Địa chỉ",
                        "SDT",
                        "Email",
                        "Ghi chú",
                        "Loại",
                        "Ngày đăng kí",
                        "Ngày hết hạn",
                        "Tên PT",
                        "Mã nhân viên",
                        "Gói tập",
                        "Thời hạn",
                        
                    };
                    var countColumnHeader = arrColumnheader.Count();
                    //Merge va dat ten cho cell 1
                    ws.Cells[1, 1].Value = "THỐNG KÊ DANH SÁCH KHÁCH HÀNG";
                    ws.Cells[1, 1, 1, countColumnHeader].Merge = true;
                    ws.Cells[1, 1, 1, countColumnHeader].Style.Font.Bold = true;
                    ws.Cells[1, 1, 1, countColumnHeader].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    int cot = 1;
                    int hang = 2;
                    foreach(var item in arrColumnheader)
                    {
                        var cell = ws.Cells[hang, cot];
                        cell.Style.Font.Bold = true;
                        cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        // Tự động điều chỉnh độ rộng của ô dựa trên nội dung
                        cell.AutoFitColumns();

                        cell.Value = item;
                        cot++;
                    }
                    foreach(var item in listExportKH)
                    {
                        cot = 1;
                        hang++;
                        ws.Cells[hang, cot++].Value = item.Mathe;
                        ws.Cells[hang, cot++].Value = item.Hoten;
                        ws.Cells[hang, cot++].Value = item.Ngaysinh;
                        ws.Cells[hang, cot++].Value = item.Gioitinh;
                        ws.Cells[hang, cot++].Value = item.CMND;
                        ws.Cells[hang, cot++].Value = item.diaChi;
                        ws.Cells[hang, cot++].Value = item.SDT;
                        ws.Cells[hang, cot++].Value = item.Email;
                        ws.Cells[hang, cot++].Value = item.GhiChu;
                        ws.Cells[hang, cot++].Value = item.maGoiTap;
                        ws.Cells[hang, cot++].Value = item.NgayBatDau.ToShortDateString();
                        ws.Cells[hang, cot++].Value = item.NgayHetHan.ToShortDateString();
                        ws.Cells[hang, cot++].Value = item.TenPT;
                        ws.Cells[hang, cot++].Value = item.maNhanVien;
                        ws.Cells[hang, cot++].Value = item.GoitapPT;
                        ws.Cells[hang, cot++].Value = item.ThoiHan.ToShortDateString();
                     
                    }
                    // lu file
                    Byte[] bin = excel.GetAsByteArray();
                    File.WriteAllBytes(filepath, bin);

                }
                MessageBox.Show("Xuất file thành công ");

            }
            catch(Exception ee)
            {
                MessageBox.Show("Có lỗi khi lưu file excel"+ee.Message);
            }
        }
        private string ReadQRCodeFromFile(string filePath)
        {
            // Đọc hình ảnh từ file
            Bitmap qrCodeImage = new Bitmap(filePath);

            // Tạo đối tượng BarcodeReader và cấu hình nó
            BarcodeReader reader = new BarcodeReader();
            reader.Options.PossibleFormats = new BarcodeFormat[] { BarcodeFormat.QR_CODE };

            // Đọc mã QR từ hình ảnh
            Result result = reader.Decode(qrCodeImage);

            if (result != null )
            {
                return result.Text;
            }
            else
            {
                throw new Exception("Có lỗi gì đó đã xảy ra");
            }
        }
       
        private void btncheckKH_Click(object sender, EventArgs e)
        {
            // Hiển thị OpenFileDialog để chọn file
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "PNG Image|*.png";
            openFileDialog.Title = "Open QR Code Image";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;

                try
                {
                    // Đọc giá trị từ mã QR
                    string value = ReadQRCodeFromFile(filePath);
                    string maTHE = value.ToString();
                    fThongTinKH fthKH = new fThongTinKH();
                    fThongTinKH.mathe = maTHE;
                    this.Hide();
                    fthKH.ShowDialog();
                    this.Show();


                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Có lỗi xảy ra: {ex.Message}");
                    return;

                }
            }
        }

        private void dtgKhachHang_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex >= 0)
            {
                object value = dtgKhachHang.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                string maTHE = value.ToString();
                fThongTinKH fthKH = new fThongTinKH();
                fThongTinKH.mathe = maTHE;
                this.Hide();
                fthKH.ShowDialog();
                this.Show();
            }
        }
    }
}
