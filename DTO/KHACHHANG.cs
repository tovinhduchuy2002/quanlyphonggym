﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class KHACHHANG
    {
        private string mathe;
        public string Mathe
        {
            get => mathe;
            set => mathe = value;
        }
        private string maNV;
        public string maNhanVien
        {
            get => maNV;
            set => maNV = value;
        }
        private string hoten;
        public string Hoten
        {
            get => hoten;
            set => hoten = value;
        }
        private string ngaysinh;
        public string Ngaysinh
        {
            get => ngaysinh;
            set => ngaysinh = value;
        }
        private string gioitinh;
        public string Gioitinh
        {
            get => gioitinh;
            set => gioitinh = value;
        }
        private string diachi;
        public string diaChi
        {
            get => diachi;
            set => diachi = value;
        }
        private string sdt;
        public string SDT
        {
            get => sdt;
            set => sdt = value;
        }
        private string email;
        public string Email
        {
            get => email;
            set => email = value;
        }
        private string cmnd;
        public string CMND
        {
            get => cmnd;
            set => cmnd = value;
        }
        private string ghichu;
        public string GhiChu
        {
            get => ghichu;
            set => ghichu = value;
        }
        private string magoitap;
        public string maGoiTap
        {
            get => magoitap;
            set => magoitap = value;
        }
        private DateTime ngaybatdau;
        public DateTime NgayBatDau
        {
            get => ngaybatdau;
            set => ngaybatdau = value;
        }
        private DateTime ngayhethan;
        public DateTime NgayHetHan
        {
            get => ngayhethan;
            set => ngayhethan = value;
        }
        private DateTime thoihan;
        public DateTime ThoiHan
        {
            get => thoihan;
            set => thoihan = value;
        }
        private string tenpt;
        public string TenPT
        {
            get => tenpt;
            set => tenpt = value;
        }
        private string goitapPT;
        public string GoitapPT
        {
            get => goitapPT;
            set => goitapPT = value;
        }
        private Byte[] anh;
        public Byte[] Anh
        {
            get => anh;
            set => anh = value;
        }
        
           
        
        public KHACHHANG()
        {
            mathe = "0";
            hoten = "khong ";
            ngaysinh = "0/0/0";
            gioitinh = "khong";
            cmnd = "0";
            diachi = "Khong";
            email = "khong co";

            sdt = "0";
            tenpt = "khong co";
            ghichu = "khong co";
            ngaybatdau = new DateTime(2000, 1, 1);
            ngayhethan = new DateTime(2000, 1, 1);
            maNhanVien = "RONG";
            goitapPT = "rong";
      
            thoihan = new  DateTime(2000,1,1);


        }
        ~KHACHHANG() { }
            
    }
}
