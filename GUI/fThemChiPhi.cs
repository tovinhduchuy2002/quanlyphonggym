﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class fThemChiPhi : Form
    {
        public fThemChiPhi()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        GIAODICH gd = new GIAODICH();
        GiaoDichBLL gdbll = new GiaoDichBLL();
        List<string> listma = new List<string>();
        
        private void btnLuu_Click(object sender, EventArgs e)
        {
           if( cmbLoaiChiPhi.SelectedIndex == -1)
            {
                MessageBox.Show("Chọn chi phí");
                return;
            }
            if (gd.LoaiChiPhi == "Nhập hàng" || gd.LoaiChiPhi == "Bảo trì")
            {
                if (cmbMASPTB.SelectedIndex == -1)
                {
                    MessageBox.Show("Chọn mã SP/TB");
                    return;
                }
                else
                {
                    gd.MaSP = cmbMASPTB.SelectedItem.ToString();
                }    
            }
            if (string.IsNullOrEmpty(tbChiPhi.Text))
            {
                MessageBox.Show("Điền chi phí");
                return;
            }
            else
            {

                int thamso;

                if (int.TryParse((tbChiPhi.Text), out thamso) == true)
                {

                    if (thamso >= 0)
                    {
                        gd.ChiPhi = thamso;
                    }
                    else
                    {
                        MessageBox.Show("Chi phí không thể âm");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Chi phí phải là số nguyên dương ");
                    return;
                }




            }
            if(string.IsNullOrEmpty(tbMoTa.Text))
            {
                MessageBox.Show("Nhập mô tả");
                return;
            }
            else
            {
                gd.MoTa = tbMoTa.Text;
            }
            bool check = gdbll.themGiaodich(gd);
            if(check == true)
            {
                MessageBox.Show("Thêm giao dịch thành công");
                return;
            }
            else
            {
                MessageBox.Show("Thêm giao dịch thất bại");
                return;
            }    


        }
        

        private void cmbLoaiChiPhi_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbLoaiChiPhi.SelectedItem == "Nhập hàng")
            {
                cmbMASPTB.Enabled = true;
                gd.LoaiChiPhi = "NHAPHANG";
                 listma = gdbll.xemListMASP();
                cmbMASPTB.Items.Clear();
                foreach(string item in listma)
                {
                    cmbMASPTB.Items.Add(item);
                }
                
            }
            if (cmbLoaiChiPhi.SelectedItem == "Bảo trì")
            {
                cmbMASPTB.Enabled = true;
                gd.LoaiChiPhi = "BAOTRI";
                cmbMASPTB.Items.Clear();
                listma = gdbll.xemListMaTBBT();
                foreach (string item in listma)
                {
                    cmbMASPTB.Items.Add(item);
                }
            }
            if (cmbLoaiChiPhi.SelectedItem == "Duy trì")
            {
                cmbMASPTB.Enabled = false;
                gd.LoaiChiPhi = "DUYTRI";
            }
            if (cmbLoaiChiPhi.SelectedItem == "Lương")
            {
                cmbMASPTB.Enabled = false;
                gd.LoaiChiPhi = "LUONG";
            }
        }

        private void cmbMASPTB_SelectedValueChanged(object sender, EventArgs e)
        {

        }
    }
}
