﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class fQuanLiSP : Form
    {
        public fQuanLiSP()
        {
            InitializeComponent();
        }

        private void btnDSSP_Click(object sender, EventArgs e)
        {
            fDanhSachSP fdsp = new fDanhSachSP();
            this.Hide();
            fdsp.ShowDialog();
            this.Show();
        }

        private void btnBSP_Click(object sender, EventArgs e)
        {
            fBanSP fbsp = new fBanSP();
            this.Hide();
            fbsp.ShowDialog();
            this.Show();
        }

        private void btnThemSP_Click(object sender, EventArgs e)
        {
            fThemSanPham ftsp = new fThemSanPham();
            this.Hide();
            ftsp.ShowDialog();
            this.Show();
        }
    }
}
