﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class HoaDonBLL
    {
        HoaDonAccess hdaccs = new HoaDonAccess();
        public bool themHoaDon(HOADON hd)
        {
            int dem = hdaccs.demHoadon()+1;
            if (dem < 10)
            {
                hd.MaHoaDon = "HD00000" + dem.ToString();
            }
            else if (dem >= 10 && dem < 100)
            {
                hd.MaHoaDon = "HD0000" + dem.ToString();
            }
            else if (dem >= 100 && dem < 1000)
            {
                hd.MaHoaDon = "HD000" + dem.ToString();
            }
            else if (dem >= 1000 && dem < 10000)
            {
                hd.MaHoaDon = "HD00" + dem.ToString();
            }
            else if (dem >= 10000 && dem < 100000)
            {
                hd.MaHoaDon = "HD0" + dem.ToString();
            }
            else
            {
                hd.MaHoaDon = "HD" + dem.ToString();
            }
            return hdaccs.themHoaDon(hd);
        }

    }
   
    
}
