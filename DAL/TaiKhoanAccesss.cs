﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

using DTO;

namespace DAL
{   
    
    public class TaiKhoanAccesss:DatabaseAccess
    {
        private string ADmail;
        public string Encrypt(string mk)
        {
            int cost = 12;
            string cyper = BCrypt.Net.BCrypt.HashPassword(mk, cost);
            return cyper;
        }
        public string Checklogin(TaiKhoan taikhoan)
        {
            string checktaikhoan = KiemtraTaiKhoan(taikhoan);
            int ltk = checkloaiTK(taikhoan);
            if (checktaikhoan == "Thành công")
            {
                if (ltk == 1)
                {
                    string laymatkhau = LayMatKhau(taikhoan);
                    bool checking = BCrypt.Net.BCrypt.Verify(taikhoan.MatKhau, laymatkhau);
                    if (checking == true)
                    {
                        return "success";
                    }
                    else
                    {
                        return "saimk";
                    }
                }
                else
                {
                    string laymatkhau = LayMatKhau(taikhoan);
                    if(taikhoan.MatKhau == laymatkhau)
                    {
                        return "success";
                    }
                    else
                    {
                        return "saimk";
                    }
                }
            }
            else
            {
                return "saiTK";
            }
        }
        public string LayADminmail()
        {
            string admail = LayGmail();
            return admail;
        }
       
       
        public string UpdateMatkhau(TaiKhoan taikhoan)
        {
            string checktaikhoan = KiemtraTaiKhoan(taikhoan);
            int LTK = checkloaiTK(taikhoan);
            if(checktaikhoan == "Thành công")
            {
                if (LTK == 1)
                {
                    taikhoan.MatKhau = Encrypt(taikhoan.MatKhau);
                    string updateMK = Doimatkhau(taikhoan);
                    return updateMK;
                }
                else
                {
                    string updateMK = Doimatkhau(taikhoan);
                    return updateMK;
                }
            }    
            else 
            {
                return "saiTK";
            }
           
        }
        public int checkloaiTK(TaiKhoan taikhoan)
        {
            int ltk = LayLTK(taikhoan);
            return ltk;
        }
       
        
       
       
    }
}
