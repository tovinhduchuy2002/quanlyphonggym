﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
namespace DAL
{
    public class SanPhamAccess:DatabaseAccess
    {
        public List<SanPham> xemListSP()
        {
            return DatabaseAccess.xemListSP();
        }
        public List<SanPham> xemListSPtheoLoai(string loaisp)
        {
            return DatabaseAccess.xemListSPtheoLoai(loaisp);
        }
        public int demSP(string loaisp)
        {
            return DatabaseAccess.demSanPham(loaisp);
        }
        public string updateSP(SanPham sp)
        {
            return DatabaseAccess.UpdatethongtinSanPham(sp);
        }
        public bool themSP(SanPham sp)
        {
            return DatabaseAccess.ThemSanPham(sp);
        }
        public SanPham xemSP(string masp)
        {
            return DatabaseAccess.xemthongtinSP(masp);
        }

    }
}
