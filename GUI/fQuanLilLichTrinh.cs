﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class fQuanLilLichTrinh : Form
    {
        public fQuanLilLichTrinh()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnNV_Click(object sender, EventArgs e)
        {
            fQLLTNVcs fnv = new fQLLTNVcs();
            this.Hide();
            fnv.ShowDialog();
            this.Show();
        }

        private void btnHLV_Click(object sender, EventArgs e)
        {
            fQLLTHLV fhlv = new fQLLTHLV();
            this.Hide();
            fhlv.ShowDialog();
            this.Show();
        }
    }
}
