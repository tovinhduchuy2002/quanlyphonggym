﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class fTrangChu : Form
    {
        public fTrangChu()
        {
            InitializeComponent();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            fQuanLiKhachHang fqlkh = new fQuanLiKhachHang();
            this.Hide();
            fqlkh.ShowDialog();
            this.Show();
        }

        private void btnQLDCTD_Click(object sender, EventArgs e)
        {
            fQuanlidungcutheduc fqldctd = new fQuanlidungcutheduc();
            this.Hide();
            fqldctd.ShowDialog();
            this.Show();
        }

        private void btnTK_Click(object sender, EventArgs e)
        {
            fQuanLiThongKeDoanhThu fqltkdt = new fQuanLiThongKeDoanhThu();
            this.Hide();
            fqltkdt.ShowDialog();
            this.Show();
        }

        private void btnQLNS_Click(object sender, EventArgs e)
        {
            fQuanlidansucs fqlds = new fQuanlidansucs();
            this.Hide();
            fqlds.ShowDialog();
            this.Show();
        }

        

        private void btnQLSP_Click(object sender, EventArgs e)
        {
            fQuanLiSP fqlsp = new fQuanLiSP();
            this.Hide();
            fqlsp.ShowDialog();
            this.Show();
        }
    }
}
