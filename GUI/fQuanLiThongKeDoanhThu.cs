﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class fQuanLiThongKeDoanhThu : Form
    {
        public fQuanLiThongKeDoanhThu()
        {
            InitializeComponent();
        }

        private void fQuanLiThongKeDoanhThu_Load(object sender, EventArgs e)
        {

        }

        private void btnTKCP_Click(object sender, EventArgs e)
        {
            fThongKeChiPhi ftkcp = new fThongKeChiPhi();
            this.Hide();
            ftkcp.ShowDialog();
            this.Show();
        }
    }
}
