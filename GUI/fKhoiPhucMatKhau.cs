﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using DTO;
using BLL;
using System.Data.SqlClient;

namespace GUI
{
    public partial class fKhoiPhucMatKhau : Form
    {
        SqlConnection conn;
        TaiKhoan taikhoan = new TaiKhoan();
        TaiKhoanBLL TKBLL = new TaiKhoanBLL();
        SendMail mail = new SendMail();
        string otp;
        public fKhoiPhucMatKhau()
        {
            InitializeComponent();
            btnKhoiPhuc.Enabled = false;
        }

        private void fKhoiPhucMatKhau_Load(object sender, EventArgs e)
        {

        }

        private void btnGetOTP_Click(object sender, EventArgs e)
        {
            string sendingmail = mail.GuiMail();
            switch (sendingmail)
            {
                case "success":
                    btnKhoiPhuc.Enabled = true;
                    MessageBox.Show("Gửi OTP thành công");
                    return;
                case "fail":
                    MessageBox.Show("Gửi OTP thất bại");
                    return;
                
            }

        }

        private void btnKhoiPhuc_Click(object sender, EventArgs e)
        {
             taikhoan.maTaiKhoan = tbMaTaiKhoan.Text;
            if(tbMatKhauMoi.Text == tbXacNhanMatKhau.Text)
            {
                 taikhoan.MatKhau = tbMatKhauMoi.Text;
            }
            else { MessageBox.Show("Nhập lại mật khẩu"); }
            if(tbOTP.Text=="")
            {
                MessageBox.Show("Vui lòng nhập OTP");
            }    
            
            if (TKBLL.CheckOTP(tbOTP.Text) == true)
            {
                
                string updatemk = TKBLL.Capnhatmatkhau(taikhoan);
                switch (updatemk)
                {
                    case "mataikhoantrang":
                        MessageBox.Show("Nhập tên tài khoản");
                        return;
                    case "matkhautrang":
                        MessageBox.Show("Nhap mật khẩu");
                        return;
                    case "saiTK":
                        MessageBox.Show("Sai tài khoản");
                        return;

                    case "success":
                        MessageBox.Show("Đổi mật khẩu thành công ");
                        return;
                    case "false":
                        MessageBox.Show("Đổi mặt khẩu thất bại");
                        return;
                    
                }    
            }
            else
            { 
                MessageBox.Show("sai ma OTP" );
            }
            
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void Test()
        {
            if (otp == tbOTP.Text)
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                SqlCommand cmd;
                if (tbMatKhauMoi.Text == tbXacNhanMatKhau.Text)
                {
                    string Doimk = "update TAIKHOAN set [MATKHAU]=REPLACE(LTRIM(RTRIM('" + tbMatKhauMoi.Text + "')),' ',' ') where [MATAIKHOAN]='" + tbMaTaiKhoan.Text + "'";
                    cmd = new SqlCommand(Doimk, conn);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Đổi lại mật khẩu thành công");
                }
                else
                {
                    MessageBox.Show("Mật khẩu không khớp");
                }
            }
            else
            {
                MessageBox.Show("SAI MÃ OTP");
                btnKhoiPhuc.Enabled = false;
            }
        }
    }
}
