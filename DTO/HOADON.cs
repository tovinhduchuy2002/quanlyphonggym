﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class HOADON
    {
        private string hoadon;
        public string MaHoaDon
        {
            get => hoadon;
            set => hoadon = value;
        }
        private string masp;
        public string MaSP
        {
            get => masp;
            set => masp = value;
        }
        private long tongtien;
        public long TongTien
        {
            get => tongtien;
            set => tongtien = value;
        }
        private DateTime ngaythanhtoan;
        public DateTime NgayThanhToan
        {
            get =>  ngaythanhtoan;
            set => ngaythanhtoan = value;
        }
        private string mathe;
        public string MaThe
        {
            get => mathe;
            set => mathe = value;
        }
        public HOADON()
        {
            ngaythanhtoan = DateTime.Today;
            tongtien = 0;
        }
        ~HOADON() { }


    }
}
