﻿
namespace GUI
{
    partial class fTrangChu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQLNS = new System.Windows.Forms.Button();
            this.btnTK = new System.Windows.Forms.Button();
            this.btnQLDCTD = new System.Windows.Forms.Button();
            this.btnQLKH = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnQLSP = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnQLNS
            // 
            this.btnQLNS.AutoSize = true;
            this.btnQLNS.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnQLNS.BackgroundImage = global::GUI.Properties.Resources.QNLNS;
            this.btnQLNS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnQLNS.FlatAppearance.BorderSize = 0;
            this.btnQLNS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQLNS.Location = new System.Drawing.Point(227, 511);
            this.btnQLNS.Name = "btnQLNS";
            this.btnQLNS.Size = new System.Drawing.Size(188, 142);
            this.btnQLNS.TabIndex = 114;
            this.btnQLNS.UseVisualStyleBackColor = false;
            this.btnQLNS.Click += new System.EventHandler(this.btnQLNS_Click);
            // 
            // btnTK
            // 
            this.btnTK.AutoSize = true;
            this.btnTK.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTK.BackgroundImage = global::GUI.Properties.Resources.TKDT;
            this.btnTK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTK.FlatAppearance.BorderSize = 0;
            this.btnTK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTK.Location = new System.Drawing.Point(576, 251);
            this.btnTK.Name = "btnTK";
            this.btnTK.Size = new System.Drawing.Size(190, 158);
            this.btnTK.TabIndex = 113;
            this.btnTK.UseVisualStyleBackColor = false;
            this.btnTK.Click += new System.EventHandler(this.btnTK_Click);
            // 
            // btnQLDCTD
            // 
            this.btnQLDCTD.AutoSize = true;
            this.btnQLDCTD.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnQLDCTD.BackgroundImage = global::GUI.Properties.Resources.QLDCTD;
            this.btnQLDCTD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnQLDCTD.FlatAppearance.BorderSize = 0;
            this.btnQLDCTD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQLDCTD.Location = new System.Drawing.Point(227, 251);
            this.btnQLDCTD.Name = "btnQLDCTD";
            this.btnQLDCTD.Size = new System.Drawing.Size(188, 158);
            this.btnQLDCTD.TabIndex = 112;
            this.btnQLDCTD.UseVisualStyleBackColor = false;
            this.btnQLDCTD.Click += new System.EventHandler(this.btnQLDCTD_Click);
            // 
            // btnQLKH
            // 
            this.btnQLKH.AutoSize = true;
            this.btnQLKH.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnQLKH.BackgroundImage = global::GUI.Properties.Resources.QLKH;
            this.btnQLKH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnQLKH.FlatAppearance.BorderSize = 0;
            this.btnQLKH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQLKH.Location = new System.Drawing.Point(885, 257);
            this.btnQLKH.Name = "btnQLKH";
            this.btnQLKH.Size = new System.Drawing.Size(190, 152);
            this.btnQLKH.TabIndex = 111;
            this.btnQLKH.UseVisualStyleBackColor = false;
            this.btnQLKH.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GUI.Properties.Resources.tileQTV;
            this.pictureBox1.Location = new System.Drawing.Point(1, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1186, 140);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // btnQLSP
            // 
            this.btnQLSP.AutoSize = true;
            this.btnQLSP.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnQLSP.BackgroundImage = global::GUI.Properties.Resources.SanPham;
            this.btnQLSP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnQLSP.FlatAppearance.BorderSize = 0;
            this.btnQLSP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQLSP.Location = new System.Drawing.Point(576, 502);
            this.btnQLSP.Name = "btnQLSP";
            this.btnQLSP.Size = new System.Drawing.Size(190, 151);
            this.btnQLSP.TabIndex = 116;
            this.btnQLSP.UseVisualStyleBackColor = false;
            this.btnQLSP.Click += new System.EventHandler(this.btnQLSP_Click);
            // 
            // fTrangChu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1182, 753);
            this.Controls.Add(this.btnQLSP);
            this.Controls.Add(this.btnQLNS);
            this.Controls.Add(this.btnTK);
            this.Controls.Add(this.btnQLDCTD);
            this.Controls.Add(this.btnQLKH);
            this.Controls.Add(this.pictureBox1);
            this.Name = "fTrangChu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fTrangChu";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnQLKH;
        private System.Windows.Forms.Button btnQLDCTD;
        private System.Windows.Forms.Button btnTK;
        private System.Windows.Forms.Button btnQLNS;
        private System.Windows.Forms.Button btnQLSP;
    }
}