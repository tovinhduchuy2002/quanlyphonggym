﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using System.Globalization;
using BLL;

namespace GUI
{
    public partial class fThongtinHLV : Form
    {
        public fThongtinHLV()
        {
            InitializeComponent();
        }
        NhanVienBLL nvbll = new NhanVienBLL();
        public bool checkupdateanh = false;
        public static string maNV { get; set; }
        NHANVIEN hlv = new NHANVIEN();
        private void LoadThongTin()
        {

            hlv = nvbll.xemThongTinNV(maNV);
            tbHoTen.Text = hlv.Hoten;
            tbMaNV.Text = hlv.maNhanVien.ToString();
            tbMaNV.Enabled = false;
            tbNgaysinh.Text = hlv.Ngaysinh;
            tbGioiTinh.Text = hlv.Gioitinh;
            tbCCCD.Text = hlv.CMND.ToString();
            tbDiachi.Text = hlv.diaChi;
            tbSDT.Text = hlv.SDT.ToString();
            tbEMail.Text = hlv.Email;
            tbGHichu.Text = hlv.GhiChu;
            tbChucvu.Text = hlv.Chucvu;
            tbChucvu.Enabled = false;
            using (MemoryStream ms = new MemoryStream(hlv.Anh))
            {
                ptbAvatar.Image = Image.FromStream(ms);
            }

        }

        private void fThongtinHLV_Load(object sender, EventArgs e)
        {
            LoadThongTin();
        }

        private void ptbAvatar_Click(object sender, EventArgs e)
        {
            OpenFileDialog moFile = new OpenFileDialog();
            moFile.Title = "Chọn ảnh khách hàng";
            moFile.Filter = "Image Files(*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf)|*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf";
            if (moFile.ShowDialog() == DialogResult.OK)
            {
                checkupdateanh = true;
                ptbAvatar.ImageLocation = moFile.FileName;
            }
        }
        private byte[] imageToByteArray(PictureBox ptb)
        {
            using (MemoryStream ms = new MemoryStream())
            {

                ptb.Image.Save(ms, ptb.Image.RawFormat); // Thay đổi định dạng ảnh nếu cần thiết
                return ms.ToArray();
            }
        }

        private void btnChinhSua_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbHoTen.Text))
            {
                MessageBox.Show("Điền họ tên");
                return;
            }
            else
            { hlv.Hoten = tbHoTen.Text; }
            if (string.IsNullOrEmpty(tbNgaysinh.Text))
            {
                MessageBox.Show("Điền ngày sinh");
                return;
            }
            else
            { hlv.Ngaysinh = tbNgaysinh.Text; }
            if (string.IsNullOrEmpty(tbGioiTinh.Text))
            {
                MessageBox.Show("Điền giới tính");
                return;
            }
            else
            { hlv.Gioitinh = tbGioiTinh.Text; }
            if (string.IsNullOrEmpty(tbCCCD.Text))
            {
                MessageBox.Show("Điền CCCD");
                return;
            }
            else
            { hlv.CMND = (string)tbCCCD.Text; }
            if (string.IsNullOrEmpty(tbDiachi.Text))
            {
                MessageBox.Show("Điền địa chỉ");
                return;
            }
            else
            { hlv.diaChi = tbDiachi.Text; }
            if (string.IsNullOrEmpty(tbSDT.Text))
            {
                MessageBox.Show("Điền Địa chỉ");
                return;
            }
            else
            { hlv.SDT = (string)tbSDT.Text; }
            if (string.IsNullOrEmpty(tbEMail.Text))
            {
                MessageBox.Show("Điền Số điện thoại");
                return;
            }
            else
            { hlv.Email = tbEMail.Text; }
            if (string.IsNullOrEmpty(tbGHichu.Text))
            {
                MessageBox.Show("Điền email");
                return;
            }
            else
            { hlv.GhiChu = tbGHichu.Text; }
            if(checkupdateanh == true)
            {
                hlv.Anh = imageToByteArray(ptbAvatar);
            }    
            
            string getupdate = nvbll.checkcapnhat(hlv);
            switch (getupdate)
            {

                case "success":
                    MessageBox.Show("Thay đổi thành công");
                    return;
                case "fail":
                    MessageBox.Show("Thay đổi thất bại");
                    return;
            }
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            bool check = nvbll.XoaNV(maNV);
            if(check == true)
            {
                MessageBox.Show("Xóa thất bại ");
            }
            else { MessageBox.Show("Xóa thành công"); } 
                
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void tbChucvu_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void tbMaNV_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void tbHoTen_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void tbNgaysinh_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void tbGioiTinh_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void tbCCCD_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void tbDiachi_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void tbSDT_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void tbEMail_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void tbGHichu_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void btnTrangChu_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
