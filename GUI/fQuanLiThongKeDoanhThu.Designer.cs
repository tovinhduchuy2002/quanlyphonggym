﻿
namespace GUI
{
    partial class fQuanLiThongKeDoanhThu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fQuanLiThongKeDoanhThu));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnMenu = new System.Windows.Forms.Button();
            this.btnTrangChu = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnTKDTBH = new System.Windows.Forms.Button();
            this.btnDTGT = new System.Windows.Forms.Button();
            this.btnTKCP = new System.Windows.Forms.Button();
            this.btnTKDT = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(113, 214);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(469, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // btnMenu
            // 
            this.btnMenu.AutoSize = true;
            this.btnMenu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnMenu.BackgroundImage = global::GUI.Properties.Resources._3_gach;
            this.btnMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMenu.FlatAppearance.BorderSize = 0;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Location = new System.Drawing.Point(29, 204);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(60, 61);
            this.btnMenu.TabIndex = 15;
            this.btnMenu.UseVisualStyleBackColor = false;
            // 
            // btnTrangChu
            // 
            this.btnTrangChu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTrangChu.BackgroundImage = global::GUI.Properties.Resources.logoTrangChu;
            this.btnTrangChu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTrangChu.FlatAppearance.BorderSize = 0;
            this.btnTrangChu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrangChu.Location = new System.Drawing.Point(29, 147);
            this.btnTrangChu.Name = "btnTrangChu";
            this.btnTrangChu.Size = new System.Drawing.Size(149, 61);
            this.btnTrangChu.TabIndex = 14;
            this.btnTrangChu.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GUI.Properties.Resources.tileQTV;
            this.pictureBox1.Location = new System.Drawing.Point(0, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1186, 140);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.ErrorImage = null;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(168, 162);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(266, 35);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 17;
            this.pictureBox3.TabStop = false;
            // 
            // btnTKDTBH
            // 
            this.btnTKDTBH.AutoSize = true;
            this.btnTKDTBH.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTKDTBH.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTKDTBH.BackgroundImage")));
            this.btnTKDTBH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTKDTBH.FlatAppearance.BorderSize = 0;
            this.btnTKDTBH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTKDTBH.Location = new System.Drawing.Point(402, 346);
            this.btnTKDTBH.Name = "btnTKDTBH";
            this.btnTKDTBH.Size = new System.Drawing.Size(378, 61);
            this.btnTKDTBH.TabIndex = 18;
            this.btnTKDTBH.UseVisualStyleBackColor = false;
            // 
            // btnDTGT
            // 
            this.btnDTGT.AutoSize = true;
            this.btnDTGT.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDTGT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDTGT.BackgroundImage")));
            this.btnDTGT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDTGT.FlatAppearance.BorderSize = 0;
            this.btnDTGT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDTGT.Location = new System.Drawing.Point(402, 434);
            this.btnDTGT.Name = "btnDTGT";
            this.btnDTGT.Size = new System.Drawing.Size(378, 61);
            this.btnDTGT.TabIndex = 19;
            this.btnDTGT.UseVisualStyleBackColor = false;
            // 
            // btnTKCP
            // 
            this.btnTKCP.AutoSize = true;
            this.btnTKCP.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTKCP.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTKCP.BackgroundImage")));
            this.btnTKCP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTKCP.FlatAppearance.BorderSize = 0;
            this.btnTKCP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTKCP.Location = new System.Drawing.Point(402, 520);
            this.btnTKCP.Name = "btnTKCP";
            this.btnTKCP.Size = new System.Drawing.Size(378, 61);
            this.btnTKCP.TabIndex = 20;
            this.btnTKCP.UseVisualStyleBackColor = false;
            this.btnTKCP.Click += new System.EventHandler(this.btnTKCP_Click);
            // 
            // btnTKDT
            // 
            this.btnTKDT.AutoSize = true;
            this.btnTKDT.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTKDT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTKDT.BackgroundImage")));
            this.btnTKDT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTKDT.FlatAppearance.BorderSize = 0;
            this.btnTKDT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTKDT.Location = new System.Drawing.Point(402, 610);
            this.btnTKDT.Name = "btnTKDT";
            this.btnTKDT.Size = new System.Drawing.Size(378, 61);
            this.btnTKDT.TabIndex = 21;
            this.btnTKDT.UseVisualStyleBackColor = false;
            // 
            // fQuanLiThongKeDoanhThu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1182, 753);
            this.Controls.Add(this.btnTKDT);
            this.Controls.Add(this.btnTKCP);
            this.Controls.Add(this.btnDTGT);
            this.Controls.Add(this.btnTKDTBH);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnMenu);
            this.Controls.Add(this.btnTrangChu);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Name = "fQuanLiThongKeDoanhThu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fQuanLiThongKeDoanhThu";
            this.Load += new System.EventHandler(this.fQuanLiThongKeDoanhThu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Button btnTrangChu;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnTKDTBH;
        private System.Windows.Forms.Button btnDTGT;
        private System.Windows.Forms.Button btnTKCP;
        private System.Windows.Forms.Button btnTKDT;
    }
}