﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using System.Globalization;
using BLL;
namespace GUI
{
    public partial class fBanSP : Form
    {
        public fBanSP()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        SanPhamBLL spbll = new SanPhamBLL();
        BienlaiBLL blbll = new BienlaiBLL();
        KHBLL khbll = new KHBLL();
        HoaDonBLL hdbll = new HoaDonBLL();
        private string loaisp;
        List<SanPham> lsp = new List<SanPham>();
        int demanh1 = 0;
        int demanh2 = 1;
        int demanh3 = 2;
        int slsp1 = 0;
        int slsp2 = 0;
        int slsp3 = 0;
        int stt = 0;
        int rowindex = -1;
        HOADON hoadon= new HOADON();
        private void LoadGioHang()
        { 
            dtgDSSP.Columns.Add("Column1", "MASP");
            dtgDSSP.Columns.Add("Column2", "TENSP");
            dtgDSSP.Columns.Add("Column3", "SOLUONG");
            dtgDSSP.Columns.Add("Column4", "GIATIEN");
        }
        int count;
        private void cmbLoaiSP_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbLoaiSP.SelectedItem == "Thực phẩm")
            {
                loaisp = "THUCPHAM";
                LoadListSPtheoLoai(loaisp);
            }
            if (cmbLoaiSP.SelectedItem == "Dụng cụ hỗ trợ")
            {
                loaisp = "DUNGCU";
                LoadListSPtheoLoai(loaisp);

            }
        }
        private void LoadListSPtheoLoai(string loaiSP)
        {
            
          
            lsp = spbll.xemListSPtheoLoai(loaiSP);

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RefreshData();
        }
        private void RefreshData()
        {
            
            
            lsp = spbll.xemListSP();


        }
        private void ThemSanPhamBan()
        {


            if (demanh1 < lsp.Count)
            {
                using (MemoryStream ms = new MemoryStream(lsp[demanh1].Anh))
                {
                    ptbSP1.Image = Image.FromStream(ms);
                    lbTenSP1.Text = lsp[demanh1].TenSP;
                    lbSLSP1.Text = "0";
                    lbGiaThanhSP1.Text = lsp[demanh1].GiaThanh.ToString();
                }
                if (demanh2 < lsp.Count)
                {
                    using (MemoryStream ms = new MemoryStream(lsp[demanh2].Anh))
                    {
                        ptbSP2.Image = Image.FromStream(ms);
                        lbTenSP2.Text = lsp[demanh2].TenSP;
                        lbSLSP2.Text = "0";
                        lbGiaThanhSP2.Text = lsp[demanh2].GiaThanh.ToString();
                        btnCongSP2.Enabled = true;
                        btnTruSP2.Enabled = true;
                    }
                    if (demanh3 < lsp.Count)
                    {
                        using (MemoryStream ms = new MemoryStream(lsp[demanh3].Anh))
                        {
                            ptbSP3.Image = Image.FromStream(ms);
                            lbtenSP3.Text = lsp[demanh3].TenSP;
                            lbSLSP3.Text = "0";
                            lbGiaThanhSP3.Text = lsp[demanh3].GiaThanh.ToString();
                            btnCongSP3.Enabled = true;
                            btnTruSP3.Enabled = true;
                        }

                    }
                    else
                    {
                        ptbSP3.Image = Properties.Resources.product;
                        lbtenSP3.Text = "Không";
                        lbSLSP3.Text = "0";
                        lbGiaThanhSP3.Text = "0";
                        btnCongSP3.Enabled = false;
                        btnTruSP3.Enabled = false;
                    }

                }
                else
                {
                    ptbSP2.Image = Properties.Resources.product;
                    lbTenSP2.Text = "Không";
                    lbSLSP2.Text = "0";
                    lbGiaThanhSP2.Text = "0";
                    btnCongSP2.Enabled = false;
                    btnTruSP2.Enabled = false;

                    ptbSP3.Image = Properties.Resources.product;
                    lbtenSP3.Text = "Không";
                    lbSLSP3.Text = "0";
                    lbGiaThanhSP3.Text = "0";
                    btnCongSP3.Enabled = false;
                    btnTruSP3.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("Đã hết sản phẩm");
                
                return;


            }

        }

        private void fBanSP_LocationChanged(object sender, EventArgs e)
        {

        }

        private void fBanSP_Load(object sender, EventArgs e)
        {
            LoadGioHang();
            RefreshData();
            ThemSanPhamBan();
          
            foreach(string mathe in LoadListKH())
            {
                cmbMaKH.Items.Add(mathe);
            }
        }

  

        private void btnPhai_Click(object sender, EventArgs e)
        {
            demanh1 = demanh1 + 3;
            demanh2 = demanh2 + 3;
            demanh3 = demanh3 + 3;

            if (demanh1 < lsp.Count)
            {
                using (MemoryStream ms = new MemoryStream(lsp[demanh1].Anh))
                {
                    ptbSP1.Image = Image.FromStream(ms);
                    lbTenSP1.Text = lsp[demanh1].TenSP;
                    lbSLSP1.Text = "0";
                    lbGiaThanhSP1.Text = lsp[demanh1].GiaThanh.ToString();
                }
                if (demanh2 < lsp.Count)
                {
                    using (MemoryStream ms = new MemoryStream(lsp[demanh2].Anh))
                    {
                        ptbSP2.Image = Image.FromStream(ms);
                        lbTenSP2.Text = lsp[demanh2].TenSP;
                        lbSLSP2.Text = "0";
                        lbGiaThanhSP2.Text = lsp[demanh2].GiaThanh.ToString();
                        btnCongSP2.Enabled = true;
                        btnTruSP2.Enabled = true;
                    }
                    if (demanh3 < lsp.Count)
                    {
                        using (MemoryStream ms = new MemoryStream(lsp[demanh3].Anh))
                        {
                            ptbSP3.Image = Image.FromStream(ms);
                            lbtenSP3.Text = lsp[demanh3].TenSP;
                            lbSLSP3.Text = "0";
                            lbGiaThanhSP3.Text = lsp[demanh3].GiaThanh.ToString();
                            btnCongSP3.Enabled = true;
                            btnTruSP3.Enabled = true;
                        }

                    }
                    else
                    {
                        ptbSP3.Image = Properties.Resources.product;
                        lbtenSP3.Text = "Không";
                        lbSLSP3.Text = "0";
                        lbGiaThanhSP3.Text = "0";
                        btnCongSP3.Enabled = false;
                        btnTruSP3.Enabled = false;
                    }    
                 
                }
                else
                {
                    ptbSP2.Image = Properties.Resources.product;
                    lbTenSP2.Text = "Không";
                    lbSLSP2.Text = "0";
                    lbGiaThanhSP2.Text = "0";
                    btnCongSP2.Enabled = false;
                    btnTruSP2.Enabled = false;

                    ptbSP3.Image = Properties.Resources.product;
                    lbtenSP3.Text = "Không";
                    lbSLSP3.Text = "0";
                    lbGiaThanhSP3.Text = "0";
                    btnCongSP3.Enabled = false;
                    btnTruSP3.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("Đã hết sản phẩm");
                demanh1 = demanh1 - 3;
                demanh2 = demanh2 - 3;
                demanh3 = demanh3 - 3;
                return;


            }

        }

        private void btnTrai_Click(object sender, EventArgs e)
        {
            demanh1 = demanh1 - 3;
            demanh2 = demanh2 - 3;
            demanh3 = demanh3 - 3;
            if (demanh1 >= 0)
            {
                
                btnTruSP1.Enabled = true;
                btnTruSP2.Enabled = true;
                btnTruSP3.Enabled = true;
                btnCongSP1.Enabled = true;
                btnCongSP2.Enabled = true;
                btnCongSP3.Enabled = true;
                using (MemoryStream ms = new MemoryStream(lsp[demanh1].Anh))
                {
                    ptbSP1.Image = Image.FromStream(ms);
                    lbTenSP1.Text = lsp[demanh1].TenSP;
                    lbSLSP1.Text = "0";
                    lbGiaThanhSP1.Text = lsp[demanh1].GiaThanh.ToString();
                }
                using (MemoryStream ms = new MemoryStream(lsp[demanh2].Anh))
                    {
                        ptbSP2.Image = Image.FromStream(ms);
                        lbTenSP2.Text = lsp[demanh2].TenSP;
                        lbSLSP2.Text = "0";
                        lbGiaThanhSP2.Text = lsp[demanh2].GiaThanh.ToString();
                    }
                    
                using (MemoryStream ms = new MemoryStream(lsp[demanh3].Anh))
                    {
                            ptbSP3.Image = Image.FromStream(ms);
                            lbtenSP3.Text = lsp[demanh3].TenSP;
                            lbSLSP3.Text = "0";
                            lbGiaThanhSP3.Text = lsp[demanh3].GiaThanh.ToString();
                   }

                    

                
            }
            else
            {

                MessageBox.Show("Đã quay lại sản phẩm ban đầu");
                demanh1 = demanh1 + 3;
                demanh2 = demanh2 + 3;
                demanh3 = demanh3 + 3;
                return;

            }
        }
        private void btnCongSP1_Click(object sender, EventArgs e)
        {
            
            slsp1++;
            lbSLSP1.Text = (slsp1).ToString();
            dtgDSSP.Rows.Add( lsp[demanh1].MaSP, lsp[demanh1].TenSP, "1", lsp[demanh1].GiaThanh);
            hoadon.TongTien = hoadon.TongTien + lsp[demanh1].GiaThanh;
            MessageBox.Show(hoadon.TongTien.ToString());

        }

        private void btnTruSP1_Click(object sender, EventArgs e)
        {
            slsp1--;
            lbSLSP1.Text = (slsp1).ToString();
            string valuecheck = lsp[demanh1].MaSP;
           foreach(DataGridViewRow row in dtgDSSP.Rows)
            {
                string cellvalue = row.Cells["Column1"].Value.ToString();
                if(cellvalue == valuecheck)
                {
                    dtgDSSP.Rows.Remove(row);
                    hoadon.TongTien -= lsp[demanh1].GiaThanh;
                }
            }
            MessageBox.Show(hoadon.TongTien.ToString());
        }

        private void btnCongSP2_Click(object sender, EventArgs e)
        {
            slsp2++;
            lbSLSP2.Text = (slsp2).ToString();
            dtgDSSP.Rows.Add(lsp[demanh2].MaSP, lsp[demanh2].TenSP, "1", lsp[demanh2].GiaThanh);
            hoadon.TongTien = hoadon.TongTien + lsp[demanh2].GiaThanh;
            MessageBox.Show(hoadon.TongTien.ToString());

        }

        private void btnTruSP2_Click(object sender, EventArgs e)
        {
            slsp2--;
            lbSLSP2.Text = (slsp2).ToString();
            string valuecheck = lsp[demanh2].MaSP;
            foreach (DataGridViewRow row in dtgDSSP.Rows)
            {
                string cellvalue = row.Cells["Column1"].Value.ToString();
                if (cellvalue == valuecheck)
                {
                    dtgDSSP.Rows.Remove(row);
                    hoadon.TongTien -= lsp[demanh2].GiaThanh;
                }
            }
            MessageBox.Show(hoadon.TongTien.ToString());
        }

        private void btnCongSP3_Click(object sender, EventArgs e)
        {

        }

        private void btnTruSP3_Click(object sender, EventArgs e)
        {

        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(cmbMaKH.SelectedIndex == -1)
            {
                MessageBox.Show("Chọn mã khách hàng");
                return;
            }
            else
            {
                hoadon.MaThe = cmbMaKH.SelectedItem.ToString();
            }    
            foreach(DataGridViewRow row in dtgDSSP.Rows)
            {
                string cellvalue = row.Cells["Column1"].Value.ToString();
                hoadon.MaSP += cellvalue + "_";
            }
            bool check = hdbll.themHoaDon(hoadon);
            if (check == true)
            {
                MessageBox.Show("Thêm hóa đơn thành công");
                return;
            }
            else
            {
                MessageBox.Show("Thêm hóa đơn thất bại");
                return;
            }    
            
        }

        private void cmbMaKH_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }
        private  List<string> LoadListKH()
        {
            return khbll.xemListMaThe();
        }
    }
}
