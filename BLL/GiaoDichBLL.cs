﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;
namespace BLL
{
    public class GiaoDichBLL
    {
        GiaoDichAccess gdaccs = new GiaoDichAccess();
       
        public bool themGiaodich(GIAODICH gd)
        {
            int count = gdaccs.demGD(gd.LoaiChiPhi) + 1;
           
           gd.MaGD = "GD_" + gd.LoaiChiPhi + count.ToString();
            
            return gdaccs.themGiaoDich(gd);
        }
        public List<GIAODICH> xemListGD()
        {
            return gdaccs.XemListGiaoDich();
        }
        public List<GIAODICH> xemListGDtheoloai(string loaichiphi)
        {
            return gdaccs.xemListGDtheoLoai(loaichiphi);
        } 
        public List<string> xemListMaTBBT()
        {
            return gdaccs.xemListmaTBBT();
        }
        public List<string> xemListMASP()
        {
            return gdaccs.xemListMaSP();
        }
    }
}
