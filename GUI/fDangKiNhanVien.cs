﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using System.Globalization;
using BLL;
namespace GUI
{
    public partial class fDangKiNhanVien : Form
    {
        public fDangKiNhanVien()
        {
            InitializeComponent();
        }
        NHANVIEN nv = new NHANVIEN();
        NhanVienBLL nvbll = new NhanVienBLL();
        public bool checkupdateanh = false;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbChucVu.SelectedItem.ToString() == "LETAN" )
            {
                nv.Chucvu = "LETAN";
            }
            else if(cmbChucVu.SelectedItem.ToString() == "HUANLUYENVIEN")
            {
                nv.Chucvu = "HUANLUYENVIEN";
            }
            else if (cmbChucVu.SelectedItem.ToString() == "BAOVE")
            {
                nv.Chucvu = "BAOVE";
            }
        }
            

        private void ptbAvatar_Click(object sender, EventArgs e)
        {
            OpenFileDialog moFile = new OpenFileDialog();
            moFile.Title = "Chọn ảnh khách hàng";
            moFile.Filter = "Image Files(*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf)|*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf";
            if (moFile.ShowDialog() == DialogResult.OK)
            {
                checkupdateanh = true;
                ptbAvatar.ImageLocation = moFile.FileName;
            }
        }
        private byte[] imageToByteArray(PictureBox ptb)
        {
            using (MemoryStream ms = new MemoryStream())
            {

                ptb.Image.Save(ms, ptb.Image.RawFormat); // Thay đổi định dạng ảnh nếu cần thiết
                return ms.ToArray();
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
           
            if (string.IsNullOrEmpty(tbHoTen.Text))
            {
                MessageBox.Show("Điền họ tên");
                return;
            }
            else
            { nv.Hoten = tbHoTen.Text; }
            if (string.IsNullOrEmpty(tbNgaysinh.Text))
            {
                MessageBox.Show("Điền ngày sinh");
                return;
            }
            else
            { nv.Ngaysinh = tbNgaysinh.Text; }
            if (string.IsNullOrEmpty(tbGioiTinh.Text))
            {
                MessageBox.Show("Điền giới tính");
                return;
            }
            else
            { nv.Gioitinh = tbGioiTinh.Text; }
            if (string.IsNullOrEmpty(tbCCCD.Text))
            {
                MessageBox.Show("Điền CCCD");
                return;
            }
            else
            { nv.CMND = (string)tbCCCD.Text; }
            if (string.IsNullOrEmpty(tbDiachi.Text))
            {
                MessageBox.Show("Điền địa chỉ");
                return;
            }
            else
            { nv.diaChi = tbDiachi.Text; }
            if (string.IsNullOrEmpty(tbSDT.Text))
            {
                MessageBox.Show("Điền SDT");
                return;
            }
            else
            { nv.SDT = (string)tbSDT.Text; }
            if (string.IsNullOrEmpty(tbEMail.Text))
            {
                MessageBox.Show("Điền EMAIL");
                return;
            }
            else
            { nv.Email = tbEMail.Text; }
            if (string.IsNullOrEmpty(tbGHichu.Text))
            {
                MessageBox.Show("Điền ghi chú");
                return;
            }
            else
            { nv.GhiChu = tbGHichu.Text; }
           
             nv.Anh = imageToByteArray(ptbAvatar);
            
            bool check = nvbll.themNV(nv);
           if(check == true)
            {
                MessageBox.Show("Thêm nhân viên thành công");
            }
            else
            {
                MessageBox.Show("Trùng mã nhân viên rồi");
            } 
                
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
