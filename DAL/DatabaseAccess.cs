﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Configuration;
using System.Net.Mail;
using DTO;

namespace DAL
{
    //Tạo kểt nối cơ sở dữ liệu
    public class SqlconnectionData
    {
        public static SqlConnection sqlconnect()
        {
            // đường dẫn tuyệt đối
            string connectionString = ConfigurationManager.ConnectionStrings["MyConnectionString"].ConnectionString;
            string strcon = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\dtbGYM.mdf;Integrated Security=True";
            // sử dụng 1 cái khác để sài sql cho dễ.
            string conneccttt = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Admin\\Desktop\\Quanliphonggym(test)\\Quanliphonggym(test)\\GYM_UpdateMaHoaDuLieu(3)\\QuanLyPhongGym\\DAL\\dtbGYM.mdf;Integrated Security=True";
            SqlConnection conn = new SqlConnection(strcon);//khoi tao connect
            return conn;

        }

    }

    public class DatabaseAccess
    {


        // Tác vụ liên quan đến TÀI KHOẢN
        public static int LayLTK(TaiKhoan taikhoan)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            int loaitk = 0;
            string readInf = "select LOAITAIKHOAN from TAIKHOAN where MATAIKHOAN = '" + taikhoan.maTaiKhoan + "'";
            SqlCommand cmd = new SqlCommand(readInf, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                loaitk = Convert.ToInt32(reader["LOAITAIKHOAN"]);

            }
            reader.Close();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            return loaitk;

        }

        public static string LayGmail()
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string MailAdmin = " ";

            string readInf = "select EMAILCANHAN from TAIKHOAN where MATAIKHOAN = 'TK01'";
            SqlCommand cmd = new SqlCommand(readInf, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                MailAdmin = reader["EMAILCANHAN"].ToString();
            }
            reader.Close();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            return MailAdmin;

        }
        public static string LayMatKhau(TaiKhoan taikhoan)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string Matkhau = "";
            string readInf = "select MATKHAU from TAIKHOAN where MATAIKHOAN = '" + taikhoan.maTaiKhoan + "'";
            SqlCommand cmd = new SqlCommand(readInf, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                Matkhau = reader["MATKHAU"].ToString();
            }
            reader.Close();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            return Matkhau;

        }
        public static string KiemtraTaiKhoan(TaiKhoan taikhoan)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();

            try
            {
                conn.Open();
                string tk = taikhoan.maTaiKhoan;
                string sql = "select *from TAIKHOAN where MATAIKHOAN='" + tk + "'";
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dta = cmd.ExecuteReader();
                if (dta.Read() == true)
                {
                    return "Thành công";
                }
                else
                {
                    dta.Close();
                    conn.Close();
                    return "Thất bại";


                }
            }
            catch (Exception ex)
            {
                return "lỗi";
            }
            finally
            {
                conn.Close();
            }
        }
        public static string Doimatkhau(TaiKhoan taikhoan)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string Doimk = "update TAIKHOAN set [MATKHAU]=REPLACE(LTRIM(RTRIM('" + taikhoan.MatKhau + "')),' ',' ') where [MATAIKHOAN]='" + taikhoan.maTaiKhoan + "'";
            SqlCommand cmd = new SqlCommand(Doimk, conn);
            cmd.ExecuteNonQuery();
            int count = cmd.ExecuteNonQuery();
            if (count > 0)
            {
                return "success";
            }
            else
            {
                return "false";
            }

        }

        // SỬA THÔNG TIN (UPDATE)
        public static string UpdatethongtinKH(KHACHHANG kh)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string updateKH = "UPDATE KHACHHANG SET HOTEN = @HOTEN,CMND=@CMND, NGAYSINH = @NGAYSINH,GIOITINH = @GIOITINH,DIACHI = @DIACHI,SDT= @SDT, EMAIL=@EMAIL,GHICHU=@GHICHU,MAGOITAP=@MAGOITAP,NGAYDANGKI= @NGAYDANGKI,NGAYHETHAN = @NGAYHETHAN, TENPT = @TENPT, MANHANVIEN = @MANHANVIEN, GOITAPPT = @GOITAPPT, THOIHAN = @THOIHAN, ANH = @ANH WHERE MATHE = @MATHE";
            string selectQuery = "SELECT HOTEN FROM NHANVIEN WHERE MANHANVIEN = @MANHANVIEN";
            using (SqlCommand selectCommand = new SqlCommand(selectQuery, conn))
            {
                selectCommand.Parameters.AddWithValue("@MANHANVIEN", kh.maNhanVien);
                string TENPT = (string)selectCommand.ExecuteScalar();
                using (SqlCommand command = new SqlCommand(updateKH, conn))
                {
                    command.Parameters.AddWithValue("@MATHE", kh.Mathe);
                    command.Parameters.AddWithValue("@HOTEN", kh.Hoten);
                    command.Parameters.AddWithValue("@NGAYSINH", kh.Ngaysinh);

                    command.Parameters.AddWithValue("@MANHANVIEN", kh.maNhanVien);
                    command.Parameters.AddWithValue("@GIOITINH", kh.Gioitinh);
                    command.Parameters.AddWithValue("@CMND", kh.CMND);

                    command.Parameters.AddWithValue("@DIACHI", kh.diaChi);
                    command.Parameters.AddWithValue("@SDT", kh.SDT);
                    command.Parameters.AddWithValue("@EMAIL", kh.Email);

                    command.Parameters.AddWithValue("@GHICHU", kh.GhiChu);
                    command.Parameters.AddWithValue("@MAGOITAP", kh.maGoiTap);
                    command.Parameters.AddWithValue("@NGAYDANGKI", kh.NgayBatDau);

                    command.Parameters.AddWithValue("@NGAYHETHAN", kh.NgayHetHan);
                    command.Parameters.AddWithValue("@TENPT", TENPT);
                    command.Parameters.AddWithValue("@GOITAPPT", kh.GoitapPT);
                    command.Parameters.AddWithValue("@THOIHAN", kh.ThoiHan);
                    command.Parameters.AddWithValue("@ANH", kh.Anh);
                    int rowsAffected = command.ExecuteNonQuery();
                    if (rowsAffected > 0)
                    {
                        return "success";
                    }
                    else
                    {
                        return "fail";
                    }
                }
            }
        }
        public static string UpdatethongtinTB(THIETBI tb)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string updateKH = "UPDATE THIETBI SET TENTHIETBI = @TENTHIETBI,LOAITHIETBI=@LOAITHIETBI,TINHTRANG=@TINHTRANG, ANH = @ANH WHERE MATHIETBI = @MATHIETBI";
            using (SqlCommand command = new SqlCommand(updateKH, conn))
            {
                command.Parameters.AddWithValue("@MATHIETBI", tb.Mathietbi);
                command.Parameters.AddWithValue("@TENTHIETBI", tb.Tenthietbi);
                command.Parameters.AddWithValue("@LOAITHIETBI", tb.Loaithietbi);
                command.Parameters.AddWithValue("@TINHTRANG", tb.Tinhtrang);
                command.Parameters.AddWithValue("@ANH", tb.Anh);
                int rowsAffected = command.ExecuteNonQuery();
                if (rowsAffected > 0)
                {
                    return "success";
                }
                else
                {
                    return "fail";
                }
            }
        }
        public static string UpdatethongtinSanPham(SanPham sp)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "UPDATE SANPHAM SET MASANPHAM = @MASANPHAM,TENSANPHAM = @TENSANPHAM, GIATHANH = @GIATHANH, SOLUONG=@SOLUONG,LOAISANPHAM = @LOAISANPHAM, ANH =@ANH WHERE MASANPHAM = @MASANPHAM";
            using(SqlCommand cmd =new SqlCommand(query,conn))
            {
                cmd.Parameters.AddWithValue("@MASANPHAM", sp.MaSP);
                cmd.Parameters.AddWithValue("@TENSANPHAM", sp.TenSP);
                cmd.Parameters.AddWithValue("@LOAISANPHAM", sp.LoaiSP);
                cmd.Parameters.AddWithValue("@GIATHANH", sp.GiaThanh);
                cmd.Parameters.AddWithValue("@SOLUONG", sp.SL);
                cmd.Parameters.AddWithValue("@ANH", sp.Anh);
                int rowsAffected = cmd.ExecuteNonQuery();
                if (rowsAffected > 0)
                {
                    return "success";
                }
                else
                {
                    return "fail";
                }
            }
        }
        public static string UpdatethongtinNV(NHANVIEN nv)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string updateKH = "UPDATE NHANVIEN SET MANHANVIEN = @MANHANVIEN, HOTEN = @HOTEN, NGAYSINH = @NGAYSINH,GIOITINH = @GIOITINH,DIACHI = @DIACHI,CMND = @CMND,SDT= @SDT, EMAIL=@EMAIL,GHICHU=@GHICHU, ANH = @ANH where MANHANVIEN = @MANHANVIEN";
            using (SqlCommand command = new SqlCommand(updateKH, conn))
            {
                command.Parameters.AddWithValue("@MANHANVIEN", nv.maNhanVien);
                command.Parameters.AddWithValue("@HOTEN", nv.Hoten);
                command.Parameters.AddWithValue("@NGAYSINH", nv.Ngaysinh);


                command.Parameters.AddWithValue("@GIOITINH", nv.Gioitinh);
                command.Parameters.AddWithValue("@CMND", nv.CMND);

                command.Parameters.AddWithValue("@DIACHI", nv.diaChi);
                command.Parameters.AddWithValue("@SDT", nv.SDT);
                command.Parameters.AddWithValue("@EMAIL", nv.Email);

                command.Parameters.AddWithValue("@GHICHU", nv.GhiChu);
                command.Parameters.AddWithValue("@ANH", nv.Anh);
                int rowsAffected = command.ExecuteNonQuery();
                if (rowsAffected > 0)
                {
                    return "success";
                }
                else
                {
                    return "fail";
                }
            }
        }

        // XEM THÔNG TIN LIST
        public static List<string> xemMAHLV(string chucvu)
        {
            List<string> listmaHLV = new List<string>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string readinf = "select MANHANVIEN from NHANVIEN where CHUCVU = @CHUCVU";
            using (SqlCommand command = new SqlCommand(readinf, conn))
            {
                command.Parameters.AddWithValue("@CHUCVU", chucvu);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    string mahlv = (string)reader["MANHANVIEN"];
                    listmaHLV.Add(mahlv);

                }
                reader.Close();
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }


            }
            return listmaHLV;

        }
        public static List<string> xemListMaThe()
        {
            List<string> listmathe = new List<string>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string readinf= "select MATHE FROM KHACHHANG";
            using(SqlCommand cmd = new SqlCommand(readinf,conn))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                
                    while(reader.Read())
                    {
                        string manv = (string)reader["MATHE"];
                        listmathe.Add(manv);
                    }
                    reader.Close();
                


            }
            conn.Close();
            return listmathe;

        }
        public static List<string> xemListMaTBBT()
        {
            List<string> listMaTBBT = new List<string>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string readinf = "select MATHIETBI from THIETBI where TINHTRANG = 'BAOTRI'";
            using(SqlCommand cmd = new SqlCommand(readinf,conn))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string mathietbi = (string)reader["MATHIETBI"];
                    listMaTBBT.Add(mathietbi);

                }
                reader.Close();
            }
            conn.Close();
            return listMaTBBT;

        }
        public static List<string> xemListMaSP()
        {
            List<string> listMaSP = new List<string>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string readinf = "select MASANPHAM from SANPHAM";
            using (SqlCommand cmd = new SqlCommand(readinf, conn))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string masanpham = (string)reader["MASANPHAM"];
                    listMaSP.Add(masanpham);

                }
                reader.Close();
            }
            conn.Close();
            return listMaSP;

        }
        
            public static List<string> xemGoiTap(string gioitinh)
        {
            List<string> maGoiTapList = new List<string>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string readinf = "select MAGOITAP from GOITAP where GHICHU = @gioitinh";
            using (SqlCommand command = new SqlCommand(readinf, conn))
            {
                command.Parameters.AddWithValue("@gioitinh", gioitinh);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    string maGoitap = (string)reader["MAGOITAP"];
                    maGoiTapList.Add(maGoitap);

                }
                reader.Close();
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }


            }


            return maGoiTapList;
        }
        public static List<NHANVIEN> xemNhanVien()
        {
            List<NHANVIEN> listNV = new List<NHANVIEN>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string readInf = "select MANHANVIEN,HOTEN,NGAYSINH,GIOITINH,CMND,DIACHI,SDT,EMAIL,GHICHU,CHUCVU from NHANVIEN where CHUCVU ='LETAN' or CHUCVU ='BAOVE' ";
            SqlCommand cmd = new SqlCommand(readInf, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                NHANVIEN nhanvien = new NHANVIEN();
                nhanvien.maNhanVien = (string)reader["MANHANVIEN"];
                nhanvien.Hoten = (string)reader["HOTEN"];
                nhanvien.Ngaysinh = (string)reader["NGAYSINH"];
                nhanvien.Email = (string)reader["EMAIL"];
                nhanvien.Gioitinh = (string)reader["GIOITINH"];
                nhanvien.CMND = (string)reader["CMND"];
                nhanvien.diaChi = (string)reader["DIACHI"];
                nhanvien.SDT = (string)reader["SDT"];
                nhanvien.GhiChu = (string)reader["GHICHU"];
                nhanvien.Chucvu = (string)reader["CHUCVU"];
                listNV.Add(nhanvien);
            }
            reader.Close();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            return listNV;
        }
        public static List<THIETBI> xemDanhsachTB(string loaitb)
        {
            List<THIETBI> lstbtheoloai = new List<THIETBI>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string selectTBtheoloai = "select MATHIETBI,TENTHIETBI,TINHTRANG,LOAITHIETBI from THIETBI where LOAITHIETBI = @LOAITHIETBI";
            using (SqlCommand cmd = new SqlCommand(selectTBtheoloai,conn))
            {
                cmd.Parameters.AddWithValue("@LOAITHIETBI", loaitb);
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    THIETBI tb = new THIETBI();
                    tb.Mathietbi = (string)reader["MATHIETBI"];
                    tb.Loaithietbi = (string)reader["LOAITHIETBI"];
                    tb.Tenthietbi = (string)reader["TENTHIETBI"];
                    tb.Tinhtrang = (string)reader["TINHTRANG"];
                    lstbtheoloai.Add(tb);

                }
                return lstbtheoloai;
            }
            conn.Close();
        }
        public static List<THIETBI> xemTB()
        {
            List<THIETBI> listTB = new List<THIETBI>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string readInf = "select MATHIETBI,TENTHIETBI,LOAITHIETBI,TINHTRANG from THIETBI ";
            SqlCommand cmd = new SqlCommand(readInf, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                THIETBI tb = new THIETBI();
                tb.Loaithietbi = (string)reader["LOAITHIETBI"];
                tb.Mathietbi = (string)reader["MATHIETBI"];
                tb.Tenthietbi = (string)reader["TENTHIETBI"];
                tb.Tinhtrang = (string)reader["TINHTRANG"];
                
                listTB.Add(tb);
            }
            reader.Close();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            return listTB;
        }
        public static List<THIETBI> xemTBBT()
        {
            List<THIETBI> listTB = new List<THIETBI>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string readInf = "select MATHIETBI,TENTHIETBI,TINHTRANG,LOAITHIETBI from THIETBI where TINHTRANG ='BAOTRI' ";
            SqlCommand cmd = new SqlCommand(readInf, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                THIETBI tb = new THIETBI();
                tb.Mathietbi = (string)reader["MATHIETBI"];
                tb.Tenthietbi = (string)reader["TENTHIETBI"];
                tb.Tinhtrang = (string)reader["TINHTRANG"];
                tb.Loaithietbi = (string)reader["LOAITHIETBI"];
                listTB.Add(tb);
            }
            reader.Close();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            return listTB;
        }
        public static List<NHANVIEN> xemHuanLuyenVien()
        {
            List<NHANVIEN> listHLV = new List<NHANVIEN>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string readInf = "select MANHANVIEN,HOTEN,NGAYSINH,GIOITINH,CMND,DIACHI,SDT,EMAIL,GHICHU,CHUCVU from NHANVIEN where CHUCVU ='HUANLUYENVIEN' ";
            SqlCommand cmd = new SqlCommand(readInf, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                NHANVIEN nhanvien = new NHANVIEN();
                nhanvien.maNhanVien = (string)reader["MANHANVIEN"];
                nhanvien.Hoten = (string)reader["HOTEN"];
                nhanvien.Ngaysinh = (string)reader["NGAYSINH"];
                nhanvien.Email = (string)reader["EMAIL"];
                nhanvien.Gioitinh = (string)reader["GIOITINH"];
                nhanvien.CMND = (string)reader["CMND"];
                nhanvien.diaChi = (string)reader["DIACHI"];
                nhanvien.SDT = (string)reader["SDT"];
                nhanvien.GhiChu = (string)reader["GHICHU"];
                nhanvien.Chucvu = (string)reader["CHUCVU"];
                listHLV.Add(nhanvien);
            }
            reader.Close();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            return listHLV;
        }
        public static List<KHACHHANG> XemKH()
        {
            List<KHACHHANG> listKH = new List<KHACHHANG>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string readInf = "select MATHE,MANHANVIEN,HOTEN,NGAYSINH,GIOITINH,CMND,DIACHI,SDT,EMAIL,GHICHU,MAGOITAP,NGAYDANGKI,NGAYHETHAN,TENPT, MANHANVIEN,GOITAPPT,THOIHAN from KHACHHANG  ";
            SqlCommand cmd = new SqlCommand(readInf, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                KHACHHANG kh = new KHACHHANG();
                kh.Mathe = (string)reader["MATHE"];
                kh.maNhanVien = (string)reader["MANHANVIEN"];
                kh.Hoten = (string)reader["HOTEN"];
                kh.Ngaysinh = (string)reader["NGAYSINH"];
                kh.Email = (string)reader["EMAIL"];
                kh.Gioitinh = (string)reader["GIOITINH"];
                kh.CMND = (string)reader["CMND"];
                kh.diaChi = (string)reader["DIACHI"];
                kh.SDT = (string)reader["SDT"];
                kh.GhiChu = (string)reader["GHICHU"];
                kh.maGoiTap = (string)reader["MAGOITAP"];
                kh.NgayBatDau = (DateTime)reader["NGAYDANGKI"];
                kh.NgayHetHan = (DateTime)reader["NGAYHETHAN"];
                kh.TenPT = (string)reader["TENPT"];
                kh.GoitapPT = (string)reader["GOITAPPT"];
                kh.ThoiHan = (DateTime)reader["THOIHAN"];
                listKH.Add(kh);
            }
            reader.Close();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            return listKH;
        }
        public static List<SanPham> xemListSP()
        {
            List<SanPham> lsp = new List<SanPham>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "select MASANPHAM,LOAISANPHAM,TENSANPHAM,GIATHANH,SOLUONG,ANH FROM SANPHAM";
            using(SqlCommand cmd = new SqlCommand(query,conn))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    SanPham sp = new SanPham();
                    sp.MaSP = (string)reader["MASANPHAM"];
                    sp.LoaiSP = (string)reader["LOAISANPHAM"];
                    sp.TenSP = (string)reader["TENSANPHAM"];
                    sp.GiaThanh = (int)reader["GIATHANH"];
                    sp.SL = (int)reader["SOLUONG"];
                    sp.Anh = (byte[])reader["ANH"];
                    lsp.Add(sp);
                }
                reader.Close();
            }
            conn.Close();
            return lsp;
        }
        public static List<SanPham> xemListSPtheoLoai(string loaisp)
        {
            List<SanPham> lsp = new List<SanPham>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "select MASANPHAM,LOAISANPHAM,TENSANPHAM,GIATHANH,SOLUONG,ANH FROM SANPHAM WHERE LOAISANPHAM=@LOAISANPHAM";
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                cmd.Parameters.AddWithValue("@LOAISANPHAM", loaisp);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    SanPham sp = new SanPham();
                    sp.MaSP = (string)reader["MASANPHAM"];
                    sp.LoaiSP = (string)reader["LOAISANPHAM"];
                    sp.TenSP = (string)reader["TENSANPHAM"];
                    sp.GiaThanh = (int)reader["GIATHANH"];
                    sp.SL = (int)reader["SOLUONG"];
                    sp.Anh = (byte[])reader["ANH"];
                    lsp.Add(sp);
                }
                reader.Close();
            }
            conn.Close();
            return lsp;
        }
        public static List<GIAODICH> xemListGDTheoloai(string loaichiphi)
        {
            List<GIAODICH> lgd = new List<GIAODICH>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "select MAGIAODICH,LOAICHIPHI,CHIPHI,MASANPHAM,MATHIETBI,MOTA,NGAYGIAODICH FROM GIAODICH WHERE LOAICHIPHI =@LOAICHIPHI  ";
            using(SqlCommand cmd = new SqlCommand(query,conn))
            {
                cmd.Parameters.AddWithValue("@LOAICHIPHI", loaichiphi);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    GIAODICH gd = new GIAODICH();
                    gd.MaGD = (string)reader["MAGIAODICH"];
                    gd.LoaiChiPhi = (string)reader["LOAICHIPHI"];
                    gd.ChiPhi = Convert.ToInt64(reader["CHIPHI"]);
                    gd.MaSP = (string)reader["MASANPHAM"];
                    gd.MaTB = (string)reader["MATHIETBI"];
                    gd.MoTa = (string)reader["MOTA"];
                    gd.NgayGD = (DateTime)reader["NGAYGIAODICH"];
                    lgd.Add(gd);
                }
                reader.Close();
                   
            }
            conn.Close();
            return lgd;

        }
        public static List<GIAODICH> xemListGD()
        {
            List<GIAODICH> lgd = new List<GIAODICH>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "select MAGIAODICH,LOAICHIPHI,CHIPHI,MASANPHAM,MATHIETBI,MOTA,NGAYGIAODICH FROM GIAODICH ";
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
               
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    GIAODICH gd = new GIAODICH();
                    gd.MaGD = (string)reader["MAGIAODICH"];
                    gd.LoaiChiPhi = (string)reader["LOAICHIPHI"];
                    gd.ChiPhi = Convert.ToInt64(reader["CHIPHI"]);
                    gd.MaSP = (string)reader["MASANPHAM"];
                    gd.MaTB = (string)reader["MATHIETBI"];
                    gd.MoTa = (string)reader["MOTA"];
                    gd.NgayGD = (DateTime)reader["NGAYGIAODICH"];
                    lgd.Add(gd);
                }
                reader.Close();

            }
            conn.Close();
            return lgd;

        }

        //THÊM DỮ LIỆU
        public static bool ThemGiaoDich(GIAODICH gd)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "INSERT INTO GIAODICH (MAGIAODICH,LOAICHIPHI,CHIPHI,MASANPHAM,MATHIETBI,MOTA,NGAYGIAODICH)VALUES(@MAGIAODICH,@LOAICHIPHI,@CHIPHI,@MASANPHAM,@MATHIETBI,@MOTA,@NGAYGIAODICH)";
            using(SqlCommand cmd = new SqlCommand(query,conn))
            {
                cmd.Parameters.AddWithValue("@MAGIAODICH", gd.MaGD);
                cmd.Parameters.AddWithValue("@LOAICHIPHI", gd.LoaiChiPhi);
                cmd.Parameters.AddWithValue("@CHIPHI", gd.ChiPhi);
                cmd.Parameters.AddWithValue("@MASANPHAM", gd.MaSP);
                cmd.Parameters.AddWithValue("@MATHIETBI", gd.MaTB);
                cmd.Parameters.AddWithValue("@MOTA", gd.MoTa);
                cmd.Parameters.AddWithValue("@NGAYGIAODICH", gd.NgayGD);
                int rowsAffected = cmd.ExecuteNonQuery();
                if (rowsAffected > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool ThemHoaDon(HOADON hd)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "INSERT INTO HOADON (MAHOADON,MASANPHAM,NGAYTHANHTOAN,TONGTIEN,MAKHACHHANG) VALUES (@MAHOADON,@MASANPHAM,@NGAYTHANHTOAN,@TONGTIEN,@MAKHACHHANG)";
            using (SqlCommand cmd = new SqlCommand(query,conn))
            {
                cmd.Parameters.AddWithValue("@MAHOADON", hd.MaHoaDon);
                cmd.Parameters.AddWithValue("@MASANPHAM", hd.MaSP);
                cmd.Parameters.AddWithValue("@NGAYTHANHTOAN", hd.NgayThanhToan);
                cmd.Parameters.AddWithValue("@TONGTIEN", hd.TongTien);
                cmd.Parameters.AddWithValue("@MAKHACHHANG", hd.MaThe);
                int rowsAffected = cmd.ExecuteNonQuery();
                if (rowsAffected > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static bool themThietBi(THIETBI tb)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();

            string query = "INSERT INTO THIETBI (MATHIETBI, TENTHIETBI,LOAITHIETBI, TINHTRANG, ANH) VALUES (@MATHIETBI, @TENTHIETBI,@LOAITHIETBI, @TINHTRANG, @ANH)";
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                bool isPrimaryKeyDuplicate = CheckDuplicate(tb.Mathietbi);

                if (isPrimaryKeyDuplicate == true)
                {
                    command.Parameters.AddWithValue("@MATHIETBI", tb.Mathietbi);
                    command.Parameters.AddWithValue("@TENTHIETBI", tb.Tenthietbi);
                    command.Parameters.AddWithValue("@TINHTRANG", tb.Tinhtrang);
                    command.Parameters.AddWithValue("@ANH", tb.Anh);
                    command.Parameters.AddWithValue("@LOAITHIETBI", tb.Loaithietbi);

                }
                else
                {
                    return false;
                }

                int rowsAffected = command.ExecuteNonQuery();

                if (rowsAffected > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }
        public static bool themNhanVien(NHANVIEN nv)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();

            string query = "INSERT INTO NHANVIEN (MANHANVIEN,HOTEN,NGAYSINH,GIOITINH,CMND,DIACHI,SDT,EMAIL,GHICHU,CHUCVU,ANH) VALUES (@MANHANVIEN,@HOTEN,@NGAYSINH,@GIOITINH,@CMND,@DIACHI,@SDT,@EMAIL,@GHICHU,@CHUCVU,@ANH)";
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                bool isPrimaryKeyDuplicate = kiemtraNhanVien(nv.maNhanVien);

                if (isPrimaryKeyDuplicate == true)
                {
                    command.Parameters.AddWithValue("@MANHANVIEN", nv.maNhanVien);
                    command.Parameters.AddWithValue("@HOTEN", nv.Hoten);
                    command.Parameters.AddWithValue("@NGAYSINH", nv.Ngaysinh);

                    command.Parameters.AddWithValue("@GIOITINH", nv.Gioitinh);
                    command.Parameters.AddWithValue("@CMND", nv.CMND);
                    command.Parameters.AddWithValue("@DIACHI", nv.diaChi);

                    command.Parameters.AddWithValue("@SDT", nv.SDT);
                    command.Parameters.AddWithValue("@EMAIL", nv.Email);
                    command.Parameters.AddWithValue("@GHICHU", nv.GhiChu);

                    command.Parameters.AddWithValue("@CHUCVU", nv.Chucvu);
                    command.Parameters.AddWithValue("@ANH", nv.Anh);
                }
                else
                {
                    return false;
                }

                int rowsAffected = command.ExecuteNonQuery();

                if (rowsAffected > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool themTaiKhoan(KHACHHANG kh)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();

            string query = "INSERT INTO KHACHHANG (MATHE,HOTEN,NGAYSINH,GIOITINH,CMND,DIACHI,SDT,EMAIL,GHICHU,MAGOITAP,NGAYDANGKI,NGAYHETHAN,TENPT, MANHANVIEN,GOITAPPT,THOIHAN,ANH) VALUES (@MATHE,@HOTEN,@NGAYSINH,@GIOITINH,@CMND,@DIACHI,@SDT,@EMAIL,@GHICHU,@MAGOITAP,@NGAYDANGKI,@NGAYHETHAN,@TENPT,@MANHANVIEN,@GOITAPPT,@THOIHAN,@ANH)";
            string selectQuery = "SELECT HOTEN FROM NHANVIEN WHERE MANHANVIEN = @MANHANVIEN";
            using (SqlCommand selectCommand = new SqlCommand(selectQuery, conn))
            {
                selectCommand.Parameters.AddWithValue("@MANHANVIEN", kh.maNhanVien);
                string TENPT = (string)selectCommand.ExecuteScalar();
               
                using (SqlCommand command = new SqlCommand(query, conn))
                {


                    bool isPrimaryKeyDuplicate = CheckPrimaryKeyDuplicate(kh.Mathe);

                    if (isPrimaryKeyDuplicate == true)
                    {
                        command.Parameters.AddWithValue("@MATHE", kh.Mathe);
                        command.Parameters.AddWithValue("@HOTEN", kh.Hoten);
                        command.Parameters.AddWithValue("@NGAYSINH", kh.Ngaysinh);

                        command.Parameters.AddWithValue("@MANHANVIEN", kh.maNhanVien);
                        command.Parameters.AddWithValue("@GIOITINH", kh.Gioitinh);
                        command.Parameters.AddWithValue("@CMND", kh.CMND);

                        command.Parameters.AddWithValue("@DIACHI", kh.diaChi);
                        command.Parameters.AddWithValue("@SDT", kh.SDT);
                        command.Parameters.AddWithValue("@EMAIL", kh.Email);
                        
                        command.Parameters.AddWithValue("@GHICHU", kh.GhiChu);
                        command.Parameters.AddWithValue("@MAGOITAP", kh.maGoiTap);
                        command.Parameters.AddWithValue("@NGAYDANGKI", kh.NgayBatDau);

                        command.Parameters.AddWithValue("@NGAYHETHAN", kh.NgayHetHan);
                        command.Parameters.AddWithValue("@TENPT", TENPT);
                        command.Parameters.AddWithValue("@GOITAPPT", kh.GoitapPT);
                        command.Parameters.AddWithValue("@THOIHAN", kh.ThoiHan);
                        command.Parameters.AddWithValue("@ANH", kh.Anh);
                    }
                    else
                    {
                        return false;
                    }

                    int rowsAffected = command.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        public static void ThemBienLai(BIENLAI bl)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "INSERT INTO BIENLAI (MABIENLAI,MATHE,MAGOITAP,GOITAPPT,TONGTIEN,NGAYTHANHTOAN) VALUES (@MABIENLAI,@MATHE,@MAGOITAP,@GOITAPPT,@TONGTIEN,@NGAYTHANHTOAN)";
            string select = "SELECT CHIPHI FROM GOITAP WHERE MAGOITAP = @MAGOITAP";
            using(SqlCommand sellectcmd = new SqlCommand(select,conn))
            {
                sellectcmd.Parameters.AddWithValue("@MAGOITAP", bl.MaGoiTap);
                int chiphigoitap = (int)sellectcmd.ExecuteScalar();
                sellectcmd.Parameters.Clear();
                sellectcmd.Parameters.AddWithValue("@MAGOITAP", bl.GoiTapPT);
                int chiphigoitapPT = (int)sellectcmd.ExecuteScalar();
                int tongtien = chiphigoitap + chiphigoitapPT;
                using(SqlCommand cmd = new SqlCommand(query,conn))
                {
                    cmd.Parameters.AddWithValue("@MABIENLAI", bl.MaBienLai);
                    cmd.Parameters.AddWithValue("@MATHE", bl.MaThe);
                    cmd.Parameters.AddWithValue("@MAGOITAP", bl.MaGoiTap);
                    cmd.Parameters.AddWithValue("@GOITAPPT",bl.GoiTapPT);
                    cmd.Parameters.AddWithValue("@NGAYTHANHTOAN", bl.NgayThanhToan);
                    cmd.Parameters.AddWithValue("@TONGTIEN",tongtien);
                    int rowsAffected = cmd.ExecuteNonQuery();
                }

            }
            conn.Close();
        }
        public static bool ThemSanPham(SanPham sp)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "INSERT INTO SANPHAM (MASANPHAM,LOAISANPHAM,TENSANPHAM,GIATHANH,SOLUONG,ANH) VALUES (@MASANPHAM,@LOAISANPHAM,@TENSANPHAM,@GIATHANH,@SOLUONG,@ANH)";
            using (SqlCommand cmd = new SqlCommand(query,conn))
            {
                cmd.Parameters.AddWithValue("@MASANPHAM", sp.MaSP);
                cmd.Parameters.AddWithValue("@LOAISANPHAM", sp.LoaiSP);
                cmd.Parameters.AddWithValue("@TENSANPHAM", sp.TenSP);
                cmd.Parameters.AddWithValue("@GIATHANH", sp.GiaThanh);
                cmd.Parameters.AddWithValue("@SOLUONG", sp.SL);
                cmd.Parameters.AddWithValue("@ANH", sp.Anh);
                int rowsAffected = cmd.ExecuteNonQuery();
                if(rowsAffected>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            
        }


        // KIỂM TRA PRIMARY KEY && Count dữ liệu
        public static int demsoKH()
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "SELECT COUNT(*) FROM KHACHHANG";
            using(SqlCommand cmd = new SqlCommand(query,conn))
            {
                int count = (int)cmd.ExecuteScalar();
                return count;
            }    
        }
        public static int demHoaDon()
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "SELECT COUNT(*) FROM HOADON";
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                int count = (int)cmd.ExecuteScalar();
                return count;
            }
            conn.Close();
        }
        public static int demGD(string loaichiphi)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "SELECT COUNT(*) FROM GIAODICH WHERE LOAICHIPHI = @LOAICHIPHI";
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                cmd.Parameters.AddWithValue("@LOAICHIPHI", loaichiphi);
                int count = (int)cmd.ExecuteScalar();
                return count;
            }
            conn.Close();
        }
        public static int demSanPham(string loaisp)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "SELECT COUNT(*) FROM SANPHAM WHERE LOAISANPHAM = @LOAISANPHAM";
            using(SqlCommand cmd = new SqlCommand(query,conn))
            {
                cmd.Parameters.AddWithValue("@LOAISANPHAM", loaisp);
                int count = (int)cmd.ExecuteScalar();
                return count;
            }
            conn.Close();
        }
        public static int demsoNV(string chucvu)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "SELECT COUNT(*) FROM NHANVIEN WHERE CHUCVU = @CHUCVU";
            using (SqlCommand cmd = new SqlCommand(query,conn))
            {
                cmd.Parameters.AddWithValue("@CHUCVU", chucvu);
                int count = (int)cmd.ExecuteScalar();
                return count;
            }
            conn.Close();
        }
        public static int demsoTB(string loaitb)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "SELECT COUNT(*) FROM THIETBI WHERE LOAITHIETBI = @LOAITHIETBI";
            using (SqlCommand cmd = new SqlCommand(query,conn))
            {
                cmd.Parameters.AddWithValue("@LOAITHIETBI", loaitb);
                int count = (int)cmd.ExecuteScalar();
                return count;
            }
        }
        
        public static bool CheckPrimaryKeyDuplicate(string primaryKey)//Kiem tra khoa chinh
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "SELECT COUNT(*) FROM KHACHHANG WHERE MATHE = @MATHE";
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@MATHE", primaryKey);
                int count = (int)command.ExecuteScalar();
                if (count > 0)// da co khoa chinh 
                {
                    return false;
                }
                else 
                {
                    return true;
                }

            }
        }
        public static bool kiemtraGoiTap(string magoitap)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "SELECT COUNT(*) FROM GOITAP WHERE MAGOITAP = @MAGOITAP";
            using(SqlCommand cmd = new SqlCommand(query,conn))
            {
                cmd.Parameters.AddWithValue("@MAGOITAP", magoitap);
                int count = (int)cmd.ExecuteScalar();
                if (count > 0)// da co khoa chinh 
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public static bool CheckDuplicate(string primaryKey)//Kiem tra khoa chinh
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "SELECT COUNT(*) FROM THIETBI WHERE MATHIETBI = @MATHIETBI";
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@MATHIETBI", primaryKey);
                int count = (int)command.ExecuteScalar();
                if (count > 0)// da co khoa chinh 
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }
        public static bool kiemtraNhanVien(string primaryKey)//Kiem tra khoa chinh
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "SELECT COUNT(*) FROM NHANVIEN WHERE MANHANVIEN = @MANHANVIEN";
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@MANHANVIEN", primaryKey);
                int count = (int)command.ExecuteScalar();
                if (count > 0)// da co khoa chinh 
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }

        
       // XEM THÔNG TIN CHI TIẾT
        public static KHACHHANG xemThongTinKH(string mathe)
        {
            KHACHHANG kh = new KHACHHANG();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string readInf = "select MATHE, MANHANVIEN,HOTEN,NGAYSINH,GIOITINH,CMND,DIACHI,SDT,EMAIL,GHICHU,MAGOITAP,NGAYDANGKI,NGAYHETHAN,TENPT, MANHANVIEN,GOITAPPT,THOIHAN, ANH from KHACHHANG where MATHE ='" + mathe + "'";
            SqlCommand cmd = new SqlCommand(readInf, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                
                kh.Mathe = (string)reader["MATHE"];
                kh.maNhanVien = (string)reader["MANHANVIEN"];
                kh.Hoten = (string)reader["HOTEN"];
                kh.Ngaysinh = (string)reader["NGAYSINH"];
                kh.Email = (string)reader["EMAIL"];
                kh.Gioitinh = (string)reader["GIOITINH"];
                kh.CMND = (string)reader["CMND"];
                kh.diaChi = (string)reader["DIACHI"];
                kh.SDT = (string)reader["SDT"];
                kh.GhiChu = (string)reader["GHICHU"];
                kh.maGoiTap = (string)reader["MAGOITAP"];
                kh.NgayBatDau = (DateTime)reader["NGAYDANGKI"];
                kh.NgayHetHan = (DateTime)reader["NGAYHETHAN"];
                kh.TenPT = (string)reader["TENPT"];
                kh.GoitapPT = (string)reader["GOITAPPT"];
                kh.ThoiHan = (DateTime)reader["THOIHAN"];
                kh.Anh = (byte[])reader["ANH"];
                
            }
            return kh;
        }
        public static SanPham xemthongtinSP(string masp)
        {
            SanPham sp = new SanPham();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "SELECT MASANPHAM,LOAISANPHAM,TENSANPHAM,GIATHANH,SOLUONG,ANH FROM SANPHAM WHERE MASANPHAM = @MASANPHAM";
            using (SqlCommand cmd = new SqlCommand(query,conn))
            {
                cmd.Parameters.AddWithValue("@MASANPHAM", masp);
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    sp.MaSP = (string)reader["MASANPHAM"];
                    sp.LoaiSP = (string)reader["LOAISANPHAM"];
                    sp.TenSP = (string)reader["TENSANPHAM"];
                    sp.GiaThanh = (int)reader["GIATHANH"];
                    sp.SL = (int)reader["SOLUONG"];
                    sp.Anh = (byte[])reader["ANH"];

                }
                reader.Close();
            }
            conn.Close();
            return sp;
        }
        public static THIETBI xemThongTinTB(string maTB)
        {
            THIETBI tb = new THIETBI();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string readInf = "select MATHIETBI,LOAITHIETBI,TENTHIETBI,TINHTRANG,ANH from THIETBI where MATHIETBI = @MATHIETBI";
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(readInf, conn))
            {
                cmd.Parameters.AddWithValue("@MATHIETBI", maTB);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    tb.Mathietbi = (string)reader["MATHIETBI"];
                    tb.Loaithietbi = (string)reader["LOAITHIETBI"];
                    tb.Tenthietbi = (string)reader["TENTHIETBI"];
                    tb.Tinhtrang = (string)reader["TINHTRANG"];
                    tb.Anh = (byte[])reader["ANH"];
                }
                reader.Close();

                
            }
            conn.Close();
            return tb;
        }
        public static int LoaiGoiTap(string magoitap)
        {
            int loaitap = 0;
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string readinf = "select THANG from GOITAP where MAGOITAP = @magoitap";
            using (SqlCommand command = new SqlCommand(readinf, conn))
            {
                command.Parameters.AddWithValue("@magoitap", magoitap);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    loaitap = Convert.ToInt32(reader["THANG"]);
                  
                }
                reader.Close();
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }


            }


            return loaitap; 
        }
        public static BIENLAI xembienlai(string mabienlai)
        {
            BIENLAI bl = new BIENLAI();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string readinf = "select MABIENLAI,MATHE,MAGOITAP,GOITAPPT,TONGTIEN,NGAYTHANHTOAN FROM BIENLAI WHERE MABIENLAI = @MABIENLAI";
            conn.Open();
            using(SqlCommand cmd = new SqlCommand(readinf,conn))
            {
                cmd.Parameters.AddWithValue("@MABIENLAI", mabienlai);
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    bl.MaBienLai = (string)reader["MABIENLAI"];
                    bl.MaThe = (string)reader["MATHE"];
                    bl.MaGoiTap = (string)reader["MAGOITAP"];
                    bl.GoiTapPT = (string)reader["GOITAPPT"];
                    bl.NgayThanhToan = (DateTime)reader["NGAYTHANHTOAN"];
                    bl.TongTien = Convert.ToInt32(reader["TONGTIEN"]);
                    
                }
                return bl;

            }
            conn.Close();

        }
        public static NHANVIEN xemThongTinNV(string manv)
        {
            NHANVIEN nv = new NHANVIEN();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string readInf = "select MANHANVIEN,HOTEN,NGAYSINH,GIOITINH,CMND,DIACHI,SDT,EMAIL,GHICHU,CHUCVU,ANH from NHANVIEN where MANHANVIEN ='" + manv + "'";
            SqlCommand cmd = new SqlCommand(readInf, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {

                nv.maNhanVien = (string)reader["MANHANVIEN"];
                nv.Hoten = (string)reader["HOTEN"];
                nv.Ngaysinh = (string)reader["NGAYSINH"];
                nv.Email = (string)reader["EMAIL"];
                nv.Gioitinh = (string)reader["GIOITINH"];
                nv.CMND = (string)reader["CMND"];
                nv.diaChi = (string)reader["DIACHI"];
                nv.SDT = (string)reader["SDT"];
                nv.GhiChu = (string)reader["GHICHU"];
                nv.Chucvu = (string)reader["CHUCVU"];
                nv.Anh = (byte[])reader["ANH"];

            }
            return nv;
        }

        

        // XÓA DỮ LIỆU
        public static bool XoaThongtinKH(string mathe)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "DELETE FROM KHACHHANG WHERE MATHE = @MaTHE";
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@MaTHE", mathe);
                command.ExecuteNonQuery();
                int count = command.ExecuteNonQuery();
                if(count > 0)
                {
                    return true;
                }
                else
                {
                    return false;

                } 
                    

            }
        }
        public static bool XoaThongtinNV(string manv)
        {
            SqlConnection conn = SqlconnectionData.sqlconnect();
            conn.Open();
            string query = "DELETE FROM NHANVIEN WHERE MANHANVIEN = @MANHANVIEN";
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@MANHANVIEN", manv);
                command.ExecuteNonQuery();
                int count = command.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;

                }


            }
        }
       







        // Thống kê
        public static List<KHACHHANG> thongkeKhachHang(DateTime d1, DateTime d2)
        {
            List<KHACHHANG> ltkkh = new List<KHACHHANG>();
            SqlConnection conn = SqlconnectionData.sqlconnect();
            string query = "SELECT * FROM KHACHHANG WHERE NGAYDANGKI >= @FromDate AND NGAYDANGKI <= @ToDate";
            conn.Open();
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@FromDate", d1);
                command.Parameters.AddWithValue("@ToDate", d2);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    KHACHHANG kh = new KHACHHANG();
                    kh.Mathe = (string)reader["MATHE"];
                    kh.maNhanVien = (string)reader["MANHANVIEN"];
                    kh.Hoten = (string)reader["HOTEN"];
                    kh.Ngaysinh = (string)reader["NGAYSINH"];
                    kh.Email = (string)reader["EMAIL"];
                    kh.Gioitinh = (string)reader["GIOITINH"];
                    kh.CMND = (string)reader["CMND"];
                    kh.diaChi = (string)reader["DIACHI"];
                    kh.SDT = (string)reader["SDT"];
                    kh.GhiChu = (string)reader["GHICHU"];
                    kh.maGoiTap = (string)reader["MAGOITAP"];
                    kh.NgayBatDau = (DateTime)reader["NGAYDANGKI"];
                    kh.NgayHetHan = (DateTime)reader["NGAYHETHAN"];
                    kh.TenPT = (string)reader["TENPT"];
                    kh.GoitapPT = (string)reader["GOITAPPT"];
                    kh.ThoiHan = (DateTime)reader["THOIHAN"];
                    ltkkh.Add(kh);
                }
                reader.Close();
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                return ltkkh;

            }
        }
    }
}
