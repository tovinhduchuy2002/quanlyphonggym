﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DTO;
using BLL;

namespace GUI
{   
    
    public partial class fManHinhDangNhap : Form
    {
        SqlConnection sqlconnect;
        TaiKhoan taikhoan = new TaiKhoan();
        TaiKhoanBLL TKBLL = new TaiKhoanBLL();
       
        public fManHinhDangNhap()
        {
            InitializeComponent();
            
        }
        private void fManHinhDangNhap_Load(object sender, EventArgs e)
        {
            tbMauKhau.PasswordChar = '*';
        }
        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            taikhoan.maTaiKhoan = tbTaiKhoan.Text;
            taikhoan.MatKhau = tbMauKhau.Text;
            string getuser = TKBLL.Checklogin(taikhoan);
            int ltk = TKBLL.CheckLTK(taikhoan);
            switch (getuser)
            {
                case "mataikhoantrang":
                    MessageBox.Show("Nhập tên tài khoản");
                    return;
                case "matkhautrang":
                    MessageBox.Show("Nhap mật khẩu");
                    return;               
                case "success":
                    MessageBox.Show("Đăng nhập thành công");
                    Phanquyen(ltk);
                    return;
                case "saiTK":
                    MessageBox.Show("Sai tài khoản");
                    return;
                case "saimk":
                    MessageBox.Show("Sai mật khẩu");
                    return;
                
            }

        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void Phanquyen(int ltk)
        {
            if (ltk == 1)
            {
                this.Hide();
                fTrangChu fTC = new fTrangChu();
                fTC.ShowDialog();
                this.Show();
            }
            else
            {
                MessageBox.Show("haha");
            }
        }

        private void fManHinhDangNhap_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult r = MessageBox.Show("Bạn có muốn thoát?", "Thoát", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (r == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void btnQuenMatKhau_Click(object sender, EventArgs e)
        {
            this.Hide();
            fKhoiPhucMatKhau fkpmk = new fKhoiPhucMatKhau();
            fkpmk.ShowDialog();
            this.Show();
        }

       
    }
}
