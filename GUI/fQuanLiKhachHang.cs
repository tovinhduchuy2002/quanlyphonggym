﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class fQuanLiKhachHang : Form
    {
        public fQuanLiKhachHang()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            fDangKiThanhVien fdktv = new fDangKiThanhVien();
            this.Hide();
            fdktv.ShowDialog();
            this.Show();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnDSKH_Click(object sender, EventArgs e)
        {
            fDanSachKhachHang fdskh = new fDanSachKhachHang();
            this.Hide();
            fdskh.ShowDialog();
            this.Show();
        }
    }
}
