﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class GoiTapBLL
    {
        GoiTapAccess gtaccs = new GoiTapAccess();

        public List<string> xemMaGoitap(string gioitinh)
        {
            return gtaccs.xemgoiTap(gioitinh);
        }
        public int LoaiGoiTap(string magoitap)
        {
            return gtaccs.ThangGoiTap(magoitap);
        }
    }
}

