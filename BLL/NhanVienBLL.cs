﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
namespace BLL
{
    public class NhanVienBLL
    {
        NhanVienACcess nvaccess = new NhanVienACcess();
        public List<NHANVIEN> xemNhanVien()
        {
            List<NHANVIEN> lnv = new List<NHANVIEN>();
            lnv = nvaccess.xemLeTan();
            return lnv;
        }
        public string checkcapnhat(NHANVIEN nv)
        {
            string updatenv = nvaccess.UpdateNV(nv);
            return updatenv;
        }
        public List<NHANVIEN> xemHuanLuyenVien()
        {
            List<NHANVIEN> lhlv = new List<NHANVIEN>();
            lhlv= nvaccess.xemHuanLuyenVien();
            return lhlv;
        }
        public NHANVIEN xemThongTinNV(string manv)
        {
            NHANVIEN nv = new NHANVIEN();
            nv = nvaccess.xemthongTinNV(manv);
            return nv;
        }
        public NHANVIEN xemThongTinHLV(string manv)
        {
            NHANVIEN nv = new NHANVIEN();
            nv = nvaccess.xemthongTinHLV(manv);
            return nv;
        }
        public bool themNV(NHANVIEN nv)
        {
            int count = nvaccess.demsoNV(nv.Chucvu) + 1;

            if (nv.Chucvu == "HUANLUYENVIEN")
            {
                if(count < 10)
                {
                    nv.maNhanVien = "HUANLUYENVIEN00" + count.ToString();
                }
                else if(count >=10 && count < 100)
                {
                    nv.maNhanVien = "HUANLUYENVIEN0" + count.ToString();
                }
                else if (count >= 100)
                {
                    nv.maNhanVien = "HUANLUYENVIEN" + count.ToString();
                }
            }
            if(nv.Chucvu=="LETAN")
            {
                if (count < 10)
                {
                    nv.maNhanVien = "LETAN00" + count.ToString();
                }
                else if (count >= 10 && count < 100)
                {
                    nv.maNhanVien = "LETAN0" + count.ToString();
                }
                else if (count >= 100)
                {
                    nv.maNhanVien = "LETAN" + count.ToString();
                }
            }
            if (nv.Chucvu == "BAOVE")
            {
                if (count < 10)
                {
                    nv.maNhanVien = "BAOVE00" + count.ToString();
                }
                else if (count >= 10 && count < 100)
                {
                    nv.maNhanVien = "BAOVE0" + count.ToString();
                }
                else if (count >= 100)
                {
                    nv.maNhanVien = "BAOVE" + count.ToString();
                }

            }
            
            return nvaccess.ThemNV(nv);
        }
        public bool XoaNV(string manv)
        {
            return nvaccess.xoaNV(manv);
        }
    }
}
