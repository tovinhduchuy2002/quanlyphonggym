﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class GiaoDichAccess:DatabaseAccess
    {
        public bool themGiaoDich(GIAODICH gd)
        {
            return DatabaseAccess.ThemGiaoDich(gd);

        }
        public int demGD(string loaichiphi)
        {
            return DatabaseAccess.demGD(loaichiphi);
        }
        public List<GIAODICH> XemListGiaoDich()
        {
            return DatabaseAccess.xemListGD();
        }
        public List<GIAODICH> xemListGDtheoLoai(string loaichiphi)
        {
            return DatabaseAccess.xemListGDTheoloai(loaichiphi);
        }
        public List<string> xemListmaTBBT()
        {
            return DatabaseAccess.xemListMaTBBT();
        }
        public List<string> xemListMaSP()
        {
            return DatabaseAccess.xemListMaSP();
        }
    }
}
