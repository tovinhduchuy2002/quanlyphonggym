﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using System.Globalization;
using BLL;

namespace GUI
{
    public partial class fThonTinSP : Form
    {
        public fThonTinSP()
        {
            InitializeComponent();
        }
        public static string masp { get; set; }
        SanPhamBLL spbll = new SanPhamBLL();
        SanPham sp = new SanPham();
        bool checkluuanh = false;
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (cmbLoaiSP.SelectedIndex == -1)
            {
                MessageBox.Show("Chọn loại sản phẩm");
                return;
            }
            if (string.IsNullOrEmpty(tbTenSP.Text))
            {
                MessageBox.Show("Điền tên Sản Phẩm");
                return;
            }
            else
            {
                sp.TenSP = tbTenSP.Text;
            }
            if (string.IsNullOrEmpty(tbGiaBan.Text))
            {
                MessageBox.Show("Điền giá sản phẩm");
                return;
            }
            else
            {

                int thamso;

                if (int.TryParse(tbGiaBan.Text, out thamso) == true)
                {
                    if (thamso >= 0)
                    {
                        sp.GiaThanh = thamso;
                    }
                    else
                    {
                        MessageBox.Show("Giá bán không thể âm");
                        return;
                    }

                }
                else
                {
                    MessageBox.Show("Giá tiền phải là số nguyên dương ");
                    return;
                }





            }

            if (string.IsNullOrEmpty(tbSL.Text))
            {
                MessageBox.Show("Điền số lượng sản phẩm");
                return;
            }
            else
            {

                int thamso;

                if (int.TryParse((tbSL.Text), out thamso) == true)
                {

                    if (thamso >= 0)
                    {
                        sp.SL = thamso;
                    }
                    else
                    {
                        MessageBox.Show("Số lượng không thể âm");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Số lượng phải là số nguyên dương ");
                    return;
                }




            }
            if(checkluuanh == true)
            {
                sp.Anh = imageToByteArray(ptbSanPham);
            }
            string getupdate = spbll.updateSP(sp);
            if(getupdate == "success")
            {
                MessageBox.Show("Update sản phẩm thành công");
                return;
            }
            if(getupdate == "fail")
            {
                MessageBox.Show("Update sản phẩm không thành công");
                return;

            }


        }

        private void fThonTinSP_Load(object sender, EventArgs e)
        {
            LoadSP();
        }
        private void LoadSP()
        {
            sp = spbll.xemSP(masp);
            if(sp.LoaiSP == "DUNGCU")
            {
                string temp = "Dụng cụ hỗ trợ";
                cmbLoaiSP.SelectedItem = temp;
            }
            if(sp.LoaiSP == "THUCPHAM")
            {
                string temp = "Thực phẩm";
                cmbLoaiSP.SelectedItem = temp;
            }
            tbTenSP.Text = sp.TenSP;
            tbMSP.Text = sp.MaSP;
            tbMSP.Enabled = false;
            tbGiaBan.Text = sp.GiaThanh.ToString();
            tbSL.Text = sp.SL.ToString();
            using (MemoryStream ms = new MemoryStream(sp.Anh))
            {
                ptbSanPham.Image = Image.FromStream(ms);
            }

        }
        private byte[] imageToByteArray(PictureBox ptb)
        {
            using (MemoryStream ms = new MemoryStream())
            {

                ptb.Image.Save(ms, ptb.Image.RawFormat); // Thay đổi định dạng ảnh nếu cần thiết
                return ms.ToArray();
            }
        }

        private void ptbSanPham_Click(object sender, EventArgs e)
        {
            
                OpenFileDialog moFile = new OpenFileDialog();
                moFile.Title = "Chọn ảnh khách hàng";
                moFile.Filter = "Image Files(*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf)|*.gif;*.jpg;*.jpeg;*.bmp;*.png;*.wmf";
                if (moFile.ShowDialog() == DialogResult.OK)
                {
                    checkluuanh = true;
                    ptbSanPham.ImageLocation = moFile.FileName;
                }
            
        }
    }
}
