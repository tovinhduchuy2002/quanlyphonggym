﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;
namespace GUI
{
    public partial class fThongKeChiPhi : Form
    {
        public fThongKeChiPhi()
        {
            InitializeComponent();
        }
        GiaoDichBLL gdbll = new GiaoDichBLL();
        List<GIAODICH> listgd = new List<GIAODICH>();

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnXuatThongtin_Click(object sender, EventArgs e)
        {
            fThemChiPhi ftcp = new fThemChiPhi();
            this.Hide();
            ftcp.ShowDialog();
            this.Show();
        }

        private void fThongKeChiPhi_Load(object sender, EventArgs e)
        {
            LoadDatagrid();
            LoadListGD();
        }
        private void LoadDatagrid()
        {
            dtgChiPhi.Columns.Add("COLUMN1", "MAGIAODICH");
            dtgChiPhi.Columns.Add("COLUMN2", "LOAICHIPHI");
            dtgChiPhi.Columns.Add("COLUMN3", "MOTA");
            dtgChiPhi.Columns.Add("COLUMN4", "CHIPHI");
                
                
        }
        private void LoadListGD()
        {
            dtgChiPhi.Rows.Clear();
            listgd = gdbll.xemListGD();
            foreach(GIAODICH gd in listgd)
            {
                dtgChiPhi.Rows.Add(gd.MaGD, gd.LoaiChiPhi, gd.MoTa, gd.ChiPhi.ToString());
            }

        }
        private void LoadListGD(string item)
        {
            dtgChiPhi.Rows.Clear();
            listgd = gdbll.xemListGDtheoloai(item);
            foreach (GIAODICH gd in listgd)
            {
                dtgChiPhi.Rows.Add(gd.MaGD, gd.LoaiChiPhi, gd.MoTa, gd.ChiPhi.ToString());
            }
        }
        private void cmbLoaiChiPhi_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbLoaiChiPhi.SelectedItem == "Nhập hàng")
            {

                LoadListGD("NHAPHANG");
            }
            if (cmbLoaiChiPhi.SelectedItem == "Bảo trì")
            {
                LoadListGD("BAOTRI");
            }
            if (cmbLoaiChiPhi.SelectedItem == "Duy trì")
            {
                LoadListGD("DUYTRI");
            }
            if (cmbLoaiChiPhi.SelectedItem == "Lương")
            {
                LoadListGD("LUONG");
            }
        }
    }
}
