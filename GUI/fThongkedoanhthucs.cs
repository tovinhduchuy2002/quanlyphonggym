﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;
using OfficeOpenXml;
namespace GUI
{
    public partial class fThongkedoanhthucs : Form
    {
        public fThongkedoanhthucs()
        {
            InitializeComponent();
        }
        KHACHHANG kh = new KHACHHANG();
        KHBLL khbll = new KHBLL();
        
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();

        }
        List<KHACHHANG> ltkKH = new List<KHACHHANG>();
        DateTime fromdate = new DateTime();
        DateTime todate = new DateTime();
        private void LoadListKH()
        {
            
            if (!string.IsNullOrEmpty(tbNgayBD.Text))
                {
                    DateTime thamchieu;
                    if (DateTime.TryParseExact(tbNgayBD.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thamchieu))
                    {
                        fromdate = thamchieu;                      
                    }
                    else
                    {
                        MessageBox.Show("Nhập lại ngày bắt đầu");
                        return;

                    }

                }
            else
            {
                MessageBox.Show("Nhập lại ngày bắt đầu");
                return;

            }
            if (!string.IsNullOrEmpty(tbngaykethuc.Text))
            {
                DateTime thamchieu;
                if (DateTime.TryParseExact(tbngaykethuc.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out thamchieu))
                {
                    if(thamchieu <= fromdate)
                    {
                        MessageBox.Show("Nhập lại ngày kết thúc");
                        return;
                    }
                    else
                    {
                        todate = thamchieu;
                    }

                }
                else
                {
                    MessageBox.Show("Nhập lại ngày kết thúc");
                    return;

                }

            }
            else
            {
                MessageBox.Show("Nhập lại ngày kết thúc");
                return;
            }

            ltkKH = khbll.thongkeKH(fromdate, todate);
            foreach (KHACHHANG khachhang in ltkKH)
            {
                dtgDanhSach.Rows.Add(khachhang.Mathe, khachhang.Hoten, khachhang.NgayBatDau, khachhang.NgayHetHan, khachhang.TenPT);
            }

        }

        private void cmbChon_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbChon.SelectedItem.ToString() == "KHACHHANG")
            {
                LoadListKH();
                
            }
        }

        private void fThongkedoanhthucs_Load(object sender, EventArgs e)
        {
            dtgDanhSach.Columns.Add("Column1", "MATHE");
            dtgDanhSach.Columns.Add("Column2", "HOTEN");
            dtgDanhSach.Columns.Add("Column3", "NGAYDANGKI");
            dtgDanhSach.Columns.Add("Column4", "NGAYHETHAN");
            dtgDanhSach.Columns.Add("Column5", "TENPT");
        }

        private void btnXuatThongtin_Click(object sender, EventArgs e)
        {
            DateTime dateNow = DateTime.Now;
            string filepath = "";
            //Tạo saveDiaglouge
            SaveFileDialog save = new SaveFileDialog();
            //file filter
            save.Filter = "Excel Workbook|*.xlsx ";
            if (save.ShowDialog() == DialogResult.OK)
            {
                filepath = save.FileName;
            }
            if (string.IsNullOrEmpty(filepath))
            {
                MessageBox.Show("Đường dẫn không hợp lệ");
                return;
            }
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            try
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Properties.Author = "Sucsongmoi";
                    excel.Workbook.Properties.Title = "Danh sách khách hàng";
                    excel.Workbook.Worksheets.Add("Danh sách khách hàng");
                    ExcelWorksheet ws = excel.Workbook.Worksheets[0];
                    ws.Name = "Danh sách khách hàng";
                    ws.Cells.Style.Font.Size = 13;
                    ws.Cells.Style.Font.Name = "Tahoma";
                    string[] arrColumnheader =
                    {
                        "Mã thẻ",
                        "Họ tên",
                        "Ngày sinh",
                        "Giới tính",
                        "CMND",
                        "Địa chỉ",
                        "SDT",
                        "Email",
                        "Ghi chú",
                        "Loại",
                        "Ngày đăng kí",
                        "Ngày hết hạn",
                        "Tên PT",
                        "Mã nhân viên",
                        "Gói tập",
                        "Thời hạn",

                    };
                    var countColumnHeader = arrColumnheader.Count();
                    //Merge va dat ten cho cell 1
                    
                    ws.Cells[1, 1].Value = "THỐNG KÊ DANH SÁCH KHÁCH HÀNG "+fromdate.ToString() +" tới "+ todate.ToString();
                    ws.Cells[1, 1, 1, countColumnHeader].Merge = true;
                    ws.Cells[1, 1, 1, countColumnHeader].Style.Font.Bold = true;
                    ws.Cells[1, 1, 1, countColumnHeader].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    int cot = 1;
                    int hang = 2;
                    foreach (var item in arrColumnheader)
                    {
                        var cell = ws.Cells[hang, cot];
                        cell.Style.Font.Bold = true;
                        cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        // Tự động điều chỉnh độ rộng của ô dựa trên nội dung
                        cell.AutoFitColumns();

                        cell.Value = item;
                        cot++;
                    }
                    foreach (var item in ltkKH)
                    {
                        cot = 1;
                        hang++;
                        ws.Cells[hang, cot++].Value = item.Mathe;
                        ws.Cells[hang, cot++].Value = item.Hoten;
                        ws.Cells[hang, cot++].Value = item.Ngaysinh; 
                        ws.Cells[hang, cot++].Value = item.Gioitinh;
                        ws.Cells[hang, cot++].Value = item.CMND;
                        ws.Cells[hang, cot++].Value = item.diaChi;
                        ws.Cells[hang, cot++].Value = item.SDT;
                        ws.Cells[hang, cot++].Value = item.Email;
                        ws.Cells[hang, cot++].Value = item.GhiChu;
                        ws.Cells[hang, cot++].Value = item.maGoiTap;
                        ws.Cells[hang, cot++].Value = item.NgayBatDau.ToShortDateString();
                        ws.Cells[hang, cot++].Value = item.NgayHetHan.ToShortDateString();
                        ws.Cells[hang, cot++].Value = item.TenPT;
                        ws.Cells[hang, cot++].Value = item.maNhanVien;
                        ws.Cells[hang, cot++].Value = item.GoitapPT;
                        ws.Cells[hang, cot++].Value = item.ThoiHan.ToShortDateString();

                    }
                    // lu file
                    Byte[] bin = excel.GetAsByteArray();
                    File.WriteAllBytes(filepath, bin);

                }
                MessageBox.Show("Xuất file thành công ");

            }
            catch (Exception ee)
            {
                MessageBox.Show("Có lỗi khi lưu file excel" + ee.Message);
            }
        }
    }
}
