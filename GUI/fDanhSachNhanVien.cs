﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;
namespace GUI
{
    public partial class fDanhSachNhanVien : Form
    {
        public fDanhSachNhanVien()
        {
            InitializeComponent();
        }
        TaiKhoanBLL tkbll = new TaiKhoanBLL();
        NhanVienBLL nvbll = new NhanVienBLL();
    
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void LoadList()
        {
            List<NHANVIEN> lNV = new List<NHANVIEN>();
            lNV = nvbll.xemNhanVien();
            dtgNhanVien.Columns.Add("Column1", "MANHANVIEN");
            dtgNhanVien.Columns.Add("Column2", "HOTEN");
            dtgNhanVien.Columns.Add("Column3", "SDT");
            dtgNhanVien.Columns.Add("Column4", "CMND");
            dtgNhanVien.Columns.Add("Column5", "CHUCVU");
            foreach (NHANVIEN nhanvien in lNV)
            {
                dtgNhanVien.Rows.Add(nhanvien.maNhanVien, nhanvien.Hoten, nhanvien.SDT, nhanvien.CMND, nhanvien.Chucvu);
            }
        }

        private void fDanhSachNhanVien_Load(object sender, EventArgs e)
        {
            LoadList();
        }

        

        private void dtgNhanVien_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex >= 0)
            {
                object value = dtgNhanVien.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                string maNV = value.ToString();
                fThongTinNhanvien fttnv = new fThongTinNhanvien();
                fThongTinNhanvien.maNV = maNV;
                this.Hide();
                fttnv.ShowDialog();
                this.Show();
            }
        }
    }
}
