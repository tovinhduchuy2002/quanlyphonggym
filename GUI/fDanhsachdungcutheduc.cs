﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;
using OfficeOpenXml;
using System.IO;

namespace GUI
{
    public partial class fDanhsachdungcutheduc : Form
    {
        public fDanhsachdungcutheduc()
        {
            InitializeComponent();
        }
        ThietBiBLL tbbll = new ThietBiBLL();
        private string loaitb;
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private List<THIETBI> lTB = new List<THIETBI>();
        private void btnThemTB_Click(object sender, EventArgs e)
        {
            fThemthietbi fttb = new fThemthietbi();
            this.Hide();
            fttb.ShowDialog();
            this.Show();
        }

        private void fDanhsachdungcutheduc_Load(object sender, EventArgs e)
        {
           
            LoadList();
            
        }

       
        private void LoadList()
        {
            
            lTB = tbbll.xemTB();
            dtbDanhsachthietbi.Columns.Add("Column1", "MATHIETBI");
            dtbDanhsachthietbi.Columns.Add("Column2", "TENTHIETBI");
            dtbDanhsachthietbi.Columns.Add("Column3", "SOLUONG");
            dtbDanhsachthietbi.Columns.Add("Column4", "DONVI");
            dtbDanhsachthietbi.Columns.Add("Column5", "TINHTRANG");
            txtTSTB.Text = lTB.Count.ToString();
            foreach (THIETBI tb in lTB)
            {
                dtbDanhsachthietbi.Rows.Add(tb.Mathietbi,tb.Tenthietbi, tb.Soluong, tb.Donvi, tb.Tinhtrang);
            }



        }
        private void RefreshData()
        {
            dtbDanhsachthietbi.Rows.Clear();
            List<THIETBI> lTB = new List<THIETBI>();
            
            lTB = tbbll.xemTB();
            txtTSTB.Text = lTB.Count.ToString();
            foreach (THIETBI tb in lTB)
            {
                dtbDanhsachthietbi.Rows.Add(tb.Mathietbi, tb.Tenthietbi, tb.Soluong, tb.Donvi, tb.Tinhtrang);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnDSTBBT_Click(object sender, EventArgs e)
        { 
            List<THIETBI> lTBBT = new List<THIETBI>();
            dtbDanhsachthietbi.Rows.Clear();
            lTBBT = tbbll.xemTBBT();
            txtTBBT.Text = lTBBT.Count.ToString();
            foreach (THIETBI tb in lTBBT)
            {
                dtbDanhsachthietbi.Rows.Add(tb.Mathietbi, tb.Tenthietbi, tb.Soluong, tb.Donvi, tb.Tinhtrang);
            }
        }

        private void btnXuatThongtin_Click(object sender, EventArgs e)
        {
            string filepath = "";
            //Tạo saveDiaglouge
            SaveFileDialog save = new SaveFileDialog();
            //file filter
            save.Filter = "Excel Workbook|*.xlsx ";
            if (save.ShowDialog() == DialogResult.OK)
            {
                filepath = save.FileName;
            }
            if (string.IsNullOrEmpty(filepath))
            {
                MessageBox.Show("Đường dẫn không hợp lệ");
                return;
            }
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            try
            {
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Properties.Author = "Sucsongmoi";
                    excel.Workbook.Properties.Title = "Danh sách thiết bị";
                    excel.Workbook.Worksheets.Add("Danh sách thiết bị");
                    ExcelWorksheet ws = excel.Workbook.Worksheets[0];
                    ws.Name = "Danh sách khách hàng";
                    ws.Cells.Style.Font.Size = 13;
                    ws.Cells.Style.Font.Name = "Tahoma";
                    string[] arrColumnheader =
                    {
                        "Mã thiết bị",
                        "Tên thiết bị",
                        "Tình trạng"                      
                    };
                    var countColumnHeader = arrColumnheader.Count();
                    //Merge va dat ten cho cell 1
                    ws.Cells[1, 1].Value = "THỐNG KÊ DANH THIẾT BỊ";
                    ws.Cells[1, 1, 1, countColumnHeader].Merge = true;
                    ws.Cells[1, 1, 1, countColumnHeader].Style.Font.Bold = true;
                    ws.Cells[1, 1, 1, countColumnHeader].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    int cot = 1;
                    int hang = 2;
                    foreach (var item in arrColumnheader)
                    {
                        var cell = ws.Cells[hang, cot];
                        cell.Style.Font.Bold = true;
                        cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        // Tự động điều chỉnh độ rộng của ô dựa trên nội dung
                        cell.AutoFitColumns();

                        cell.Value = item;
                        cot++;
                    }
                    foreach (var item in lTB)
                    {
                        cot = 1;
                        hang++;
                        ws.Cells[hang, cot++].Value = item.Mathietbi;
                        ws.Cells[hang, cot++].Value = item.Tenthietbi;
                        ws.Cells[hang, cot++].Value = item.Tinhtrang;
                        

                    }
                    // lu file
                    Byte[] bin = excel.GetAsByteArray();
                    File.WriteAllBytes(filepath, bin);

                }
                MessageBox.Show("Xuất file thành công ");

            }
            catch (Exception ee)
            {
                MessageBox.Show("Có lỗi khi lưu file excel" + ee.Message);
            }
        }

        private void cmbloaithietbi_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbloaithietbi.SelectedItem.ToString() == "MÁY CARDIO")
            {
                loaitb = "MAYCARDIO";
                LoadForm();
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP NGỰC")
            {
                loaitb = "MAYTAPNGUC";
                LoadForm();
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP VAI")
            {
                loaitb = "MAYTAPVAI";
                LoadForm();
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP TAY")
            {
                loaitb = "MAYTAPTAY";
                LoadForm();
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP BỤNG")
            {
                loaitb = "MAYTAPBUNG";
                LoadForm();
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP CHÂN")
            {
                loaitb = "MAYTAPCHAN";
                LoadForm();
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP LƯNG")
            {
                loaitb = "MAYTAPLUNG";
                LoadForm();
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP XÔ")
            {
                loaitb = "MAYTAPXO";
                LoadForm();
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "MÁY TẬP TỔNG HỢP")
            {
                loaitb = "MAYTAPTONGHOP";
                LoadForm();
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "DÀN RACK")
            {
                loaitb = "DANRACK";
                LoadForm();
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "TẠ ĐƠN")
            {
                loaitb = "TADON";
                LoadForm();
            }
            else if (cmbloaithietbi.SelectedItem.ToString() == "TẠ ĐÒN")
            {
                loaitb = "TADONN";
                LoadForm();
            }
        }
        private void LoadForm()
        {
            fLoaiThietBi fltb = new fLoaiThietBi();
            fLoaiThietBi.loaiTB = loaitb;
            this.Hide();
            fltb.ShowDialog();
            this.Show();
        }

        private void dtbDanhsachthietbi_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex >= 0)
            {
                object value = dtbDanhsachthietbi.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                string maTB = (string)value;
                fThongtinTB ftttb = new fThongtinTB();
                fThongtinTB.maTB = maTB;
                this.Hide();
                ftttb.ShowDialog();
                this.Show();
            }
        }
    }
}
