﻿
namespace GUI
{
    partial class fKhoiPhucMatKhau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetOTP = new System.Windows.Forms.Button();
            this.lbMatKhauMoi = new System.Windows.Forms.Label();
            this.tbMaTaiKhoan = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbXacNhanMatKhau = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbOTP = new System.Windows.Forms.TextBox();
            this.btnKhoiPhuc = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.tbMatKhauMoi = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnGetOTP
            // 
            this.btnGetOTP.Location = new System.Drawing.Point(273, 119);
            this.btnGetOTP.Margin = new System.Windows.Forms.Padding(4);
            this.btnGetOTP.Name = "btnGetOTP";
            this.btnGetOTP.Size = new System.Drawing.Size(100, 28);
            this.btnGetOTP.TabIndex = 0;
            this.btnGetOTP.Text = "Get OTP";
            this.btnGetOTP.UseVisualStyleBackColor = true;
            this.btnGetOTP.Click += new System.EventHandler(this.btnGetOTP_Click);
            // 
            // lbMatKhauMoi
            // 
            this.lbMatKhauMoi.AutoSize = true;
            this.lbMatKhauMoi.Location = new System.Drawing.Point(48, 58);
            this.lbMatKhauMoi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbMatKhauMoi.Name = "lbMatKhauMoi";
            this.lbMatKhauMoi.Size = new System.Drawing.Size(92, 17);
            this.lbMatKhauMoi.TabIndex = 1;
            this.lbMatKhauMoi.Text = "Mật khẩu mới";
            // 
            // tbMaTaiKhoan
            // 
            this.tbMaTaiKhoan.Location = new System.Drawing.Point(160, 22);
            this.tbMaTaiKhoan.Margin = new System.Windows.Forms.Padding(4);
            this.tbMaTaiKhoan.Name = "tbMaTaiKhoan";
            this.tbMaTaiKhoan.Size = new System.Drawing.Size(209, 22);
            this.tbMaTaiKhoan.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 90);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Xác nhận mật khẩu";
            // 
            // tbXacNhanMatKhau
            // 
            this.tbXacNhanMatKhau.Location = new System.Drawing.Point(160, 86);
            this.tbXacNhanMatKhau.Margin = new System.Windows.Forms.Padding(4);
            this.tbXacNhanMatKhau.Name = "tbXacNhanMatKhau";
            this.tbXacNhanMatKhau.Size = new System.Drawing.Size(209, 22);
            this.tbXacNhanMatKhau.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 123);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Mã OTP";
            // 
            // tbOTP
            // 
            this.tbOTP.Location = new System.Drawing.Point(160, 119);
            this.tbOTP.Margin = new System.Windows.Forms.Padding(4);
            this.tbOTP.Name = "tbOTP";
            this.tbOTP.Size = new System.Drawing.Size(104, 22);
            this.tbOTP.TabIndex = 2;
            // 
            // btnKhoiPhuc
            // 
            this.btnKhoiPhuc.Location = new System.Drawing.Point(160, 151);
            this.btnKhoiPhuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnKhoiPhuc.Name = "btnKhoiPhuc";
            this.btnKhoiPhuc.Size = new System.Drawing.Size(100, 28);
            this.btnKhoiPhuc.TabIndex = 0;
            this.btnKhoiPhuc.Text = "Khôi phục";
            this.btnKhoiPhuc.UseVisualStyleBackColor = true;
            this.btnKhoiPhuc.Click += new System.EventHandler(this.btnKhoiPhuc_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.Location = new System.Drawing.Point(273, 151);
            this.btnHuy.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(100, 28);
            this.btnHuy.TabIndex = 0;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.UseVisualStyleBackColor = true;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // tbMatKhauMoi
            // 
            this.tbMatKhauMoi.Location = new System.Drawing.Point(160, 54);
            this.tbMatKhauMoi.Margin = new System.Windows.Forms.Padding(4);
            this.tbMatKhauMoi.Name = "tbMatKhauMoi";
            this.tbMatKhauMoi.Size = new System.Drawing.Size(209, 22);
            this.tbMatKhauMoi.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã tài khoản:";
            // 
            // fKhoiPhucMatKhau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 209);
            this.Controls.Add(this.tbOTP);
            this.Controls.Add(this.tbXacNhanMatKhau);
            this.Controls.Add(this.tbMatKhauMoi);
            this.Controls.Add(this.tbMaTaiKhoan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbMatKhauMoi);
            this.Controls.Add(this.btnHuy);
            this.Controls.Add(this.btnKhoiPhuc);
            this.Controls.Add(this.btnGetOTP);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "fKhoiPhucMatKhau";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fKhoiPhucMatKhau";
            this.Load += new System.EventHandler(this.fKhoiPhucMatKhau_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetOTP;
        private System.Windows.Forms.Label lbMatKhauMoi;
        private System.Windows.Forms.TextBox tbMaTaiKhoan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbXacNhanMatKhau;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbOTP;
        private System.Windows.Forms.Button btnKhoiPhuc;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.TextBox tbMatKhauMoi;
        private System.Windows.Forms.Label label1;
    }
}