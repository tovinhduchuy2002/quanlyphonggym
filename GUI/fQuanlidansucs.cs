﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class fQuanlidansucs : Form
    {
        public fQuanlidansucs()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnDSNV_Click(object sender, EventArgs e)
        {
            fDanhSachNhanVien fdsnv = new fDanhSachNhanVien();
            this.Hide();
            fdsnv.ShowDialog();
            this.Show();
        }

        private void btnDSHLV_Click(object sender, EventArgs e)
        {
            fDanhSachHLV fdshlv = new fDanhSachHLV();
            this.Hide();
            fdshlv.ShowDialog();
            this.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fDangKiNhanVien FDKNV = new fDangKiNhanVien();
            this.Hide();
            FDKNV.ShowDialog();
            this.Show();
        }
    }
}
