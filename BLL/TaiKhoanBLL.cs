﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class TaiKhoanBLL
    {   
     
        TaiKhoanAccesss tkaccess = new TaiKhoanAccesss();
        
        public string Checklogin(TaiKhoan taikhoan)
        {
            if(taikhoan.maTaiKhoan =="")
            {
                return "mataikhoantrang";
            }
            else if(taikhoan.MatKhau =="")
            {
                return "matkhautrang";
            }
            string info = tkaccess.Checklogin(taikhoan);
            return info;
        }
        public string Capnhatmatkhau(TaiKhoan taikhoan)
        {
            if (taikhoan.maTaiKhoan == "")
            {
                return "mataikhoantrang";
            }
            else if (taikhoan.MatKhau == "")
            {
                return "matkhautrang";
            }
            string update = tkaccess.UpdateMatkhau(taikhoan);
             return update;
        }
        public string Check()
        {
            return SendMail.OTPreal;
        }
        public bool CheckOTP(string checker)
        {
             

            if (checker == SendMail.OTPreal)
            {

                return true;
                
            }
            else return false;

        }
        public int CheckLTK(TaiKhoan taikhoan)
        {
            int ltk = tkaccess.checkloaiTK(taikhoan);
            return ltk;
        }
       
       
       
       
    }
}
